# Maven Setup

[![Maven Setup pipeline](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "Maven Setup pipeline")
 ![Maven Setup pipeline](https://gitlab.com/freedumbytes/setup/badges/master/pipeline.svg "Maven Setup pipeline")](https://gitlab.com/freedumbytes/setup)

[![Maven Setup License](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Maven Setup License")
 ![Maven Setup License](https://img.shields.io/badge/Apache_License-2.0-orange.svg "Maven Setup License")](https://apache.org/licenses/LICENSE-2.0.html)

[![Maven Setup Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Setup Maven Site")
 ![Maven Setup Maven Site](https://img.shields.io/badge/Maven_Setup-latest_release-802C59 "Maven Setup Maven Site")](https://freedumbytes.gitlab.io/setup)
[![Maven Setup Maven Site](https://img.shields.io/badge/Maven_Setup-current_snapshot-802C59 "Maven Setup Maven Site")](https://freedumbytes.gitlab.io/setup/snapshot)

 * [![Maven Setup Reactor Dependency Convergence](https://img.shields.io/badge/Reactor_Dependency_Convergence-latest_release-802C59
      "Maven Setup Reactor Dependency Convergence")](https://freedumbytes.gitlab.io/setup/dependency-convergence.html)
   [![Maven Setup Reactor Dependency Convergence](https://img.shields.io/badge/Reactor_Dependency_Convergence-current_snapshot-802C59
      "Maven Setup Reactor Dependency Convergence")](https://freedumbytes.gitlab.io/setup/snapshot/dependency-convergence.html)
 * [![Maven Setup Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-latest_release-802C59
      "Maven Setup Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/plugin-updates-report.html)
   [![Maven Setup Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-current_snapshot-802C59
      "Maven Setup Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/plugin-updates-report.html)
 * [![Maven Setup Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-latest_release-802C59
      "Maven Setup Property Updates Report")](https://freedumbytes.gitlab.io/setup/property-updates-report.html)
   [![Maven Setup Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-current_snapshot-802C59
      "Maven Setup Property Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/property-updates-report.html)
 * [![Maven Setup Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-latest_release-802C59
      "Maven Setup Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/dependency-updates-report.html)
   [![Maven Setup Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-current_snapshot-802C59
      "Maven Setup Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/dependency-updates-report.html)

[![Maven Setup Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Maven Setup Javadoc")
 ![Maven Setup Javadoc](https://img.shields.io/badge/Javadoc-latest_release-4D7A97 "Maven Setup Javadoc")](https://freedumbytes.gitlab.io/setup/apidocs)
[![Maven Setup Javadoc](https://img.shields.io/badge/Javadoc-current_snapshot-4D7A97 "Maven Setup Javadoc")](https://freedumbytes.gitlab.io/setup/snapshot/apidocs)

[![Maven Setup Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Maven Setup Dependency Check")
 ![Maven Setup Dependency Check](https://img.shields.io/badge/Dependency_Check-latest_release-F78D0A "Maven Setup Dependency Check")](https://freedumbytes.gitlab.io/setup/dependency-check-report.html)
[![Maven Setup Dependency Check](https://img.shields.io/badge/Dependency_Check-current_snapshot-F78D0A "Maven Setup Dependency Check")](https://freedumbytes.gitlab.io/setup/snapshot/dependency-check-report.html)

[![Maven Setup Quality Gate Status](https://sonarcloud.io/api/project_badges/quality_gate?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup
   "Maven Setup Quality Gate Status")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup)

 * [![Maven Setup Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=reliability_rating
      "Maven Setup Reliability Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=reliability_rating&view=treemap)
   [![Maven Setup Bugs](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=bugs
      "Maven Setup Bugs")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=bugs)
 * [![Maven Setup Security Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=security_rating
      "Maven Setup Security Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=security_rating&view=treemap)
   [![Maven Setup Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=vulnerabilities
      "Maven Setup Vulnerabilities")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=vulnerabilities)
 * [![Maven Setup Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_rating
      "Maven Setup Maintainability Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_rating&view=treemap)
   [![Maven Setup Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_index
      "Maven Setup Technical Debt")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_index)
   [![Maven Setup Code Smells](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=code_smells
      "Maven Setup Code Smells")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=code_smells)
 * [![Maven Setup Coverage](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=coverage
      "Maven Setup Coverage")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=coverage&view=treemap)
 * [![Maven Setup Duplicated Lines Density](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=duplicated_lines_density
      "Maven Setup Duplicated Lines Density")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=duplicated_lines_density&view=treemap)
 * [![Maven Setup Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=ncloc
      "Maven Setup Lines of Code")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=ncloc)

[![Maven Setup Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Maven Setup Maven Central")
 ![Maven Setup Maven Central](https://img.shields.io/maven-central/v/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=Maven%20Central
   "Maven Setup Maven Central")](https://search.maven.org/search?q=g%3Anl.demon.shadowland.freedumbytes.maven.config)

[![Maven Setup Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Maven Setup Nexus")
 ![Maven Setup Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=Nexus
   "Maven Setup Nexus")](https://oss.sonatype.org/#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.config~~~~)

[![Maven Setup MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "Maven Setup MvnRepository")
 ![Maven Setup MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=MvnRepository
   "Maven Setup MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.config)

## Legend

 * [![Apache License](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache License") Apache License](https://apache.org/licenses) terms and conditions for use, reproduction, and distribution.
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Maven generated Site](https://maven.apache.org/plugins/maven-site-plugin) includes the project's reports that were configured in the POM.
 * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Javadoc](https://en.wikipedia.org/wiki/Javadoc) (originally cased JavaDoc) is a documentation generator created by Sun Microsystems for the Java language.
 * [![Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Dependency Check") Dependency Check](https://owasp.org/www-project-dependency-check)
   is a utility that identifies project dependencies and checks if there are any known, publicly disclosed, vulnerabilities.
 * [![SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "SonarCloud") SonarCloud](https://sonarcloud.io/explore/projects?sort=-analysis_date)
   inspects the quality of source code and detect bugs, vulnerabilities and code smells in more than 20 different languages.
 * [![Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Maven Central") The Central Repository](https://search.maven.org) official search.
 * [![Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Nexus") Nexus Repository Manager](https://oss.sonatype.org) manages binaries and build artifacts across your software supply chain.
 * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") MvnRepository](https://mvnrepository.com) is like Google for Maven artifacts with Dependencies Updates information.

## Table of Contents

[[_TOC_]]

## Available Documentation

Overall project documentation can be found at:

 * [![Bitbucket](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket") freedumbytes.bitbucket.io](https://freedumbytes.bitbucket.io)
 * [![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") freedumbytes.github.io](https://freedumbytes.github.io)
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") freedumbytes.gitlab.io](https://freedumbytes.gitlab.io)

## Available Projects

Currently using the following git repositories:

 * [![Bitbucket](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket") Bitbucket](https://bitbucket.org/freedumbytes)
 * [![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") GitHub](https://github.com/freedumbytes)
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") GitLab](https://gitlab.com/freedumbytes)

Check the following [![DPL Manual](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "DPL Manual") git command examples](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#vcs.git.commands).

## Development Production Line - The Short Story

The [![DPL Manual](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "DPL Manual") Development Production Line](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html) manual (as of version `4.0-beta`)
contains a description how the following projects came about:

 1. [![DPL Manual](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "DPL Manual") Maven Setup](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.setup)
    the Maven site, license and plugin configuration in a parent POM.

 2. [![DPL Manual](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "DPL Manual") Maven Versions Rules](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.versions.rules)
    the Maven `ruleSet` file containing the rules that control how to compare version numbers.

    **Note**: The `rules.xml` was almost removed with
    [![Versions Maven Plugin 3.0.0 and higher](https://img.shields.io/badge/Versions_Maven_Plugin-3.0.0_%2B-D5AF1B "Versions Maven Plugin 3.0.0 and higher")](https://www.mojohaus.org/versions-maven-plugin)
    (see also [![issue 157](https://img.shields.io/github/issues/detail/state/mojohaus/versions-maven-plugin/157.svg "issue 157")](https://github.com/mojohaus/versions-maven-plugin/issues/157)).

 3. [![DPL Manual](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "DPL Manual") Custom Dependency Check](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.dependency.check.custom.suppression.hints)
    `suppressionFiles` and `hintsFile` containing the rules that control how to suppress false positives and resolve false negatives.

 4. [![DPL Manual](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "DPL Manual") Bill Of Materials](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.grouping.dependencies)
    to setup the required versions of the most popular Maven dependencies and group them logically together.

 5. [![DPL Manual](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "DPL Manual") Custom Maven Skins](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.site.skin.custom) configuration.

 6. [![JModalWindow](https://freedumbytes.gitlab.io/setup/images/icon/jmodalwindow.png "JModalWindow") JModalWindow](https://freedumbytes.gitlab.io/jmodalwindow.xhtml)
    was created to support modal functionality, similar to `JDialog`, without blocking all frames. And was migrated from Ant to Maven to demonstrate `maven.compiler.target` usage.

 7. [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") Basketball](https://gitlab.com/freedumbytes/basketball) Data Storage of Game Statistics.

## Maven Setup

To use the site, license and default plugin configuration of [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") Maven Setup](https://gitlab.com/freedumbytes/setup), for easy upgrade to the latest versions:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
    <artifactId>setup</artifactId>
    <version>x.y.z</version>
  </parent>
```

Or to also use the [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") Java build and reporting settings](https://gitlab.com/freedumbytes/setup/-/tree/master/bom/java)
and its now parent BOM default `dependencyManagement`:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
    <artifactId>java</artifactId>
    <version>x.y.z</version>
  </parent>
```

Plus for example the following subset of the `setup` default configured properties, that might need overriding:

```xml
  <properties>
    <projectFolderName>jmodalwindow</projectFolderName>
    <projectRoot>${gitlabHost}/${projectFolderName}</projectRoot>

    <sourceConnection>scm:git:${gitlabRepo}/${projectFolderName}.git</sourceConnection>
    <sourceDevConnection>scm:git:${gitlabRepoSSH}/${projectFolderName}.git</sourceDevConnection>
    <sourceWebRoot>${gitlabRepo}/${projectFolderName}/tree/master</sourceWebRoot>

    <ciSystem>GitLab CI</ciSystem>
    <ciWebRoot>${gitlabRepo}/${projectFolderName}/pipelines</ciWebRoot>

    <issueSystem>GitLab</issueSystem>
    <issueWebRoot>${gitlabRepo}/${projectFolderName}/issues</issueWebRoot>

    <processDependencyManagement>false</processDependencyManagement>

    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    <maven.compiler.source>${maven.compiler.target}</maven.compiler.source>
    <maven.compiler.target>${maven.compiler.compilerVersion}</maven.compiler.target>
    <maven.compiler.testSource>${maven.compiler.testTarget}</maven.compiler.testSource>
    <maven.compiler.testTarget>${maven.compiler.target}</maven.compiler.testTarget>

    <mojoSignatureArtifactId>java18</mojoSignatureArtifactId>
    <ignoreDependencies>true</ignoreDependencies>

    <extraEnforcerRulesMaxJdkVersion>${maven.compiler.target}</extraEnforcerRulesMaxJdkVersion>
    <enforcerRulesFailEnforceBytecodeVersion>${enforcerRulesFail}</enforcerRulesFailEnforceBytecodeVersion>

    <javadocVersion>1.8.0</javadocVersion>
    <javadocDoclint>none</javadocDoclint>

    <aggregateSurefireReports>true</aggregateSurefireReports>
    <surefire.useSystemClassLoader>true</surefire.useSystemClassLoader>
    <failsafe.useSystemClassLoader>true</failsafe.useSystemClassLoader>
  </properties>
```

Those properties can be changed when:

 * Compiling for older JDKs.
 * Compiling and testing with different source levels.
 * Single module projects should use `aggregateSurefireReports` `false`, otherwise the Surefire Report will always show that the number of tests is `0` and the Failsafe Report will be missing altogether.
 * Error: _Could not find or load main class org.apache.maven.surefire.booter.ForkedBooter_ (see also [Class Loading and Forking in Maven Surefire](https://maven.apache.org/surefire/maven-surefire-plugin/examples/class-loading.html)).

**Note**: To demonstrate that sometimes only one property needs to be overridden the related properties are also listed. But obviously it also possible to differ from the default related property setting such as `target` and `testTarget`.

### Maven Plugins Overview

[Apache Maven](https://maven.apache.org) [3.6.3](https://maven.apache.org/docs/history.html) is a software project management and comprehension tool.
Based on the concept of a project object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information.

Maven is, at its heart, a plugin execution framework. Looking for a specific goal to execute?
This page lists [the ![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") core plugins](https://maven.apache.org/plugins) and some others,
such as those hosted at [the ![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") MojoHaus Project](https://www.mojohaus.org/plugins.html) (previously known as Mojo@Codehaus):

 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-help-plugin](https://maven.apache.org/plugins/maven-help-plugin) [3.2.0](https://github.com/apache/maven-help-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317522))
   * `mvn help:active-profiles`
   * `mvn help:all-profiles`
   * `mvn help:effective-pom`
   * `mvn help:effective-settings`
   * `mvn help:system`
 * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") wagon-maven-plugin](https://www.mojohaus.org/wagon-maven-plugin) [2.0.2](https://github.com/mojohaus/wagon-maven-plugin/releases)
   ([issues](https://github.com/mojohaus/wagon-maven-plugin/milestones?state=closed))
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") wagon-... provider](https://maven.apache.org/wagon) [3.5.1](https://github.com/apache/maven-wagon/releases)
     ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12318122))
     * The following Wagon providers are deprecated and will be removed in version 4.0.0:
       * [HTTP lightweight](https://maven.apache.org/wagon/wagon-providers/wagon-http-lightweight)
       * [FTP](https://maven.apache.org/wagon/wagon-providers/wagon-ftp)
       * [SSH/SCP](https://maven.apache.org/wagon/wagon-providers/wagon-ssh)
       * [WebDAV](https://maven.apache.org/wagon/wagon-providers/wagon-webdav-jackrabbit)
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-clean-plugin](https://maven.apache.org/plugins/maven-clean-plugin) [3.1.0](https://github.com/apache/maven-clean-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317224))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-toolchains-plugin](https://maven.apache.org/plugins/maven-toolchains-plugin) [3.0.0](https://github.com/apache/maven-toolchains-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12318020))
 * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") versions-maven-plugin](https://www.mojohaus.org/versions-maven-plugin) [2.9.0](https://github.com/mojohaus/versions-maven-plugin/releases)
   ([issues](https://github.com/mojohaus/versions-maven-plugin/milestones?state=closed))
   * `mvn versions:display-plugin-updates` displays all plugins that have newer versions available.
   * `mvn versions:display-dependency-updates -DprocessDependencyManagement=false` displays **module** dependencies that have newer versions available.
   * `mvn versions:display-dependency-updates -DprocessDependencyManagement=true` displays **all** dependencies that have newer versions available.
   * `mvn versions:display-property-updates` displays properties that are linked to artifact versions and have updates available.
   * **Tip**: Use `grep` to filter for example output of `mvn versions:display-plugin-updates | grep '\->'` on updates only.
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache")  Maven Plugin Tools](https://maven.apache.org/plugin-tools) [3.6.4](https://github.com/apache/maven-plugin-tools/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317820))
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-plugin-plugin](https://maven.apache.org/plugin-tools/maven-plugin-plugin) 3.6.4
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-plugin-annotations](https://maven.apache.org/plugin-tools/maven-plugin-annotations) 3.6.4
     ([usage](https://maven.apache.org/plugin-tools/maven-plugin-plugin/examples/using-annotations.html))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-archetype-plugin](https://maven.apache.org/archetype/maven-archetype-plugin) [3.2.1](https://github.com/apache/maven-archetype/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317122))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-invoker-plugin](https://maven.apache.org/plugins/maven-invoker-plugin) [3.2.2](https://github.com/apache/maven-invoker-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317525))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-jdeps-plugin](https://maven.apache.org/plugins/maven-jdeps-plugin) [3.1.2](https://github.com/apache/maven-jdeps-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12319223))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-dependency-plugin](https://maven.apache.org/plugins/maven-dependency-plugin) [3.2.0](https://github.com/apache/maven-dependency-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317227))
 * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") truezip-maven-plugin](https://www.mojohaus.org/truezip/truezip-maven-plugin) [1.2](https://github.com/mojohaus/truezip/releases)
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-patch-plugin](https://maven.apache.org/plugins/maven-patch-plugin) [1.2](https://github.com/apache/maven-patch-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317622))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-resources-plugin](https://maven.apache.org/plugins/maven-resources-plugin) [3.2.0](https://github.com/apache/maven-resources-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317827))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-antrun-plugin](https://maven.apache.org/plugins/maven-antrun-plugin) [3.0.0](https://github.com/apache/maven-antrun-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317121))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-compiler-plugin](https://maven.apache.org/plugins/maven-compiler-plugin) [3.10.0](https://github.com/apache/maven-compiler-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317225))
 * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") animal-sniffer-maven-plugin](https://www.mojohaus.org/animal-sniffer/animal-sniffer-maven-plugin)
   [1.21](https://github.com/mojohaus/animal-sniffer/releases) ([issues](https://github.com/mojohaus/animal-sniffer/milestones?state=closed))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-enforcer-plugin](https://maven.apache.org/enforcer/maven-enforcer-plugin) [3.0.0](https://github.com/apache/maven-enforcer/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317520))
   * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") extra-enforcer-rules](https://www.mojohaus.org/extra-enforcer-rules) [1.3](https://github.com/mojohaus/extra-enforcer-rules/releases)
     ([issues](https://github.com/mojohaus/extra-enforcer-rules/milestones?state=closed))
   * `mvn enforcer:enforce`
   * `mvn enforcer:display-info`
 * [![EclEmma](https://freedumbytes.gitlab.io/setup/images/icon/eclemma.png "EclEmma") jacoco-maven-plugin](https://www.eclemma.org/jacoco/trunk/doc/maven.html) [0.8.7](https://github.com/jacoco/jacoco/releases)
   ([issues](https://github.com/jacoco/jacoco/milestones?state=closed))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-surefire-plugin](https://maven.apache.org/surefire/maven-surefire-plugin) [2.22.2](https://github.com/apache/maven-surefire/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317927))
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-failsafe-plugin](https://maven.apache.org/surefire/maven-failsafe-plugin) 2.22.2
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-surefire-report-plugin](https://maven.apache.org/surefire/maven-surefire-report-plugin) 2.22.2
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-jar-plugin](https://maven.apache.org/plugins/maven-jar-plugin) [3.2.2](https://github.com/apache/maven-jar-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317526))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-ejb-plugin](https://maven.apache.org/plugins/maven-ejb-plugin) [3.1.0](https://github.com/apache/maven-ejb-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317421))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-war-plugin](https://maven.apache.org/plugins/maven-war-plugin) [3.3.2](https://github.com/apache/maven-war-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12318121))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-rar-plugin](https://maven.apache.org/plugins/maven-rar-plugin) [2.4](https://github.com/apache/maven-rar-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317823))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-ear-plugin](https://maven.apache.org/plugins/maven-ear-plugin) [3.2.0](https://github.com/apache/maven-ear-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317422))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-shade-plugin](https://maven.apache.org/plugins/maven-shade-plugin) [3.2.4](https://github.com/apache/maven-shade-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317921))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-source-plugin](https://maven.apache.org/plugins/maven-source-plugin) [3.2.1](https://github.com/apache/maven-source-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317924))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-javadoc-plugin](https://maven.apache.org/plugins/maven-javadoc-plugin) [3.3.2](https://github.com/apache/maven-javadoc-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317529))
   * [![Eclipse EE4J](https://freedumbytes.gitlab.io/setup/images/icon/eclipse-foundation.png "Eclipse EE4J") Jakarta Interceptors](https://projects.eclipse.org/projects/ee4j.interceptors)
     interpose on business method invocations and specific events ([changelog](https://github.com/eclipse-ee4j/interceptor-api/releases))
     * `ERROR` class file for javax.interceptor.InterceptorBinding not found
       (see also [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab")
                  ![commit 00b54368](https://img.shields.io/badge/commit-00b54368-green "commit 00b54368")](https://gitlab.com/freedumbytes/setup/-/commit/00b54368)).
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-assembly-plugin](https://maven.apache.org/plugins/maven-assembly-plugin) [3.3.0](https://github.com/apache/maven-assembly-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317220))
 * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") build-helper-maven-plugin](https://www.mojohaus.org/build-helper-maven-plugin) [3.3.0](https://github.com/mojohaus/build-helper-maven-plugin/releases)
   ([issues](https://github.com/mojohaus/build-helper-maven-plugin/milestones?state=closed))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-install-plugin](https://maven.apache.org/plugins/maven-install-plugin) [2.5.2](https://github.com/apache/maven-install-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317524))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-jarsigner-plugin](https://maven.apache.org/plugins/maven-jarsigner-plugin) [3.0.0](https://github.com/apache/maven-jarsigner-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317528))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-gpg-plugin](https://maven.apache.org/plugins/maven-gpg-plugin) [3.0.1](https://github.com/apache/maven-gpg-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317521))
 * [![Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Nexus") nexus-staging-maven-plugin](https://github.com/sonatype/nexus-maven-plugins/tree/master/staging/maven-plugin)
   [1.6.11](https://github.com/sonatype/nexus-maven-plugins/releases)
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-deploy-plugin](https://maven.apache.org/plugins/maven-deploy-plugin) [2.8.2](https://github.com/apache/maven-deploy-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317228))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-release-plugin](https://maven.apache.org/maven-release/maven-release-plugin) [2.5.3](https://github.com/apache/maven-release/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317824))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-scm-plugin](https://maven.apache.org/scm/maven-scm-plugin) [1.12.2](https://github.com/apache/maven-scm/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317828))
 * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") exec-maven-plugin](https://www.mojohaus.org/exec-maven-plugin) [3.0.0](https://github.com/mojohaus/exec-maven-plugin/releases)
   ([issues](https://github.com/mojohaus/exec-maven-plugin/milestones?state=closed))
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-site-plugin](https://maven.apache.org/plugins/maven-site-plugin) [3.11.0](https://github.com/apache/maven-site-plugin/releases)
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317923))
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Doxia](https://maven.apache.org/doxia/doxia)
     for generating static and dynamic content, supporting [a variety of markup languages](https://maven.apache.org/doxia/references).
   * Used by [Doxia Sitetools](https://maven.apache.org/doxia/doxia-sitetools) extension, that adds support for HTML sites and documents, like RTF or PDF.
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") maven-project-info-reports-plugin](https://maven.apache.org/plugins/maven-project-info-reports-plugin)
   [3.2.1](https://github.com/apache/maven-project-info-reports-plugin/releases) ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317821))
   * `mvn project-info-reports:dependency-convergence`
 * [![OWASP](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "OWASP") dependency-check-maven](https://jeremylong.github.io/DependencyCheck/dependency-check-maven)
   [6.5.3](https://github.com/jeremylong/DependencyCheck/releases) ([issues](https://github.com/jeremylong/DependencyCheck/milestones?state=closed)) ([changelog](https://github.com/jeremylong/DependencyCheck/blob/main/RELEASE_NOTES.md))
   * [![OWASP](https://freedumbytes.gitlab.io/setup/images/icon/owasp.png "OWASP") nist-data-mirror](https://github.com/stevespringett/nist-data-mirror) [1.5.1](https://github.com/stevespringett/nist-data-mirror/releases)
     * [![NVD Mirror](https://freedumbytes.gitlab.io/setup/images/icon/nvd.png "NVD Mirror") NIST NVD JSON Feeds Mirror](https://freedumbytes.gitlab.io/setup/nist-nvd-mirror)
 * [![SonarSource](https://freedumbytes.gitlab.io/setup/images/icon/sonarsource.png "SonarSource") sonar-maven-plugin](https://github.com/SonarSource/sonar-scanner-maven)
   [3.9.1.2184](https://github.com/SonarSource/sonar-scanner-maven/releases) ([changelog](https://jira.sonarsource.com/secure/ConfigureReleaseNote.jspa?projectId=10977))
 * [![SonarQube](https://freedumbytes.gitlab.io/setup/images/icon/sonarqube.png "SonarQube") sonarqube-maven-report](https://github.com/SonarQubeCommunity/sonar-maven-report)
   [0.2.2](https://github.com/SonarQubeCommunity/sonar-maven-report/releases)
 * [![Replacer](https://freedumbytes.gitlab.io/setup/images/icon/google.png "Replacer") replacer](https://code.google.com/archive/p/maven-replacer-plugin)
   [1.5.3](https://mvnrepository.com/artifact/com.google.code.maven-replacer-plugin/replacer) ([changelog](https://code.google.com/archive/p/maven-replacer-plugin/wikis/ReleaseNotes.wiki))
 * [![m2e](https://freedumbytes.gitlab.io/setup/images/icon/m2e.png "m2e") lifecycle-mapping](https://eclipse.org/m2e) 1.0.0 a **fake**/**inexistent** plugin.
   * [![m2e](https://freedumbytes.gitlab.io/setup/images/icon/m2e.png "m2e") M2Eclipse](https://eclipse.org/m2e) provides tight integration for Apache Maven into the IDE:
     * The [ignore](https://eclipse.org/m2e/documentation/m2e-execution-not-covered.html) option, as the name suggests, tells M2Eclipse to silently ignore the plugin execution.
     * Goal [project-info-reports:plugin-management](https://maven.apache.org/plugins/maven-project-info-reports-plugin/plugin-management-mojo.html) parameter `pluginManagementExcludes`
       (see also [![issue MPIR-375](https://img.shields.io/jira/issue/https/issues.apache.org/jira/MPIR-375.svg "issue MPIR-375")](https://issues.apache.org/jira/browse/MPIR-375)).
   * Alternatives (see Eclipse `Problems` option `Quick Fix`):
     * Defaults: [lifecycle-mapping-metadata.xml](https://git.eclipse.org/c/m2e/m2e-core.git/tree/org.eclipse.m2e.lifecyclemapping.defaults/lifecycle-mapping-metadata.xml)
     * Check for possible [m2e-connector](https://marketplace.eclipse.org/search/site/m2e-connector) at Eclipse MarketPlace.
   * Currently configured plugins with setting `ignore` or `m2e…PluginOnConfiguration` default `true` and `m2e…PluginOnIncremental` default `false`:
     * maven-plugin-plugin
     * maven-dependency-plugin
     * build-helper-maven-plugin
     * [![Asciidoctor](https://freedumbytes.gitlab.io/setup/images/icon/asciidoctor.png "Asciidoctor") asciidoctor-maven-plugin](https://github.com/asciidoctor/asciidoctor-maven-plugin)
       [2.2.2](https://github.com/asciidoctor/asciidoctor-maven-plugin/releases) ([issues](https://github.com/asciidoctor/asciidoctor-maven-plugin/milestones?state=closed))
     * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") axistools-maven-plugin](https://www.mojohaus.org/axistools-maven-plugin)
       [1.4](https://github.com/mojohaus/axistools-maven-plugin/releases) ([issues](https://github.com/mojohaus/axistools-maven-plugin/milestones?state=closed))
     * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") cxf-codegen-plugin](https://cxf.apache.org/docs/maven-cxf-codegen-plugin-wsdl-to-java.html)
       [3.5.0](https://github.com/apache/cxf/releases) ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12310511))
       * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") cxf-xjc-plugin](https://cxf.apache.org/cxf-xjc-plugin.html)
         [3.3.1](https://github.com/apache/cxf-xjc-utils/releases) ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12315520))
     * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") jaxws-maven-plugin](https://www.mojohaus.org/jaxws-maven-plugin)
       [2.6](https://github.com/mojohaus/jaxws-maven-plugin/releases) ([issues](https://github.com/mojohaus/jaxws-maven-plugin/milestones?state=closed))
     * [![PressGang](https://freedumbytes.gitlab.io/setup/images/icon/pressgang.png "PressGang") maven-jdocbook-plugin](https://github.com/pressgang/maven-jdocbook-plugin)
       [2.3.10](https://github.com/pressgang/maven-jdocbook-plugin/releases) ([issues](https://github.com/pressgang/maven-jdocbook-plugin/milestones?state=closed))
     * [![PressGang](https://freedumbytes.gitlab.io/setup/images/icon/pressgang.png "PressGang") maven-jdocbook-style-plugin](https://github.com/pressgang/maven-jdocbook-style-plugin)
       [2.0.0](https://github.com/pressgang/maven-jdocbook-style-plugin/releases) ([issues](https://github.com/pressgang/maven-jdocbook-style-plugin/milestones?state=closed))

## Maven Versions Rules

To use the ruleSet file containing the [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") Versions Rules](https://gitlab.com/freedumbytes/versions-rules) that control how to compare version numbers:

```xml
  <properties>
    <ossrhHost>https://oss.sonatype.org</ossrhHost>

    <versionsRulesVersion>1.3.1</versionsRulesVersion>
    <versionsRulesFolderVersion>${versionsRulesVersion}</versionsRulesFolderVersion>
    <versionsRulesPath>
      ${nexusHost}${nexusMavenGroupUri}/nl/demon/shadowland/freedumbytes/maven/versioning/versions-rules/${versionsRulesFolderVersion}
    </versionsRulesPath>
    <processDependencyManagement>true</processDependencyManagement>
  </properties>

  <profiles>
    <profile>
      <id>openSource</id>

      <properties>
        <nexusHost>${ossrhHost}</nexusHost>
        <nexusRepositoriesUri>/index.html#view-repositories</nexusRepositoriesUri>
        <nexusMavenGroupUri>/content/groups/public</nexusMavenGroupUri>
        <nexusMavenReleasesUri>/content/repositories/releases</nexusMavenReleasesUri>
        <nexusMavenSnapshotsUri>/content/repositories/snapshots</nexusMavenSnapshotsUri>
      </properties>
    </profile>

    <profile>
      <id>enableUpdatesReports</id>

      <reporting>
        <plugins>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>versions-maven-plugin</artifactId>
            <reportSets>
              <reportSet>
                <reports>
                  <report>dependency-updates-report</report>
                  <report>plugin-updates-report</report>
                  <report>property-updates-report</report>
                </reports>
                <configuration>
                  <processDependencyManagement>${processDependencyManagement}</processDependencyManagement>
                </configuration>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>versions-maven-plugin</artifactId>
          <version>${versionsMavenPluginVersion}</version>
          <configuration>
            <generateBackupPoms>false</generateBackupPoms>
            <outputEncoding>${project.reporting.outputEncoding}</outputEncoding>
            <rulesUri>${versionsRulesPath}/versions-rules-${versionsRulesVersion}.xml</rulesUri>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

Now display all plugins that have newer versions with: `mvn versions:display-plugin-updates -PopenSource` or generate the site `mvn site -PopenSource,enableUpdatesReports,-mirrorNistNvd`.

**Tip**: To test the next snapshot version of the rules, when they are installed in the local Maven repository, use the following command:

```
mvn versions:display-plugin-updates \
    -DversionsRulesPath=file://c/dev/data/repo/maven/local/nl/demon/shadowland/freedumbytes/maven/versioning/versions-rules/1.3.2-SNAPSHOT \
    -DversionsRulesVersion=1.3.2-SNAPSHOT
```

## Bill Of Materials

[![Bill Of Materials Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Bill Of Materials Maven Site")
 ![Bill Of Materials Maven Site](https://img.shields.io/badge/Bill_Of_Materials-latest_release-802C59 "Bill Of Materials Maven Site")](https://freedumbytes.gitlab.io/setup/bom)
[![Bill Of Materials Maven Site](https://img.shields.io/badge/Bill_Of_Materials-current_snapshot-802C59 "Bill Of Materials Maven Site")](https://freedumbytes.gitlab.io/setup/snapshot/bom)

 * [![Bill Of Materials Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-latest_release-802C59 "Bill Of Materials Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/bom/plugin-updates-report.html)
   [![Bill Of Materials Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-current_snapshot-802C59 "Bill Of Materials Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/plugin-updates-report.html)
 * [![Bill Of Materials Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-latest_release-802C59 "Bill Of Materials Property Updates Report")](https://freedumbytes.gitlab.io/setup/bom/property-updates-report.html)
   [![Bill Of Materials Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-current_snapshot-802C59 "Bill Of Materials Property Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/property-updates-report.html)
 * [![Bill Of Materials Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-latest_release-802C59 "Bill Of Materials Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/bom/dependency-updates-report.html)
   [![Bill Of Materials Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-current_snapshot-802C59
      "Bill Of Materials Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/dependency-updates-report.html)

To define the required versions of the most popular Maven dependencies link to the BOM:

```xml
  <properties>
    <bomVersion>x.y.z</bomVersion>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
        <artifactId>bom</artifactId>
        <version>${bomVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
```

Alas downgrading a subset of dependencies to a prior version requires repeating that subset of dependencies:

```xml
  <properties>
    <bomVersion>x.y.z</bomVersion>
    <springFrameworkVersion>4.3.30.RELEASE</springFrameworkVersion>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-framework-bom</artifactId>
        <version>${springFrameworkVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <dependency>
        <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
        <artifactId>bom</artifactId>
        <version>${bomVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
```

**Note**: The overridden dependency `spring-framework-bom` must be placed before the `bom` dependency in the `dependencyManagement`. Also because the Spring Framework has a BOM copying one dependency is sufficient in this case.

Thus as an alternative just extend the POM from it (see for example the module `java` POM) with `parent`:

[![Java Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Java Maven Site")
 ![Java Maven Site](https://img.shields.io/badge/Java_Setup-latest_release-802C59 "Java Maven Site")](https://freedumbytes.gitlab.io/setup/bom/java)
[![Java Maven Site](https://img.shields.io/badge/Java_Setup-current_snapshot-802C59 "Java Maven Site")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java)

 * [![Java Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-latest_release-802C59 "Java Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/plugin-updates-report.html)
   [![Java Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-current_snapshot-802C59 "Java Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/plugin-updates-report.html)
 * [![Java Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-latest_release-802C59 "Java Property Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/property-updates-report.html)
   [![Java Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-current_snapshot-802C59 "Java Property Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/property-updates-report.html)
 * [![Java Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-latest_release-802C59 "Java Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/dependency-updates-report.html)
   [![Java Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-current_snapshot-802C59 "Java Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/dependency-updates-report.html)

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
    <artifactId>bom</artifactId>
    <version>x.y.z</version>
  </parent>

  <artifactId>java</artifactId>
  <packaging>pom</packaging>

  <name>${organizationName} Java Setup</name>
  <description>Extends Maven Setup with Java build and reporting settings.</description>
```

Now having the Bill Of Materials as an ancestor module makes downgrading (or upgrading for that matter) effortless by just overriding the corresponding property only:

```xml
  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
<!--
    <artifactId>bom</artifactId>
 -->
    <artifactId>java</artifactId>
    <version>x.y.z</version>
  </parent>

  <groupId>nl.demon.shadowland.freedumbytes.demo</groupId>
  <artifactId>example</artifactId>
  <version>1.0.0</version>

  <properties>
    <springFrameworkVersion>4.3.30.RELEASE</springFrameworkVersion>
  </properties>
```

### Dependency Management Overview

If there is a set of dependencies, which are logically grouped together, just create a project for them with POM packaging.

When the following subsections are marked as [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Grouping](https://freedumbytes.gitlab.io/setup/bom/grouping)
the set of related dependencies can be included as one. For example to include Groupings of Log and Test Dependencies:


```xml
  <dependencies>
    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.config.dependencies</groupId>
      <artifactId>log</artifactId>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.config.dependencies</groupId>
      <artifactId>test</artifactId>
      <type>pom</type>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

**Note**: Use `scope` `test` to restrict the dependencies to the test phase only.

#### Common Dependencies

 * [![Project Lombok](https://freedumbytes.gitlab.io/setup/images/icon/lombok.png "Project Lombok") Lombok](https://projectlombok.org) reducing boilerplate code ([changelog](https://projectlombok.org/changelog)).
 * [![Apache Commons Lang](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons Lang") Apache Commons Lang](https://commons.apache.org/proper/commons-lang)
   provides a host of helper utilities for the `java.lang` API ([changelog](https://commons.apache.org/proper/commons-lang/changes-report.html)).
 * [![Apache Commons Codec](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons Codec") Apache Commons Codec](https://commons.apache.org/proper/commons-codec)
   common encoders and decoders such as Base64, Hex, Phonetic and URLs ([changelog](https://commons.apache.org/proper/commons-codec/changes-report.html)).
 * [![Apache Commons IO](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons IO") Apache Commons IO](https://commons.apache.org/proper/commons-io)
   utilities to assist with developing IO functionality ([changelog](https://commons.apache.org/proper/commons-io/changes-report.html)).
 * [![Guava](https://freedumbytes.gitlab.io/setup/images/icon/google.png "Guava") Guava](https://github.com/google/guava) Google Core Libraries for Java ([changelog](https://github.com/google/guava/releases)).

#### Grouping Spring Dependencies

 * [![Spring Framework](https://freedumbytes.gitlab.io/setup/images/icon/spring-framework.png "Spring Framework") Spring Framework](https://spring.io/projects/spring-framework)
   a comprehensive programming and configuration model ([changelog](https://github.com/spring-projects/spring-framework/releases)).

#### Grouping Hibernate Dependencies

 * [![Hibernate ORM](https://freedumbytes.gitlab.io/setup/images/icon/hibernate.png "Hibernate ORM") Hibernate Object/Relational Mapping (ORM)](https://hibernate.org/orm) framework ([changelog](https://hibernate.org/orm/releases)).
 * [![Spring ORM](https://freedumbytes.gitlab.io/setup/images/icon/spring-framework.png "Spring ORM") Spring Object/Relational Mapping (ORM)](https://docs.spring.io/spring-framework/docs/current/reference/html/data-access.html#orm)
   provides integration layers for popular object-relational mapping APIs, including Hibernate ([changelog](https://github.com/spring-projects/spring-framework/releases)).
   * Thus obviously the above mentioned **Grouping Spring Dependencies**.
 * [![Ehcache](https://freedumbytes.gitlab.io/setup/images/icon/ehcache.png "Ehcache") Ehcache](https://www.ehcache.org)
   standards-based cache that boosts performance, offloads your database, and simplifies scalability ([changelog](https://github.com/ehcache/ehcache3/releases)).
 * [![Javassist](https://freedumbytes.gitlab.io/setup/images/icon/redhat.png "Javassist") Java Programming Assistant (Javassist)](https://www.javassist.org)
   manipulating Java bytecode ([changelog](https://github.com/jboss-javassist/javassist/releases)).

#### Grouping Test Dependencies

 * [![Hamcrest](https://freedumbytes.gitlab.io/setup/images/icon/hamcrest.png "Hamcrest") Hamcrest](http://hamcrest.org) matchers that can be combined to create flexible expressions of intent
   ([changelog](https://github.com/hamcrest/JavaHamcrest/releases)).
 * [![JUnit 4](https://freedumbytes.gitlab.io/setup/images/icon/junit.png "JUnit 4") JUnit 4](https://junit.org/junit4) an instance of the xUnit architecture for unit testing frameworks
   ([changelog](https://github.com/junit-team/junit4/releases)).
 * [![JUnit Jupiter](https://freedumbytes.gitlab.io/setup/images/icon/junit-jupiter.png "JUnit Jupiter") JUnit Jupiter](https://junit.org) the programmer-friendly testing framework for Java and the JVM
   ([changelog](https://github.com/junit-team/junit5/releases)).
 * [![AssertJ Core](https://freedumbytes.gitlab.io/setup/images/icon/assertj.png "AssertJ Core") AssertJ Core](https://assertj.github.io/doc) fluent assertions java library
   ([changelog](https://assertj.github.io/doc/#assertj-core-release-notes)).
 * [![Byte Buddy](https://freedumbytes.gitlab.io/setup/images/icon/byte-buddy.png "Byte Buddy") Byte Buddy](https://bytebuddy.net) code generation and manipulation library for creating and modifying Java classes
   ([changelog](https://github.com/raphw/byte-buddy/releases)).
 * [![Mockito](https://freedumbytes.gitlab.io/setup/images/icon/mockito.png "Mockito") Mockito](https://github.com/mockito/mockito) a mocking framework that tastes really good ([changelog](https://github.com/mockito/mockito/releases)).
 * [![Spring Test](https://freedumbytes.gitlab.io/setup/images/icon/spring-framework.png "Spring Test") Spring Test](https://docs.spring.io/spring-framework/docs/current/reference/html/testing.html)
   provides generic, annotation-driven unit and integration testing support that is agnostic of the testing framework in use ([changelog](https://github.com/spring-projects/spring-framework/releases)).
   * Thus obviously the above mentioned **Grouping Spring Dependencies**.
 * [![CGLIB](https://freedumbytes.gitlab.io/setup/images/icon/github.png "CGLIB") Byte Code Generation Library (CGLIB)](https://github.com/cglib/cglib) a byte instrumentation library ([changelog](https://github.com/cglib/cglib/releases)).
 * [![Apache Commons DBCP](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons DBCP") Apache Commons DBCP](https://commons.apache.org/proper/commons-dbcp) Database Connection Pool supporting different versions of JDBC
   ([changelog](https://commons.apache.org/proper/commons-dbcp/changes-report.html)).
 * [![H2](https://freedumbytes.gitlab.io/setup/images/icon/h2.png "H2") H2](https://h2database.com/html/main.html) the Java SQL database ([changelog](https://h2database.com/html/changelog.html)).
 * [![HSQLDB](https://freedumbytes.gitlab.io/setup/images/icon/hsqldb.png "HSQLDB") HyperSQL DataBase (HSQLDB)](http://hsqldb.org) database management system ([changelog](http://hsqldb.org/doc/2.0/changelist_2_0.txt)).
 * [![DbUnit](https://freedumbytes.gitlab.io/setup/images/icon/dbunit.png "DbUnit") DbUnit](http://dbunit.sourceforge.net) a JUnit extension that puts your database into a known state between test runs
   ([changelog](http://dbunit.sourceforge.net/changes-report.html)).

#### Grouping Log Dependencies

 * [![Apache Commons Logging](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons Logging") Apache Commons Logging](https://commons.apache.org/proper/commons-logging)
   an ultra-thin bridge between different logging implementations ([changelog](https://commons.apache.org/proper/commons-logging/changes-report.html)).
   * **Note**: The `commons-logging` implementation is also supplied as part of `jcl-over-slf4j` and `spring-jcl`.
 * [![Apache Log4j2](https://freedumbytes.gitlab.io/setup/images/icon/log4j2.png "Apache Log4j2") Apache Log4j](https://logging.apache.org/log4j) enable logging at runtime without modifying the application binary
   ([changelog](https://logging.apache.org/log4j/2.x/changes-report.html)).
   * `Log4j-config.xsd` versions of which the `master` works fine as `xsi:schemaLocation` for XML validation. But the `strict` jar embedded `schema` doesn't match (yet).
     * [rel/2.13.3](https://github.com/apache/logging-log4j2/blob/rel/2.13.3/log4j-core/src/main/resources/Log4j-config.xsd)
     * [log4j-2.13.3](https://github.com/apache/logging-log4j2/blob/log4j-2.13.3/log4j-core/src/main/resources/Log4j-config.xsd)
     * [rel/2.17.1](https://github.com/apache/logging-log4j2/blob/rel/2.17.1/log4j-core/src/main/resources/Log4j-config.xsd)
     * [master](https://github.com/apache/logging-log4j2/blob/master/log4j-core/src/main/resources/Log4j-config.xsd)
   * [![Apache Log4j](https://freedumbytes.gitlab.io/setup/images/icon/log4j.png "Apache Log4j") Log4j 1.x API](https://logging.apache.org/log4j/2.x/log4j-1.2-api)
     - [Bridge](https://logging.apache.org/log4j/2.x/maven-artifacts.html#Log4j_1.x_API_Bridge)
   * [![Apache Commons Logging](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons Logging") Apache Commons Logging](https://logging.apache.org/log4j/2.x/log4j-jcl)
     - [Bridge](https://logging.apache.org/log4j/2.x/maven-artifacts.html#Apache_Commons_Logging_Bridge)
   * [![SLF4J](https://freedumbytes.gitlab.io/setup/images/icon/qos.png "SLF4J") SLF4J](https://logging.apache.org/log4j/2.x/log4j-slf4j-impl) - [Bridge](https://logging.apache.org/log4j/2.x/maven-artifacts.html#SLF4J_Bridge)
   * [![JDK](https://freedumbytes.gitlab.io/setup/images/icon/java.png "JDK Logging / JUL Adapter") JDK Logging / JUL Adapter](https://logging.apache.org/log4j/2.x/maven-artifacts.html#JUL_Adapter) is a
     [custom implementation](https://logging.apache.org/log4j/2.x/log4j-jul) of `java.util.logging.LogManager`.
 * [![SLF4J](https://freedumbytes.gitlab.io/setup/images/icon/qos.png "SLF4J") Simple Logging Facade for Java (SLF4J)](https://www.slf4j.org) serves as a simple facade or abstraction for various logging frameworks
   ([changelog](https://www.slf4j.org/news.html)).
 * [![JDK](https://freedumbytes.gitlab.io/setup/images/icon/java.png "JDK") Java Logging APIs](https://docs.oracle.com/en/java/javase/11/core/java-logging-overview.html)
   contained in the package [`java.util.logging`](https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/package-summary.html) produce log reports.

#### Grouping MySQL Dependencies

 * [![MySQL](https://freedumbytes.gitlab.io/setup/images/icon/mysql.png "MySQL") MySQL](https://mysql.com) Standard Edition includes InnoDB, making it a fully integrated transaction-safe, ACID compliant database.
   * [![MySQL](https://freedumbytes.gitlab.io/setup/images/icon/mysql.png "MySQL") Connector/J](https://github.com/mysql/mysql-connector-j) JDBC Type 4 driver ([changelog](https://dev.mysql.com/doc/relnotes/connector-j/8.0/en)).
     * [![MySQL](https://freedumbytes.gitlab.io/setup/images/icon/mysql.png "MySQL") Using Maven](https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-installing-maven.html)
 * [![HikariCP](https://freedumbytes.gitlab.io/setup/images/icon/hikari.png "HikariCP") HikariCP](https://github.com/brettwooldridge/HikariCP) a zero-overhead production ready JDBC connection pool
   ([changelog](https://raw.githubusercontent.com/brettwooldridge/HikariCP/dev/CHANGES)).
 * [![Javassist](https://freedumbytes.gitlab.io/setup/images/icon/redhat.png "Javassist") Java Programming Assistant (Javassist)](https://www.javassist.org)
   manipulating Java bytecode ([changelog](https://github.com/jboss-javassist/javassist/releases)).

#### Grouping Apache Tomcat Dependencies

[![Apache Tomcat](https://freedumbytes.gitlab.io/setup/images/icon/tomcat.png "Apache Tomcat") Apache Tomcat](https://tomcat.apache.org)
is an open source software implementation of a subset of the Jakarta EE (formally Java EE) technologies.
[Different versions of Apache Tomcat](https://tomcat.apache.org/whichversion.html) are available for different versions of the specifications ([changelog](https://tomcat.apache.org/tomcat-9.0-doc/changelog.html)).

Reference for some of the Jakarta EE Specifications:

 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Jakarta Servlets](https://projects.eclipse.org/projects/ee4j.servlet) standard technology for interacting with the web on the Jakarta EE platform
   ([changelog](https://github.com/eclipse-ee4j/servlet-api/releases)).
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Jakarta Server Pages (JSP)](https://projects.eclipse.org/projects/ee4j.jsp) technology that helps software developers create dynamically generated web pages
   ([changelog](https://github.com/eclipse-ee4j/jsp-api/releases)).
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Jakarta WebSocket](https://projects.eclipse.org/projects/ee4j.websocket) a standard API for creating WebSocket applications
   ([changelog](https://github.com/eclipse-ee4j/websocket-api/releases)).
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Jakarta Expression Language (EL)](https://projects.eclipse.org/projects/ee4j.el)
   is a mechanism for the presentation layer to communicate with the application logic
   ([changelog](https://github.com/eclipse-ee4j/el-ri/releases)).
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Jakarta Authentication Service Provider Interface for Containers (JASPIC)](https://projects.eclipse.org/projects/ee4j.authentication)
   defines a Service Provider Interface (SPI)
   ([changelog](https://github.com/eclipse-ee4j/authentication/releases)).

### Additional Spring Boot Dependency Management Overview

Another level deeper:

[![Spring Boot BOM Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Spring Boot BOM Maven Site")
 ![Spring Boot BOM Maven Site](https://img.shields.io/badge/Spring_Boot_BOM-latest_release-802C59 "Spring Boot BOM Maven Site")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom)
[![Spring Boot BOM Maven Site](https://img.shields.io/badge/Spring_Boot_BOM-current_snapshot-802C59 "Spring Boot BOM Maven Site")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom)

 * [![Spring Boot BOM Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-latest_release-802C59 "Spring Boot BOM Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom/plugin-updates-report.html)
   [![Spring Boot BOM Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-current_snapshot-802C59
      "Spring Boot BOM Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom/plugin-updates-report.html)
 * [![Spring Boot BOM Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-latest_release-802C59
      "Spring Boot BOM Property Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom/property-updates-report.html)
   [![Spring Boot BOM Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-current_snapshot-802C59
      "Spring Boot BOM Property Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom/property-updates-report.html)
 * [![Spring Boot BOM Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-latest_release-802C59
      "Spring Boot BOM Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom/dependency-updates-report.html)
   [![Spring Boot BOM Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-current_snapshot-802C59
      "Spring Boot BOM Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom/dependency-updates-report.html)

The following exclusions are also configurated:

 * [![issue 6777](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/6777.svg "issue 6777")](https://github.com/spring-projects/spring-boot/issues/6777)
   provide dependency management for Querydsl.
 * [![issue 7067](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/7067.svg "issue 7067")](https://github.com/spring-projects/spring-boot/issues/7067)
   `htmlunit` pulls in unwanted `commons-logging`
   (see also [![pull request 7064](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/7064.svg "pull request 7064")](https://github.com/spring-projects/spring-boot/pull/7064)).
 * [![issue 9496](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/9496.svg "issue 9496")](https://github.com/spring-projects/spring-boot/issues/9496)
   exclude `commons-logging` from `commons-dbcp2`.
 * [![issue 12364](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/12364.svg "issue 12364")](https://github.com/spring-projects/spring-boot/issues/12364)
   Quartz scheduler dependency conflicts on HikariCP.
 * [![issue 14060](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/14060.svg "issue 14060")](https://github.com/spring-projects/spring-boot/issues/14060) `mysql-connector-java` using old `protobuf-java`
   (see also [![pull request 14062](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/14062.svg "pull request 14062")](https://github.com/spring-projects/spring-boot/pull/14062)).
 * [![issue 14691](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/14691.svg "issue 14691")](https://github.com/spring-projects/spring-boot/issues/14691)
   use offical JSON API jar rather than Geronimo's in Artemis starter.
 * [![issue 15392](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/15392.svg "issue 15392")](https://github.com/spring-projects/spring-boot/issues/15392)
   exclude `jcl-over-slf4j` and remove use of it from spring-boot's tests. The spring-boot module's logging dependencies are necessarily quite complicated due to the logging system supporting both Logback and Log4j2.
 * [![issue 15436](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/15436.svg "issue 15436")](https://github.com/spring-projects/spring-boot/issues/15436)
   `spring-boot-starter-log4j2` fails to start due to Logback Classic being on the classpath.
 * [![pull request 19528](https://img.shields.io/github/issues/detail/state/spring-projects/spring-boot/19528.svg "pull request 19528")](https://github.com/spring-projects/spring-boot/pull/19528)
   `javax.annotation-api` in commit _Polish add Stackdriver metrics export support_
   (see also [![commit 462442e7](https://img.shields.io/badge/commit-462442e7-green "commit 462442e7")](https://github.com/spring-projects/spring-boot/commit/462442e7)).

#### Spring Boot

[![Spring Boot](https://freedumbytes.gitlab.io/setup/images/icon/spring-boot.png "Spring Boot") Spring Boot](https://spring.io/projects/spring-boot)
makes it easy to create stand-alone, production-grade Spring based Applications that you can _just run_.

 * [Release Notes](https://github.com/spring-projects/spring-boot/wiki)
   * [Spring Boot 2.0 Migration Guide](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.0-Migration-Guide)
   * [Spring Boot Config Data Migration Guide](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-Config-Data-Migration-Guide)
 * [Actuator: Production-ready Features](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html)
 * [Auto-configuration Classes](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-auto-configuration-classes.html)
 * [Dependency versions](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-dependency-versions.html)
   * **Note**: Switched to CamelCase version properties instead of Spring Boot Kebab-Case and Dot.Notation.
   * See also **Dependency Management Overview**
 * [Devtools Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#devtools-properties)
 * [Starters](https://docs.spring.io/spring-boot/docs/current/reference/html/using-spring-boot.html#using-boot-starter)
 * [Testing](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-testing)
 * [AntLib](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins.html#build-tool-plugins-antlib)
 * [Creating Your Own Auto-configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-developing-auto-configuration)
 * [Buildpacks](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-container-images-buildpacks)
 * [Configuration Metadata](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-configuration-metadata.html)
 * [Generating Your Own Metadata by Using the Annotation Processor](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-configuration-metadata.html#configuration-metadata-annotation-processor)
 * [Gradle Plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins.html#build-tool-plugins-gradle-plugin)
 * [Building Container Images](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-container-images)
   * [Dependency Management Plugin](https://docs.spring.io/dependency-management-plugin/docs/current/reference/html)
 * [The Executable Jar Format](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-executable-jar-format.html)
 * [Supporting Other Build Systems](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins.html#build-tool-plugins-other-build-systems)

[![Spring Framework](https://freedumbytes.gitlab.io/setup/images/icon/spring-framework.png "Spring Framework") Spring Framework](https://spring.io/projects/spring-framework)
provides core support for dependency injection, transaction management, web apps, data access, messaging, and more.
  * See also **Grouping Spring Dependencies**

[![Spring Data](https://freedumbytes.gitlab.io/setup/images/icon/spring-data.png "Spring Data") Spring Data](https://spring.io/projects/spring-data)
provides a consistent approach to data access - relational, non-relational, map-reduce, and beyond.

[![Spring Security](https://freedumbytes.gitlab.io/setup/images/icon/spring-security.png "Spring Security") Spring Security](https://spring.io/projects/spring-security)
protects your application with comprehensive and extensible authentication and authorization support.

[![Spring Session](https://freedumbytes.gitlab.io/setup/images/icon/spring-session.png "Spring Session") Spring Session](https://spring.io/projects/spring-session)
provides an API and implementations for managing a user's session information.

[![Spring Integration](https://freedumbytes.gitlab.io/setup/images/icon/spring-integration.png "Spring Integration") Spring Integration](https://spring.io/projects/spring-integration)
supports the well-known Enterprise Integration Patterns through lightweight messaging and declarative adapters.

[![Spring HATEOAS](https://freedumbytes.gitlab.io/setup/images/icon/spring-hateoas.png "Spring HATEOAS") Spring HATEOAS](https://spring.io/projects/spring-hateoas)
simplifies creating REST representations that follow the HATEOAS principle.

[![Spring REST Docs](https://freedumbytes.gitlab.io/setup/images/icon/spring-rest-docs.png "Spring REST Docs") Spring REST Docs](https://spring.io/projects/spring-restdocs)
lets you document RESTful services by combining hand-written documentation with auto-generated snippets produced with Spring MVC Test or REST Assured.

[![Spring Batch](https://freedumbytes.gitlab.io/setup/images/icon/spring-batch.png "Spring Batch") Spring Batch](https://spring.io/projects/spring-batch)
simplifies and optimizes the work of processing high-volume batch operations.

[![Spring AMQP](https://freedumbytes.gitlab.io/setup/images/icon/spring-amqp.png "Spring AMQP") Spring AMQP](https://spring.io/projects/spring-amqp)
applies core Spring concepts to the development of AMQP-based messaging solutions.

[![Spring Kafka](https://freedumbytes.gitlab.io/setup/images/icon/spring-kafka.png "Spring Kafka") Spring Kafka](https://spring.io/projects/spring-kafka)
provides familiar Spring abstractions for Apache Kafka.

[![Spring LDAP](https://freedumbytes.gitlab.io/setup/images/icon/spring-ldap.png "Spring LDAP") Spring LDAP](https://spring.io/projects/spring-ldap)
simplifies the development of applications that use LDAP by using Spring's familiar template-based approach.

[![Spring Web Services](https://freedumbytes.gitlab.io/setup/images/icon/spring-web-services.png "Spring Web Services") Spring Web Services](https://spring.io/projects/spring-ws)
facilitates the development of contract-first SOAP web services.

[![Spring Retry](https://freedumbytes.gitlab.io/setup/images/icon/spring.png "Spring Retry") Spring Retry](https://github.com/spring-projects/spring-retry)
provides declarative retry support for Spring applications. It is used in Spring Batch, Spring Integration, and others. Imperative retry is also supported for explicit usage.

#### Eclipse Foundation

[![Eclipse Tools Project](https://freedumbytes.gitlab.io/setup/images/icon/eclipse.png "Eclipse Tools Project") Eclipse Tools Project](https://projects.eclipse.org/projects/tools)
mission is to foster the creation of a wide variety of exemplary, extensible tools for the Eclipse Platform.

 * [![Eclipse Tools Project](https://freedumbytes.gitlab.io/setup/images/icon/eclipse.png "Eclipse Tools Project") AspectJ (AOP)](https://projects.eclipse.org/projects/tools.aspectj)
   is a seamless aspect-oriented extension to the Java programming language.
 * [![Eclipse Tools Project](https://freedumbytes.gitlab.io/setup/images/icon/eclipse.png "Eclipse Tools Project") Orbit](https://projects.eclipse.org/projects/tools.orbit)
   provides a repository of bundled versions of third party libraries that are approved for use in one or more Eclipse projects.
   * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") Jetty Orbit :: JSP API](https://mvnrepository.com/artifact/org.eclipse.jetty.orbit/javax.servlet.jsp)
     originates from the Orbit Project at Eclipse, it is an osgi bundle.

[![Eclipse RT](https://freedumbytes.gitlab.io/setup/images/icon/runtimes.png "Eclipse RT") Eclipse Runtimes (RT)](https://projects.eclipse.org/projects/rt)
is designed to foster, promote and house runtime efforts in the Eclipse Community. It is part of a larger move to drive Eclipse Equinox-based technology across a broad range of computing environments and problem domains.

 * [![Eclipse Jetty](https://freedumbytes.gitlab.io/setup/images/icon/jetty.png "Eclipse Jetty") Eclipse Jetty](https://projects.eclipse.org/projects/rt.jetty)
   provides a Web server and `javax.servlet` container, plus support for Web Sockets, OSGi, JMX, JNDI, JASPI, AJP and many other integrations.
   * [![Eclipse Jetty](https://freedumbytes.gitlab.io/setup/images/icon/jetty.png "Eclipse Jetty") Jasper JSP](https://github.com/jetty-project/jasper-jsp)
     a version of Apache Jasper with most Tomcat dependencies removed so that JSP can be used in other servlet containers.
   * [![Eclipse Jetty](https://freedumbytes.gitlab.io/setup/images/icon/jetty.png "Eclipse Jetty") ReactiveStream HttpClient](https://github.com/jetty-project/jetty-reactive-httpclient)
     a ReactiveStreams wrapper around Jetty's HttpClient.

[![Eclipse EE4J](https://freedumbytes.gitlab.io/setup/images/icon/eclipse-foundation.png "Eclipse EE4J") Eclipse Enterprise for Java (EE4J)](https://projects.eclipse.org/projects/ee4j)
is an open source initiative to create standard APIs, implementations of those APIs, and technology compatibility kits for Java runtimes that enable development, deployment, and management of server-side and cloud-native applications.

 * [![Eclipse EE4J](https://freedumbytes.gitlab.io/setup/images/icon/eclipse-foundation.png "Eclipse EE4J") JAXB Implementation](https://projects.eclipse.org/projects/ee4j.jaxb-impl)
   Jakarta XML Binding defines an API and tools that automate the mapping between XML documents and Java objects.
 * [![Eclipse EE4J](https://freedumbytes.gitlab.io/setup/images/icon/jersey.png "Eclipse EE4J") Jersey](https://projects.eclipse.org/projects/ee4j.jersey)
   is a REST framework, that provides a JAX-RS implementation, Jakarta RESTful Web Services implementation, and more.
 * [![Eclipse EE4J](https://freedumbytes.gitlab.io/setup/images/icon/eclipse-foundation.png "Eclipse EE4J") Metro](https://projects.eclipse.org/projects/ee4j.metro)
   is a high-performance, extensible, easy-to-use web service stack. It is a one-stop shop for all your web service needs, from the simplest hello world web service to reliable, secured, and transacted web service that involves .NET services.
   * [Jakarta SOAP Implementation (SAAJ)](https://github.com/eclipse-ee4j/metro-saaj) an implementation of Jakarta SOAP Specification.
   * [Mimepull](https://github.com/eclipse-ee4j/metro-mimepull) offering streaming API to access attachments parts in a MIME message.

[![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Jakarta EE](https://jakarta.ee)
is a set of specifications that enables the world wide community of java developers to work on cloud native java enterprise applications.

 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Activation](https://projects.eclipse.org/projects/ee4j.jaf)
   is a standard extension to the Java platform that lets you take advantage of standard services to: determine the type of an arbitrary piece of data; encapsulate access to it; discover the operations available on it;
   and instantiate the appropriate bean to perform the operation(s).
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Annotations](https://projects.eclipse.org/projects/ee4j.ca)
   defines a collection of annotations representing common semantic concepts that enable a declarative style of programming that applies across a variety of Java technologies.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Bean Validation](https://projects.eclipse.org/projects/ee4j.bean-validation)
   is a specification which:
   * lets you express constraints on object models via annotations.
   * lets you write custom constraints in an extensible way.
   * provides the APIs to validate objects and object graphs.
   * provides the APIs to validate parameters and return values of methods and constructors.
   * reports the set of violations (localized).
   * runs on Java SE but is integrated in Java EE.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Expression Language (EL)](https://projects.eclipse.org/projects/ee4j.el)
   provides an important mechanism for enabling the presentation layer (web pages) to communicate with the application logic (managed beans).
   The EL is used by several JavaEE technologies, such as JavaServer Faces technology, JavaServer Pages (JSP) technology, and Contexts and Dependency Injection for Java EE (CDI).
   The EL can also be used in stand-alone environments.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") JSON Binding (JSON-B)](https://projects.eclipse.org/projects/ee4j.jsonb)
   is a standard binding layer for converting Java objects to/from JSON messages.
   It defines a default mapping algorithm for converting existing Java classes to JSON, while enabling developers to customize the mapping process through the use of Java annotations.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") JSON Processing (JSON-P)](https://projects.eclipse.org/projects/ee4j.jsonp)
   is a Java API to process (e.g. parse, generate, transform and query) JSON documents.
   It produces and consumes JSON in a streaming fashion (similar to StAX API for XML) and allows to build a Java object model for JSON using API classes (similar to DOM API for XML).
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Mail](https://projects.eclipse.org/projects/ee4j.mail)
   defines a platform-independent and protocol-independent framework to build mail and messaging applications.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Java Message Service (JMS)](https://projects.eclipse.org/projects/ee4j.jms)
   is a Java Message Oriented Middleware API for sending messages between two or more clients. It is a programming model to handle the producer-consumer messaging problem.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Java Persistence API (JPA)](https://projects.eclipse.org/projects/ee4j.jpa)
   is the Java API for the management of persistence and object/relational mapping in Jakarta EE and Java SE environments.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") RESTful Web Services (JAX-RS)](https://projects.eclipse.org/projects/ee4j.jaxrs)
   provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Java Servlets](https://projects.eclipse.org/projects/ee4j.servlet)
   is a standard technology for interacting with the web on the Jakarta EE platform.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") JavaServer Pages Standard Tag Library (JSTL)](https://projects.eclipse.org/projects/ee4j.jstl)
   encapsulates as simple tags the core functionality common to many Web applications.
   JSTL has support for common, structural tasks such as iteration and conditionals, tags for manipulating XML documents, internationalization tags, and SQL tags.
   It also provides a framework for integrating existing custom tags with JSTL tags.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Java Transaction API (JTA)](https://projects.eclipse.org/projects/ee4j.jta)
   specifies standard Java interfaces between a transaction manager and the parties involved in a distributed transaction system: the resource manager, the application server, and the transactional applications.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Java API for WebSocket](https://projects.eclipse.org/projects/ee4j.websocket)
   specifies the API that Java developers can use when they want to integrate WebSockets into their applications - both on the server side as well as on the Java client side.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") XML Binding (JAXB)](https://projects.eclipse.org/projects/ee4j.jaxb)
   defines an API and tools that automate the mapping between XML documents and Java objects.
 * [![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") XML Web Services (JAX-WS)](https://projects.eclipse.org/projects/ee4j.jaxws)
   is a Java programming language API for creating web services, particularly SOAP services. Jakarta XML Web Services is one of the Java XML programming APIs.
   * [SOAP with Attachments](https://github.com/eclipse-ee4j/saaj-api) defines an API enabling developers to produce and consume messages conforming to the SOAP 1.1, SOAP 1.2, and SOAP Attachments Feature.

#### Java EE Platform Specification

[![Java EE](https://freedumbytes.gitlab.io/setup/images/icon/java.png "Java EE") Java Platform, Enterprise Edition (Java EE)](https://javaee.github.io/javaee-spec)
is the standard in community-driven enterprise software. This project is now part of the above-mentioned Eclipse EE4J initiative.

 * [JavaBeans Activation Framework (JAF)](https://github.com/javaee/activation)
 * [Java Common Annotations (JSR-250)](https://github.com/javaee/javax.annotation)
 * [JCache (JSR-107)](https://github.com/jsr107/jsr107spec)
 * [Bean Validation (JSR-380)](https://beanvalidation.org)
 * [JSON Binding (JSON-B) (JSR-367)](https://github.com/javaee/jsonb-spec)
 * [JSON Processing (JSON-P) (JSR-374)](https://github.com/javaee/jsonp)
 * [JavaMail](https://github.com/javaee/javamail)
 * [Java Message Service (JMS)](https://github.com/javaee/javax.jms)
 * [Money and Currency (JSR-354)](https://github.com/JavaMoney/jsr354-api)
 * [Java Persistence API (JPA) (JSR-338)](https://github.com/javaee/jpa-spec)
 * [Java Servlets](https://github.com/javaee/servlet-spec)
 * [JavaServer Pages Standard Tag Library (JSTL)](https://github.com/javaee/jstl-api)
 * [Java Transaction API (JTA)](https://github.com/javaee/javax.transaction)
 * [Java API for WebSocket (JSR-356)](https://github.com/javaee/websocket-spec)
 * [Java Architecture for XML Binding (JAXB)](https://github.com/javaee/jaxb-spec)
 * [Java XML Web Services (JAX-WS) (JSR-224)](https://github.com/javaee/jax-ws-spec)

#### Programming Language

[![Apache Groovy](https://freedumbytes.gitlab.io/setup/images/icon/groovy.png "Apache Groovy") Apache Groovy](https://groovy-lang.org)
is a powerful, optionally typed and dynamic language, with static-typing and static compilation capabilities, for the Java platform aimed at improving developer productivity thanks to a concise, familiar and easy to learn syntax.
It integrates smoothly with any Java program, and immediately delivers to your application powerful features, including scripting capabilities, Domain-Specific Language authoring, runtime and compile-time meta-programming and functional programming.

[![Kotlin](https://freedumbytes.gitlab.io/setup/images/icon/kotlin.png "Kotlin") Kotlin](https://kotlinlang.org)
a modern programming language that makes developers happier.

 * [![Kotlin](https://freedumbytes.gitlab.io/setup/images/icon/kotlin.png "Kotlin") kotlinx.coroutines](https://github.com/Kotlin/kotlinx.coroutines) with multiplatform support.

[![ReactiveX](https://freedumbytes.gitlab.io/setup/images/icon/reactivex.png "ReactiveX") ReactiveX](http://reactivex.io)
an API for asynchronous programming with observable streams.

 * [![ReactiveX](https://freedumbytes.gitlab.io/setup/images/icon/reactivex.png "ReactiveX") RxJava Reactive Extensions for the JVM](https://github.com/ReactiveX/RxJava)
 * [![ReactiveX](https://freedumbytes.gitlab.io/setup/images/icon/reactivex.png "ReactiveX") RxJava Reactive Streams](https://github.com/ReactiveX/RxJavaReactiveStreams)

[![Project Reactor](https://freedumbytes.gitlab.io/setup/images/icon/project-reactor.png "Project Reactor") Project Reactor](https://projectreactor.io)
is a fourth-generation reactive library, based on the Reactive Streams specification, for building non-blocking applications on the JVM.

[![RSocket](https://freedumbytes.gitlab.io/setup/images/icon/rsocket.png "RSocket") RSocket](https://rsocket.io)
application protocol providing Reactive Streams semantics.

#### Common Utilities

[![Project Lombok](https://freedumbytes.gitlab.io/setup/images/icon/lombok.png "Project Lombok") Project Lombok](https://projectlombok.org)
is a java library that automatically plugs into your editor and build tools, spicing up your java.
Never write another getter or equals method again, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.
 * See also **Common Dependencies**

[![Apache Commons Lang](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons Lang") Apache Commons Lang](https://commons.apache.org/proper/commons-lang)
provides a host of helper utilities for the `java.lang` API, notably String manipulation methods, basic numerical methods, object reflection, concurrency, creation and serialization and System properties.
 * See also **Common Dependencies**

[![Apache Commons Codec](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons Codec") Apache Commons Codec](https://commons.apache.org/proper/commons-codec)
provides implementations of common encoders and decoders such as Base64, Hex, Phonetic and URLs.
 * See also **Common Dependencies**

#### Reflection

[![ANTLR](https://freedumbytes.gitlab.io/setup/images/icon/antlr.png "ANTLR") ANTLR (ANother Tool for Language Recognition)](https://www.antlr.org)
is a powerful parser generator for reading, processing, executing, or translating structured text or binary files. It's widely used to build languages, tools, and frameworks.

[![ClassMate](https://freedumbytes.gitlab.io/setup/images/icon/fasterxml.png "ClassMate") ClassMate](https://github.com/FasterXML/java-classmate)
is a zero-dependency Java library for accurately introspecting type information, including reliable resolution of generic type declarations for both classes (_types_) and members (_fields_, _methods_ and _constructors_).

#### JSON / XML / YAML

[![Gson](https://freedumbytes.gitlab.io/setup/images/icon/google.png "Gson") Gson](https://github.com/google/gson)
is a Java library that can be used to convert Java Objects into their JSON representation.

[![Jackson](https://freedumbytes.gitlab.io/setup/images/icon/fasterxml.png "Jackson") Jackson](https://github.com/FasterXML/jackson)
is a suite of data-processing tools for Java (and the JVM platform), including the flagship streaming JSON parser/generator library, matching data-binding library (POJOs to and from JSON)
and additional data format modules to process data encoded in Avro, BSON, CBOR, CSV, Smile, (Java) Properties, Protobuf, XML or YAML the Aalto non-blocking XML parser, and a growing family of utility libraries and extensions.

[![Jaxen](https://freedumbytes.gitlab.io/setup/images/icon/github.png "Jaxen") Jaxen](https://github.com/jaxen-xpath/jaxen)
is a universal XPath engine for Java.

[![JDOM](https://freedumbytes.gitlab.io/setup/images/icon/github.png "JDOM") JDOM](http://jdom.org)
is, quite simply, a Java representation of an XML document.
JDOM provides a way to represent that document for easy and efficient reading, manipulation, and writing. It has a straightforward API, is a lightweight and fast, and is optimized for the Java programmer.
It's an alternative to DOM and SAX, although it integrates well with both DOM and SAX.

[![Apache Johnzon](https://freedumbytes.gitlab.io/setup/images/icon/johnzon.png "Apache Johnzon") Apache Johnzon](https://johnzon.apache.org)
 is a project providing an implementation of JsonProcessing (aka JSR-353) and a set of useful extension for this specification like an Object mapper,
 some JAX-RS providers and a WebSocket module provides a basic integration with Java WebSocket API (JSR-356).

[![Jayway JsonPath](https://freedumbytes.gitlab.io/setup/images/icon/jayway.png "Jayway JsonPath") Jayway JsonPath](https://github.com/json-path/JsonPath)
a Java DSL for reading JSON documents.

[![JSON Smart](https://freedumbytes.gitlab.io/setup/images/icon/github.png "JSON Smart") JSON Smart](https://github.com/netplex/json-smart-v2)
a Small and Fast Parser.

[![WSDL4J](https://freedumbytes.gitlab.io/setup/images/icon/sourceforge.png "WSDL4J") WSDL4J](https://sourceforge.net/p/wsdl4j/wiki/Home)
Java Toolkit for the Web Services Description Language allows the creation, representation, and manipulation of WSDL documents.

[![YAML](https://freedumbytes.gitlab.io/setup/images/icon/yaml.png "YAML") YAML](https://yaml.org/refcard.html)
is a data serialization format designed for human readability and interaction with scripting language.

 * [![SnakeYAML](https://freedumbytes.gitlab.io/setup/images/icon/sid.png "SnakeYAML") SnakeYAML](https://bitbucket.org/asomov/snakeyaml/wiki/Documentation) library to serialize Java objects to YAML documents and vice versa.

#### Logging

[![JBoss Logging Tools](https://freedumbytes.gitlab.io/setup/images/icon/jboss.png "JBoss Logging Tools") JBoss Logging Tools](https://github.com/jboss-logging/jboss-logging-tools)
are used to create internationalized log statements and exceptions.

 * [![JBoss Logging Framework](https://freedumbytes.gitlab.io/setup/images/icon/jboss-logging.png "JBoss Logging Framework") jboss-logging framework](https://github.com/jboss-logging/jboss-logging)
   * [![JBoss Logging Guide](https://freedumbytes.gitlab.io/setup/images/icon/jboss.png "JBoss Logging Guide") Guide](https://docs.jboss.org/hibernate/orm/current/topical/html_single/logging/Logging.html) for the JBoss Logging library.

[![Apache Log4j2](https://freedumbytes.gitlab.io/setup/images/icon/log4j2.png "Apache Log4j2") Apache Log4j2](https://logging.apache.org/log4j)
is an upgrade to Log4j that provides significant improvements over its predecessor. It also provides many of the improvements available in Logback, while fixing some inherent problems in Logback's architecture.

An unauthenticated remote code execution exists in the popular Java library Apache Log4j2:

 * [![Tech Solvency](https://freedumbytes.gitlab.io/setup/images/icon/techsolvency.png "Tech Solvency") Log4Shell: The Story So Far](https://www.techsolvency.com/story-so-far/cve-2021-44228-log4j-log4shell).
 * [![New Tech Forum](https://freedumbytes.gitlab.io/setup/images/icon/infoworld.png "New Tech Forum") How to detect the Log4j vulnerability in your applications](https://www.infoworld.com/article/3644492/how-to-detect-the-log4j-vulnerability-in-your-applications.html).
 * [![Microsoft 365 Defender](https://freedumbytes.gitlab.io/setup/images/icon/microsoft.png "Microsoft 365 Defender") Guidance for preventing, detecting, and hunting for exploitation of the Log4j 2 vulnerability](https://www.microsoft.com/security/blog/2021/12/11/guidance-for-preventing-detecting-and-hunting-for-cve-2021-44228-log4j-2-exploitation).
 * [![SLF4J](https://freedumbytes.gitlab.io/setup/images/icon/qos.png "SLF4J") Comments on the log4shell(CVE-2021-44228) vulnerability](https://www.slf4j.org/log4shell.html).

[![Reload4j](https://freedumbytes.gitlab.io/setup/images/icon/qos.png "Reload4j") Reload4j](https://reload4j.qos.ch) is a fork of Apache Log4j version `1.2.17` with the goal of fixing pressing security issues.
It is intended as a drop-in replacement for Log4j version `1.2.17`.

[![Logback](https://freedumbytes.gitlab.io/setup/images/icon/qos.png "Logback") Logback](https://logback.qos.ch/reasonsToSwitch.html)
is intended as a successor to the popular Log4j project, picking up where Log4j leaves off.

[![SLF4J](https://freedumbytes.gitlab.io/setup/images/icon/qos.png "SLF4J") Simple Logging Facade for Java (SLF4J)](http://slf4j.org)
serves as a simple facade or abstraction for various logging frameworks (e.g. `java.util.logging` (JUL), Logback, Log4j) allowing the end user to plug in the desired logging framework at deployment time.

#### Transaction Management

[![Atomikos](https://freedumbytes.gitlab.io/setup/images/icon/atomikos.png "Atomikos") Atomikos](https://atomikos.com)
TransactionsEssentials is open source transaction management with JTA/XA and connection pooling for self-contained applications outside of the application server, ideal for your cloud.

[![Bitronix](https://freedumbytes.gitlab.io/setup/images/icon/github.png "Bitronix") Bitronix Transaction Manager (BTM)](https://github.com/bitronix/btm)
is a simple but complete implementation of the JTA 1.1 API.
It is a fully working XA transaction manager that provides all services required by the JTA API while trying to keep the code as simple as possible for easier understanding of the XA semantics.

[![Narayana](https://freedumbytes.gitlab.io/setup/images/icon/narayana.png "Narayana") Narayana](https://narayana.io)
is a transactions toolkit which provides support for applications developed using a broad range of standards-based transaction protocols: JTA, JTS, Web-Service Transactions, REST Transactions, STM and XATMI/TX.

 * [![Narayana](https://freedumbytes.gitlab.io/setup/images/icon/narayana.png "Narayana") SPI for integrating the Narayana transaction service](https://github.com/jbosstm/jboss-transaction-spi)
   * [![JBoss](https://freedumbytes.gitlab.io/setup/images/icon/jboss.png "JBoss") Narayana JDBC integration for Tomcat](https://jbossts.blogspot.com/2018/05/narayana-jdbc-integration-for-tomcat.html)

#### Database Connection Pools

[![Apache Commons DBCP](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons DBCP") Apache Commons DBCP](https://commons.apache.org/proper/commons-dbcp)
to share a pool of open connections between all of the application's current users.
 * See also **Grouping Test Dependencies**

[![HikariCP](https://freedumbytes.gitlab.io/setup/images/icon/hikari.png "HikariCP") HikariCP](https://github.com/brettwooldridge/HikariCP)
is a zero-overhead production ready JDBC connection pool.
 * See also **Grouping MySQL Dependencies**

[![PooledJMS](https://freedumbytes.gitlab.io/setup/images/icon/github.png "PooledJMS") PooledJMS](https://github.com/messaginghub/pooled-jms)
a JMS Connection pool for messaging applications that provides pooling for JMS Connections, Sessions and MessageProducers. This project was forked from the ActiveMQ JMS Pool and enhanced to provide JMS 2.0 functionality.

#### Database

[![Couchbase](https://freedumbytes.gitlab.io/setup/images/icon/couchbase.png "Couchbase") Couchbase](https://couchbase.com)
is an award-winning distributed NoSQL cloud database. It delivers unmatched versatility, performance, scalability, and financial value across cloud, on-premises, hybrid, distributed cloud, and edge computing deployments.

 * [![Couchbase Java SDK](https://freedumbytes.gitlab.io/setup/images/icon/couchbase.png "Couchbase Java SDK") Couchbase Java SDK](https://docs.couchbase.com/java-sdk/current/hello-world/start-using-sdk.html)

[![Datastax](https://freedumbytes.gitlab.io/setup/images/icon/datastax.png "Datastax") Datastax](https://docs.datastax.com/en/developer/java-driver/latest)
open-source, scale-out, cloud-native NoSQL database. DataStax is a hybrid database-as-a-service built on Apache Cassandra.

 * [![Datastax Java Driver](https://freedumbytes.gitlab.io/setup/images/icon/datastax.png "Datastax Java Driver") Datastax Java Driver](https://docs.datastax.com/en/developer/java-driver/latest)

[![Db2](https://freedumbytes.gitlab.io/setup/images/icon/ibm.png "Db2") Db2](https://ibm.com/products/db2-database)
powered by AI and built for deeper insights, available both on premises and on the cloud.

 * [![JCC](https://freedumbytes.gitlab.io/setup/images/icon/ibm.png "JCC") DB2 JDBC Driver (JCC)](https://ibm.com/support/pages/db2-jdbc-driver-versions-and-downloads)

[![Derby](https://freedumbytes.gitlab.io/setup/images/icon/derby.png "Derby") Derby](https://db.apache.org/derby)
is an open source relational database implemented entirely in Java.

[![Firebird](https://freedumbytes.gitlab.io/setup/images/icon/firebird.png "Firebird") Firebird](https://firebirdsql.org/)
true universal open source database.

 * [![Jaybird](https://freedumbytes.gitlab.io/setup/images/icon/firebird.png "Jaybird") Jaybird JCA/JDBC Driver](https://firebirdsql.org/en/devel-jdbc-driver)

[![H2](https://freedumbytes.gitlab.io/setup/images/icon/h2.png "H2") H2](https://h2database.com/html/main.html)
is an embeddable RDBMS written in Java.
 * See also **Grouping Test Dependencies**

[![HSQLDB](https://freedumbytes.gitlab.io/setup/images/icon/hsqldb.png "HSQLDB") HyperSQL DataBase (HSQLDB)](http://hsqldb.org)
management system for development, testing and deployment of database applications.
 * See also **Grouping Test Dependencies**

[![LDAP](https://freedumbytes.gitlab.io/setup/images/icon/ldap.png "LDAP") Lightweight Directory Access Protocol (LDAP)](https://ldap.com)
is a mature, flexible, and well supported standards-based mechanism for interacting with directory servers.
It's often used for authentication and storing information about users, groups, and applications, but an LDAP directory server is a fairly general-purpose data store and can be used in a wide variety of applications.

 * [![UnboundID LDAP SDK](https://freedumbytes.gitlab.io/setup/images/icon/pingidentity.png "UnboundID LDAP SDK") UnboundID LDAP SDK for Java](https://github.com/pingidentity/ldapsdk)

[![MariaDB](https://freedumbytes.gitlab.io/setup/images/icon/mariadb.png "MariaDB") MariaDB](https://mariadb.com)
frees companies from the costs, constraints and complexity of proprietary databases, enabling them to reinvest in what matters most - developing innovative, customer-facing applications rapidly.

 * [![MariaDB](https://freedumbytes.gitlab.io/setup/images/icon/mariadb.png "MariaDB") MariaDB Connector/J](https://mariadb.com/kb/en/about-mariadb-connector-j)

[![MSSQL](https://freedumbytes.gitlab.io/setup/images/icon/microsoft.png "MSSQL") Microsoft SQL Server](https://microsoft.com/sql-server)
Unleash the power in your data. Reimagine the realm of possibility.
Microsoft data platform solutions release the potential hidden in your data - whether it's on-premises, in the cloud, or at the edge - and reveal insights and opportunities to transform your business.

 * [![MSSQL](https://freedumbytes.gitlab.io/setup/images/icon/microsoft.png "MSSQL") Microsoft JDBC Driver](https://github.com/Microsoft/mssql-jdbc)
 * [![jTDS](https://freedumbytes.gitlab.io/setup/images/icon/jtds.png "jTDS") JDBC driver for Microsoft SQL Server and Sybase ASE (jTDS)](http://jtds.sourceforge.net)

[![MongoDB](https://freedumbytes.gitlab.io/setup/images/icon/mongodb.png "MongoDB") MongoDB](https://mongodb.com)
is a general purpose, document-based, distributed database built for modern application developers and for the cloud era.

 * [![MongoDB](https://freedumbytes.gitlab.io/setup/images/icon/mongodb.png "MongoDB") MongoDB Java Drivers](https://mongodb.github.io/mongo-java-driver)
 * [![Embedded MongoDB](https://freedumbytes.gitlab.io/setup/images/icon/flapdoodle.png "Embedded MongoDB") Embedded MongoDB](https://github.com/flapdoodle-oss/de.flapdoodle.embed.mongo)
   will provide a platform neutral way for running mongodb in unittests.

[![MySQL](https://freedumbytes.gitlab.io/setup/images/icon/mysql.png "MySQL") MySQL](https://mysql.com) Standard Edition includes InnoDB, making it a fully integrated transaction-safe, ACID compliant database.

 * [![MySQL](https://freedumbytes.gitlab.io/setup/images/icon/mysql.png "MySQL") MySQL Connector/J](https://github.com/mysql/mysql-connector-j)
   * See also **Grouping MySQL Dependencies**

[![Neo4j](https://freedumbytes.gitlab.io/setup/images/icon/neo4j.png "Neo4j") Neo4j](https://neo4j.com)
is a native graph database, built from the ground up to leverage not only data but also data relationships. Neo4j connects data as it's stored, enabling queries never before imagined, at speeds never thought possible.

 * [![Neo4j](https://freedumbytes.gitlab.io/setup/images/icon/neo4j.png "Neo4j") Neo4j Java Driver](https://github.com/neo4j/neo4j-java-driver)

[![Oracle](https://freedumbytes.gitlab.io/setup/images/icon/oracle.png "Oracle") Oracle Database](https://oracle.com/database)
products offer customers cost-optimized and high-performance versions of Oracle Database, the world's leading converged, multi-model database management system, as well as in-memory, NoSQL and MySQL databases.
Oracle Autonomous Database, available on premises via Oracle Cloud@Customer or in the Oracle Cloud Infrastructure, enables customers to simplify relational database environments and reduce management workloads.

 * [![Oracle](https://freedumbytes.gitlab.io/setup/images/icon/oracle.png "Oracle") Oracle JDBC](https://oracle.com/database/technologies/appdev/jdbc.html)

[![PostgreSQL](https://freedumbytes.gitlab.io/setup/images/icon/postgresql.png "PostgreSQL") PostgreSQL](https://postgresql.org)
the World's Most Advanced Open Source Relational Database.

 * [![PostgreSQL](https://freedumbytes.gitlab.io/setup/images/icon/postgresql.png "PostgreSQL") PostgreSQL JDBC Driver](https://jdbc.postgresql.org)

[![R2DBC](https://freedumbytes.gitlab.io/setup/images/icon/r2dbc.png "R2DBC") Reactive Relational Database Connectivity (R2DBC)](https://r2dbc.io)
project brings reactive programming APIs to relational databases.

 * [![Cloud Spanner](https://freedumbytes.gitlab.io/setup/images/icon/google-cloud.png "Cloud Spanner")
    Cloud Spanner R2DBC Driver](https://github.com/GoogleCloudPlatform/cloud-spanner-r2dbc)
 * [![H2](https://freedumbytes.gitlab.io/setup/images/icon/h2.png "H2")
    Reactive Relational Database Connectivity H2 Implementation](https://github.com/r2dbc/r2dbc-h2)
 * [![MariaDB](https://freedumbytes.gitlab.io/setup/images/icon/mariadb.png "MariaDB")
    MariaDB R2DBC connector](https://github.com/mariadb-corporation/mariadb-connector-r2dbc)
 * [![MSSQL](https://freedumbytes.gitlab.io/setup/images/icon/microsoft.png "MSSQL")
    Reactive Relational Database Connectivity Microsoft SQL Server Implementation](https://github.com/r2dbc/r2dbc-mssql)
 * [![MySQL](https://freedumbytes.gitlab.io/setup/images/icon/mysql.png "MySQL")
    Reactive Relational Database Connectivity MySQL Implementation](https://github.com/mirromutth/r2dbc-mysql)
 * [![PostgreSQL](https://freedumbytes.gitlab.io/setup/images/icon/postgresql.png "PostgreSQL")
    PostgreSQL R2DBC Driver](https://github.com/pgjdbc/r2dbc-postgresql)

[![SAP ASE](https://freedumbytes.gitlab.io/setup/images/icon/sap.png "SAP ASE") SAP Adaptive Server Enterprise (ASE)](https://sap.com/products/sybase-ase.html)
originally known as Sybase SQL Server, and also commonly known as Sybase DB or Sybase ASE, is a relational model database server developed by Sybase Corporation, which later became part of SAP AG.

 * [![jTDS](https://freedumbytes.gitlab.io/setup/images/icon/jtds.png "jTDS") JDBC driver for Microsoft SQL Server and Sybase ASE (jTDS)](http://jtds.sourceforge.net)

[![SQLite](https://freedumbytes.gitlab.io/setup/images/icon/sqlite.png "SQLite") SQLite](https://sqlite.org)
is a C-language library that implements a small, fast, self-contained, high-reliability, full-featured, SQL database engine.
SQLite is the most used database engine in the world. SQLite is built into all mobile phones and most computers and comes bundled inside countless other applications that people use every day.

 * [![SQLite](https://freedumbytes.gitlab.io/setup/images/icon/sqlite.png "SQLite") SQLite JDBC Driver](https://github.com/xerial/sqlite-jdbc)

#### Database Querying

[![Hibernate ORM](https://freedumbytes.gitlab.io/setup/images/icon/hibernate.png "Hibernate ORM") Hibernate Object/Relational Mapping (ORM)](https://hibernate.org/orm)
enables developers to more easily write applications whose data outlives the application process.
 * See also **Grouping Hibernate Dependencies**

[![Hibernate Validator](https://freedumbytes.gitlab.io/setup/images/icon/hibernate.png "Hibernate Validator") Hibernate Validator](https://hibernate.org/validator)
allows to express and validate application constraints. The default metadata source are annotations, with the ability to override and extend through the use of XML.
It is not tied to a specific application tier or programming model and is available for both server and client application programming.

[![jOOQ](https://freedumbytes.gitlab.io/setup/images/icon/jooq.png "jOOQ") Java Object Oriented Querying (jOOQ)](https://jooq.org)
generates Java code from your database and creates type safe SQL queries through its fluent API.

[![Querydsl](https://freedumbytes.gitlab.io/setup/images/icon/querydsl.png "Querydsl") Querydsl](http://querydsl.com)
Unified Queries for Java.

#### Database Versioning

[![Flyway](https://freedumbytes.gitlab.io/setup/images/icon/flyway.png "Flyway") Flyway](https://flywaydb.org)
version control for your database. Robust schema evolution across all your environments. With ease, pleasure and plain SQL.

[![Liquibase](https://freedumbytes.gitlab.io/setup/images/icon/liquibase.png "Liquibase") Liquibase](https://liquibase.org)
track, version, and deploy database changes.

#### Scheduling

[![Quartz](https://freedumbytes.gitlab.io/setup/images/icon/quartz.png "Quartz") Quartz](http://quartz-scheduler.org)
is a richly featured, open source job scheduling library that can be integrated within virtually any Java application - from the smallest stand-alone application to the largest e-commerce system.

#### Searching

[![Elasticsearch](https://freedumbytes.gitlab.io/setup/images/icon/elastic.png "Elasticsearch") Elasticsearch](https://elastic.co/elasticsearch)
is a distributed, RESTful search and analytics engine capable of addressing a growing number of use cases.

[![InfluxDB](https://freedumbytes.gitlab.io/setup/images/icon/influxdata.png "InfluxDB") InfluxDB](https://influxdata.com)
empowers developers to build IoT, analytics and monitoring software. It is purpose-built to handle the massive volumes and countless sources of time-stamped data produced by sensors, applications and infrastructure.

[![Solr](https://freedumbytes.gitlab.io/setup/images/icon/solr.png "Solr") Solr](https://lucene.apache.org/solr)
is the popular, blazing-fast, open source enterprise search platform built on Apache Lucene.

#### Queueing

[![ActiveMQ](https://freedumbytes.gitlab.io/setup/images/icon/activemq.png "ActiveMQ") ActiveMQ](https://activemq.apache.org)
is the most popular open source, multi-protocol, Java-based messaging server.

 * [![ActiveMQ](https://freedumbytes.gitlab.io/setup/images/icon/activemq.png "ActiveMQ") Classic](https://activemq.apache.org/components/classic)
 * [![ActiveMQ](https://freedumbytes.gitlab.io/setup/images/icon/activemq.png "ActiveMQ") Artemis](https://activemq.apache.org/components/artemis)

[![Apache Kafka](https://freedumbytes.gitlab.io/setup/images/icon/kafka.png "Apache Kafka") Apache Kafka](https://kafka.apache.org)
is an open-source distributed event streaming platform for high-performance data pipelines, streaming analytics, data integration, and mission-critical applications.

[![RabbitMQ Java Client](https://freedumbytes.gitlab.io/setup/images/icon/rabbitmq.png "RabbitMQ Java Client") RabbitMQ Java Client](https://rabbitmq.com/java-client.html)
allows Java and JVM-based applications to connect to and interact with RabbitMQ nodes.

[![Reactive Streams](https://freedumbytes.gitlab.io/setup/images/icon/github.png "Reactive Streams") Reactive Streams](http://reactive-streams.org)
is an initiative to provide a standard for asynchronous stream processing with non-blocking back pressure. This encompasses efforts aimed at runtime environments (JVM and JavaScript) as well as network protocols.

#### Caching

[![Caffeine](https://freedumbytes.gitlab.io/setup/images/icon/caffeine.png "Caffeine") Caffeine](https://github.com/ben-manes/caffeine)
is a high performance, near optimal caching library based on Java 8.

[![Apache Commons Pool](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache Commons Pool") Apache Commons Pool](https://commons.apache.org/proper/commons-pool)
open source software library provides an object-pooling API and a number of object pool implementations.

[![Ehcache](https://freedumbytes.gitlab.io/setup/images/icon/ehcache.png "Ehcache") Ehcache](https://www.ehcache.org)
is an open source, standards-based cache that boosts performance, offloads your database, and simplifies scalability.
 * See also **Grouping Hibernate Dependencies**

[![Hazelcast](https://freedumbytes.gitlab.io/setup/images/icon/hazelcast.png "Hazelcast") Hazelcast](https://hazelcast.org)
is an open-source distributed in-memory data store and computation platform.

 * [![Hazelcast](https://freedumbytes.gitlab.io/setup/images/icon/hazelcast.png "Hazelcast") Hibernate Second Level Cache](https://github.com/hazelcast/hazelcast-hibernate)

[![Infinispan](https://freedumbytes.gitlab.io/setup/images/icon/infinispan.png "Infinispan") Infinispan](https://infinispan.org)
is an open-source in-memory data grid that offers flexible deployment options and robust capabilities for storing, managing, and processing data.
Infinispan provides a key/value data store that can hold all types of data, from Java objects to plain text.
Infinispan distributes your data across elastically scalable clusters to guarantee high availability and fault tolerance, whether you use Infinispan as a volatile cache or a persistent data store.

 * [![Infinispan](https://freedumbytes.gitlab.io/setup/images/icon/infinispan.png "Infinispan") Console](https://github.com/infinispan/infinispan-console)
 * [![Infinispan](https://freedumbytes.gitlab.io/setup/images/icon/infinispan.png "Infinispan") ProtoStream](https://github.com/infinispan/protostream)

#### Templating

[![Apache FreeMarker](https://freedumbytes.gitlab.io/setup/images/icon/freemarker.png "Apache FreeMarker") Apache FreeMarker](https://freemarker.apache.org)
is a template engine: a Java library to generate text output (HTML web pages, e-mails, configuration files, source code, etc.) based on templates and changing data.

[![Mustache](https://freedumbytes.gitlab.io/setup/images/icon/mustache.png "Mustache") Mustache](https://mustache.github.io)
logic-less templates, works great with TextMate, Vim, Emacs, Coda, and Atom.

[![Thymeleaf](https://freedumbytes.gitlab.io/setup/images/icon/thymeleaf.png "Thymeleaf") Thymeleaf](https://www.thymeleaf.org)
bring elegant natural templates to your development workflow - HTML that can be correctly displayed in browsers and also work as static prototypes, allowing for stronger collaboration in development teams.

 * [![Thymeleaf](https://freedumbytes.gitlab.io/setup/images/icon/thymeleaf.png "Thymeleaf") Thymeleaf Extras Java8Time](https://github.com/thymeleaf/thymeleaf-extras-java8time)
 * [![Thymeleaf](https://freedumbytes.gitlab.io/setup/images/icon/thymeleaf.png "Thymeleaf") Thymeleaf Extras Spring Security](https://github.com/thymeleaf/thymeleaf-extras-springsecurity)
 * [![Thymeleaf](https://freedumbytes.gitlab.io/setup/images/icon/github.png "Thymeleaf") Thymeleaf Extras Data Attribute](https://github.com/mxab/thymeleaf-extras-data-attribute)
 * [![Thymeleaf](https://freedumbytes.gitlab.io/setup/images/icon/github.png "Thymeleaf") Thymeleaf Layout Dialect](https://github.com/ultraq/thymeleaf-layout-dialect)

#### HTTP Protocol

[![Apache HttpComponents](https://freedumbytes.gitlab.io/setup/images/icon/httpcomponents.png "Apache HttpComponents") Apache HttpComponents](https://hc.apache.org)
a toolset of low level Java components focused on HTTP and associated protocols.

 * [![Apache HttpComponents](https://freedumbytes.gitlab.io/setup/images/icon/httpcomponents.png "Apache HttpComponents") HttpCore NIO](https://hc.apache.org/httpcomponents-core-ga)
 * [![Apache HttpComponents](https://freedumbytes.gitlab.io/setup/images/icon/httpcomponents.png "Apache HttpComponents") HttpClient](https://hc.apache.org/httpcomponents-client-ga)
 * [![Apache HttpComponents](https://freedumbytes.gitlab.io/setup/images/icon/httpcomponents.png "Apache HttpComponents") HttpAsyncClient](https://hc.apache.org/httpcomponents-asyncclient-4.1.x)
   * **Note**: HttpAsyncClient 5.0 development moved to HttpComponents HttpClient project.

[![OkHttp](https://freedumbytes.gitlab.io/setup/images/icon/square.png "OkHttp") OkHttp](https://square.github.io/okhttp)
perseveres when the network is troublesome: it will silently recover from common connection problems. OkHttp includes a library for testing HTTP, HTTPS, and HTTP/2 clients.

#### Mailing

[![SendGrid](https://freedumbytes.gitlab.io/setup/images/icon/sendgrid.png "SendGrid") SendGrid](https://sendgrid.com)
send email newsletters, password resets, promotional emails and shipping notifications with confidence.

#### Platform

[![AppEngine SDK](https://freedumbytes.gitlab.io/setup/images/icon/google-cloud.png "AppEngine SDK") AppEngine SDK](https://cloud.google.com/appengine)
build highly scalable applications on a fully managed serverless platform.

[![Netty](https://freedumbytes.gitlab.io/setup/images/icon/netty.png "Netty") Netty](https://netty.io)
is an asynchronous event-driven network application framework for rapid development of maintainable high performance protocol servers & clients.

 * [![Netty](https://freedumbytes.gitlab.io/setup/images/icon/netty.png "Netty") Forked Tomcat Native](https://github.com/netty/netty/wiki/Forked-Tomcat-Native)
   * [BoringSSL](https://boringssl.googlesource.com/boringssl)

[![Apache Tomcat](https://freedumbytes.gitlab.io/setup/images/icon/tomcat.png "Apache Tomcat") Apache Tomcat](https://tomcat.apache.org)
is an open source implementation of the Java Servlets, JavaServer Pages, Java Expression Language and Java WebSocket technologies.

[![Undertow](https://freedumbytes.gitlab.io/setup/images/icon/undertow.png "Undertow") Undertow](https://undertow.io)
is a flexible performant web server written in java, providing both blocking and non-blocking API's based on NIO.

#### Store

[![Redis](https://freedumbytes.gitlab.io/setup/images/icon/redis.png "Redis") Redis](https://redis.io)
is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker.
It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs, geospatial indexes with radius queries and streams.
Redis has built-in replication, Lua scripting, LRU eviction, transactions and different levels of on-disk persistence, and provides high availability via Redis Sentinel and automatic partitioning with Redis Cluster.

 * [![Jedis](https://freedumbytes.gitlab.io/setup/images/icon/redis.png "Jedis") Jedis](https://github.com/redis/jedis) is a blazingly small and sane Redis java client.
 * [![Lettuce](https://freedumbytes.gitlab.io/setup/images/icon/lettuce.png "Lettuce") Lettuce](https://lettuce.io) is a fully non-blocking Redis client built with netty providing Reactive, Asynchronous and Synchronous Data Access.

#### Authentication and Authorisation

[![Nimbus JOSE + JWT](https://freedumbytes.gitlab.io/setup/images/icon/connect2id.png "Nimbus JOSE + JWT") Nimbus JOSE + JWT](https://connect2id.com/products/nimbus-jose-jwt)
implements the Javascript Object Signing and Encryption (JOSE) and JSON Web Token (JWT) specs.

[![Nimbus OAuth 2.0 SDK](https://freedumbytes.gitlab.io/setup/images/icon/connect2id.png "Nimbus OAuth 2.0 SDK") Nimbus OAuth 2.0 SDK](https://connect2id.com/products/nimbus-oauth-openid-connect-sdk)
for developing OAuth 2.0 and OpenID Connect clients and servers.

#### Byte Code

[![Objenesis](https://freedumbytes.gitlab.io/setup/images/icon/easymock.png "Objenesis") Objenesis](https://github.com/easymock/objenesis)
aims to overcome the restriction, when a class cannot be instantiated with dynamic instantiation using `Class.newInstance()`, by bypassing the constructor on object instantiation.
This library is part of [![EasyMock](https://freedumbytes.gitlab.io/setup/images/icon/easymock.png "EasyMock") EasyMock](https://github.com/easymock) and used by Mockito.

[![Byte Buddy](https://freedumbytes.gitlab.io/setup/images/icon/byte-buddy.png "Byte Buddy") Byte Buddy](https://github.com/raphw/byte-buddy)
is a code generation and manipulation library for creating and modifying Java classes during the runtime of a Java application and without the help of a compiler. Also used by Mockito.
 * See also **Grouping Test Dependencies**

[![Janino](https://freedumbytes.gitlab.io/setup/images/icon/janino.png "Janino") Janino](https://janino-compiler.github.io/janino)
can not only compile a set of source files to a set of class files like JAVAC, but also compile a Java expression, a block, a class body, one `.java` file or a set of `.java` files in memory,
load the bytecode and execute it directly in the same JVM.

#### Testing

[![AssertJ Core](https://freedumbytes.gitlab.io/setup/images/icon/assertj.png "AssertJ Core") AssertJ Core](https://assertj.github.io/doc)
is a java library providing a rich set of assertions, truly helpful error messages, improves test code readability and is designed to be super easy to use within your favorite IDE.
 * See also **Grouping Test Dependencies**

[![Awaitility](https://freedumbytes.gitlab.io/setup/images/icon/awaitility.png "Awaitility") Awaitility](http://awaitility.org)
for synchronizing asynchronous operations. Testing asynchronous systems is hard. Not only does it require handling threads, timeouts and concurrency issues, but the intent of the test code can be obscured by all these details.
Awaitility is a DSL that allows you to express expectations of an asynchronous system in a concise and easy to read manner.

[![Hamcrest](https://freedumbytes.gitlab.io/setup/images/icon/hamcrest.png "Hamcrest") Hamcrest](http://hamcrest.org)
matchers that can be combined to create flexible expressions of intent.
 * See also **Grouping Test Dependencies**

[![HtmlUnit](https://freedumbytes.gitlab.io/setup/images/icon/gargoylesoftware.png "HtmlUnit") HtmlUnit](https://htmlunit.sourceforge.io)
is a _GUI-Less browser_ for Java programs.

[![JSONassert](https://freedumbytes.gitlab.io/setup/images/icon/skyscreamer.png "JSONassert") JSONassert](https://github.com/skyscreamer/JSONassert)
write JSON unit tests in less code. Great for testing REST interfaces.

[![JUnit](https://freedumbytes.gitlab.io/setup/images/icon/junit.png "JUnit") JUnit](https://junit.org/junit4)
is a simple framework to write repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks.
 * See also **Grouping Test Dependencies**

[![JUnit Jupiter](https://freedumbytes.gitlab.io/setup/images/icon/junit-jupiter.png "JUnit Jupiter") JUnit Jupiter](https://junit.org)
the programmer-friendly testing framework for Java and the JVM.
 * See also **Grouping Test Dependencies**

[![Mockito](https://freedumbytes.gitlab.io/setup/images/icon/mockito.png "Mockito") Mockito](https://site.mockito.org)
a tasty mocking framework for unit tests in Java. It has been designed to be intuitive to use when the test needs mocks.
 * See also **Grouping Test Dependencies**

[![NekoHTML](https://freedumbytes.gitlab.io/setup/images/icon/sourceforge.png "NekoHTML") NekoHTML](http://nekohtml.sourceforge.net)
is a simple HTML scanner and tag balancer that enables application programmers to parse HTML documents and access the information using standard XML interfaces.

[![REST Assured](https://freedumbytes.gitlab.io/setup/images/icon/rest-assured.png "REST Assured") REST Assured](https://rest-assured.io)
a Java DSL for easy testing of REST services.

[![Selenium](https://freedumbytes.gitlab.io/setup/images/icon/selenium.png "Selenium") Selenium](https://selenium.dev)
automates browsers. That's it! What you do with that power is entirely up to you. Primarily it is for automating web applications for testing purposes, but is certainly not limited to just that.
Boring web-based administration tasks can (and should) also be automated as well.

 * [![Selenium](https://freedumbytes.gitlab.io/setup/images/icon/selenium.png "Selenium") HtmlUnitDriver](https://github.com/SeleniumHQ/htmlunit-driver)

[![XMLUnit](https://freedumbytes.gitlab.io/setup/images/icon/xmlunit.png "XMLUnit") XMLUnit](https://www.xmlunit.org)
provides you with the tools to verify the XML you emit is the one you want to create. It provides helpers to validate against an XML Schema, assert the values of XPath queries or compare XML documents against expected outcomes.
The most important part is a diff-engine that provides you with full control over what kind of difference is important to you and which part of the generated document to compare with which part of your reference document.

#### JMX

[![Jolokia](https://freedumbytes.gitlab.io/setup/images/icon/jolokia.png "Jolokia") Jolokia](https://jolokia.org)
is remote JMX with JSON over HTTP.

#### Packaging

[![WebJars](https://freedumbytes.gitlab.io/setup/images/icon/webjars.png "WebJars") WebJars](https://www.webjars.org)
are client-side web libraries (e.g. jQuery & Bootstrap) packaged into JAR (Java Archive) files. Explicitly and easily manage the client-side dependencies in JVM-based web applications.
Use JVM-based build tools (e.g. Maven, Gradle, sbt, ...) to download your client-side dependencies. Transitive dependencies are automatically resolved and optionally loaded via RequireJS.

[![HAL](https://freedumbytes.gitlab.io/setup/images/icon/wikipedia.png "HAL") Hypertext Application Language (HAL)](https://en.wikipedia.org/wiki/Hypertext_Application_Language)
is a simple format that gives a consistent and easy way to hyperlink between resources in your API.

 * [![HAL](https://freedumbytes.gitlab.io/setup/images/icon/github.png "HAL") HAL-browser](https://github.com/mikekelly/hal-browser)
   * [![WebJars](https://freedumbytes.gitlab.io/setup/images/icon/webjars.png "WebJars") hal-browser-webjar](https://github.com/webjars/hal-browser)
   * [![WebJars](https://freedumbytes.gitlab.io/setup/images/icon/webjars.png "WebJars") webjars-locator-core](https://github.com/webjars/webjars-locator-core)

#### Metrics

[![Metrics](https://freedumbytes.gitlab.io/setup/images/icon/metrics.png "Metrics") Metrics](https://metrics.dropwizard.io)
provides a powerful toolkit of ways to measure the behavior of critical components in your production environment.

[![Micrometer](https://freedumbytes.gitlab.io/setup/images/icon/micrometer.png "Micrometer") Micrometer](https://micrometer.io)
an application metrics facade for the most popular monitoring tools. Instrument your code with dimensional metrics with a vendor neutral interface and decide on the monitoring backend at the last minute.

[![Prometheus](https://freedumbytes.gitlab.io/setup/images/icon/prometheus.png "Prometheus") Prometheus](https://prometheus.io)
is an open-source systems monitoring and alerting toolkit.

### Additional Spring Boot Plugins Overview

Deepest level:

[![Spring Boot Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Spring Boot Maven Site")
 ![Spring Boot Maven Site](https://img.shields.io/badge/Spring_Boot-latest_release-802C59 "Spring Boot Maven Site")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom/spring-boot)
[![Spring Boot Maven Site](https://img.shields.io/badge/Spring_Boot-current_snapshot-802C59 "Spring Boot Maven Site")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom/spring-boot)

 * [![Spring Boot Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-latest_release-802C59 "Spring Boot Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom/spring-boot/plugin-updates-report.html)
   [![Spring Boot Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-current_snapshot-802C59
      "Spring Boot Plugin Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom/spring-boot/plugin-updates-report.html)
 * [![Spring Boot Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-latest_release-802C59
      "Spring Boot Property Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom/spring-boot/property-updates-report.html)
   [![Spring Boot Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-current_snapshot-802C59
      "Spring Boot Property Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom/spring-boot/property-updates-report.html)
 * [![Spring Boot Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-latest_release-802C59
      "Spring Boot Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/bom/java/spring-boot-bom/spring-boot/dependency-updates-report.html)
   [![Spring Boot Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-current_snapshot-802C59
      "Spring Boot Dependency Updates Report")](https://freedumbytes.gitlab.io/setup/snapshot/bom/java/spring-boot-bom/spring-boot/dependency-updates-report.html)

Extra Spring Boot Maven Plugins:

 * [![Spring Boot](https://freedumbytes.gitlab.io/setup/images/icon/spring-boot.png "Spring Boot") spring-boot-maven-plugin](https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle) 2.4.9
   ([changelog](https://github.com/spring-projects/spring-boot/releases)) [compare 2.4.8](https://github.com/spring-projects/spring-boot/compare/v2.4.8...v2.4.9#files_bucket) and specifically the files:
   * ` gradle.properties`
   * `spring-boot-project/spring-boot-starters/spring-boot-starter-parent/build.gradle`
   * `spring-boot-project/spring-boot-dependencies/build.gradle`
 * [![Flyway](https://freedumbytes.gitlab.io/setup/images/icon/flyway.png "Flyway") flyway-maven-plugin](https://github.com/flyway/flyway) 7.1.1 ([changelog](https://github.com/flyway/flyway/releases))
 * [![ktoso](https://freedumbytes.gitlab.io/setup/images/icon/ktoso.png "ktoso") git-commit-id-plugin](https://github.com/git-commit-id/git-commit-id-maven-plugin) 3.0.1
   ([changelog](https://github.com/git-commit-id/git-commit-id-maven-plugin/releases))
 * [![Apache Johnzon](https://freedumbytes.gitlab.io/setup/images/icon/johnzon.png "Apache Johnzon") johnzon-maven-plugin](https://johnzon.apache.org) 1.2.11
   ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12315523))
 * [![jOOQ](https://freedumbytes.gitlab.io/setup/images/icon/jooq.png "jOOQ") jooq-codegen-maven](https://github.com/jOOQ/jOOQ) 3.14.9 ([changelog](https://github.com/jOOQ/jOOQ/releases))
 * [![Kotlin](https://freedumbytes.gitlab.io/setup/images/icon/kotlin.png "Kotlin") kotlin-maven-plugin](https://github.com/JetBrains/kotlin) 1.4.32 ([changelog](https://github.com/JetBrains/kotlin/releases))
 * [![Liquibase](https://freedumbytes.gitlab.io/setup/images/icon/liquibase.png "Liquibase") liquibase-maven-plugin](https://github.com/liquibase/liquibase) 3.10.3 ([changelog](https://github.com/liquibase/liquibase/releases))
 * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") xml-maven-plugin](https://github.com/mojohaus/xml-maven-plugin) 1.0.2 ([changelog](https://github.com/mojohaus/xml-maven-plugin/releases))

## Java Utilities Overview

Overview of [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Java Utilities](https://freedumbytes.gitlab.io/setup/bom/java/utilities) and their content:

 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Log Test Utilities](https://freedumbytes.gitlab.io/setup/bom/java/utilities/log-test-utils)
   * Just a default sample [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") log4j2.xml](https://gitlab.com/freedumbytes/setup/blob/master/bom/java/utilities/log-test-utils/src/main/resources/log4j2.xml)
     useful for testing the other projects.
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Test Utilities](https://freedumbytes.gitlab.io/setup/bom/java/utilities/test-utils)
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") LoggerRule](https://freedumbytes.gitlab.io/setup/bom/java/utilities/test-utils/apidocs/nl/demon/shadowland/freedumbytes/test/unit/rule/LoggerRule.html)
     with assertions for Log4j logging behaviour in unit tests.
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Log Utilities](https://freedumbytes.gitlab.io/setup/bom/java/utilities/log-utils)
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Slf4jLoggingFeatureLevelWrapper](https://freedumbytes.gitlab.io/setup/bom/java/utilities/log-utils/apidocs/nl/demon/shadowland/freedumbytes/java/util/logging/jersey/Slf4jLoggingFeatureLevelWrapper.html)
     alternative `Logger` for Jersey `LoggingFeature` to handle all logging of package or class as if of supplied level and uses `isLoggable` to check if `log4j2.xml` is set to this level or higher.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Slf4jLogManager](https://freedumbytes.gitlab.io/setup/bom/java/utilities/log-utils/apidocs/nl/demon/shadowland/freedumbytes/java/util/logging/manager/Slf4jLogManager.html)
     custom LogManager for `java.util.logging` that doesn't require `SLF4JBridgeHandler` and uses
     [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Slf4jLoggerWrapper](https://freedumbytes.gitlab.io/setup/bom/java/utilities/log-utils/apidocs/nl/demon/shadowland/freedumbytes/java/util/logging/manager/Slf4jLoggerWrapper.html)`.isLoggable`
     thus removing the performance issue of the bridge handler.
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Hibernate Utilities](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils)
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") UUIDGenerator](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/uuid/UUIDGenerator.html)
     a static factory to retrieve a type 5 (name based) `UUID` based on the specified byte array.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") CustomVersionFiveStrategy](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/hibernate/id/uuid/CustomVersionFiveStrategy.html)
     a type 5 (name based) `UUID` generation strategy.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") CustomVersionThreeStrategy](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/hibernate/id/uuid/CustomVersionThreeStrategy.html)
     a type 3 (name based) `UUID` generation strategy.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") GenericDao](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/GenericDao.html)
     all `CRUD` (create, read, update, delete) basic data access operations are isolated in this interface and shared across all `DAO` implementations.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") JpaDao](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/JpaDao.html)
     a `JPA` implementation of `GenericDao`.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Identifiable](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/Identifiable.html)
     an interface to mark objects that are identifiable by an `ID` of any `Serializable` type.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") IdentifiableGenericParams](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/IdentifiableGenericParams.html)
     order of `Identifiable` generic parameters.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") JpaDaoGenericParams](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/JpaDaoGenericParams.html)
     order of `JpaDao` generic parameters.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Parameterizable](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/reflect/Parameterizable.html)
     find the parameterized type of a generic super class or a generic interface in the supplied class.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") ParameterMatchMix](https://freedumbytes.gitlab.io/setup/bom/java/utilities/hibernate-utils/apidocs/nl/demon/shadowland/freedumbytes/reflect/ParameterMatchMix.html)
     how to get a class instance of a generic parameter type with given index.
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Apache Tomcat Utilities](https://freedumbytes.gitlab.io/setup/bom/java/utilities/tomcat-utils)
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") SessionTimeoutListener](https://freedumbytes.gitlab.io/setup/bom/java/utilities/tomcat-utils/apidocs/nl/demon/shadowland/freedumbytes/servlet/http/listener/session/timeout/SessionTimeoutListener.html)
     a sample `@WebListener` Session Timeout Logger.
   * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") SessionKillerFilter](https://freedumbytes.gitlab.io/setup/bom/java/utilities/tomcat-utils/apidocs/nl/demon/shadowland/freedumbytes/servlet/http/filter/session/killer/SessionKillerFilter.html)
     a sample `@WebFilter` to see `SessionTimeoutListener` in action.

To use the custom `Slf4jLogManager` for example in Maven edit the `pom.xml` as follows:

```xml
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <argLine>-Djava.util.logging.manager=nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager ${argLine}</argLine>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
        <configuration>
          <argLine>-Djava.util.logging.manager=nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager ${argLine}</argLine>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

**Note**: When using the JaCoCo Plugin don't forget the extra `${argLine}` option to supply its already configured agent.

This will result in some extra never noticed before logging:

```
2018-12-24 19:47:48,264 INFO  [main] internal.ServiceFinder  - Running in a non-OSGi environment
2018-12-24 19:47:48,285 INFO  [main] test.JerseyTest  - Found multiple TestContainerFactory service providers, using the default found {0}
2018-12-24 19:47:48,599 INFO  [main] server.ApplicationHandler  - Initiating Jersey application, version Jersey: 2.27 2018-04-10 07:34:57...
2018-12-24 19:47:49,033 INFO  [main] internal.ExecutorProviders  - Selected ExecutorServiceProvider implementation
                                     [org.glassfish.jersey.server.ServerExecutorProvidersConfigurator$DefaultManagedAsyncExecutorProvider]
                                     to be used for injection of executor qualified by [org.glassfish.jersey.server.ManagedAsyncExecutor] annotation.
2018-12-24 19:47:49,034 INFO  [main] internal.ExecutorProviders  - Selected ScheduledExecutorServiceProvider implementation
                                     [org.glassfish.jersey.server.ServerExecutorProvidersConfigurator$DefaultBackgroundSchedulerProvider]
                                     to be used for injection of scheduler qualified by [org.glassfish.jersey.server.BackgroundScheduler] annotation.
2018-12-24 19:47:49,372 INFO  [main] server.ApplicationHandler  - Jersey application initialized.
Root Resource Classes:
  nl.demon.shadowland.freedumbytes.rest.test.spring.EchoResource
Pre-match Filters:
   org.glassfish.jersey.logging.ServerLoggingFilter
Global Response Filters:
   org.glassfish.jersey.logging.ServerLoggingFilter
Global Reader Interceptors:
   org.glassfish.jersey.server.internal.MappableExceptionWrapperInterceptor
Global Writer Interceptors:
   org.glassfish.jersey.logging.ServerLoggingFilter
   org.glassfish.jersey.server.internal.MappableExceptionWrapperInterceptor
   org.glassfish.jersey.server.internal.JsonWithPaddingInterceptor
Message Body Readers:
   org.glassfish.jersey.moxy.json.internal.ConfigurableMoxyJsonProvider
Message Body Writers:
   org.glassfish.jersey.moxy.json.internal.ConfigurableMoxyJsonProvider
```

To test both `LogManager` and `Slf4jLogManager` use for example the following `pom.xml` setup:

```xml
  <properties>
    <jacoco.append>true</jacoco.append>
  </properties>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <excludes>
            <exclude>**/LogManagerTest.java</exclude>
            <exclude>**/Slf4jLogManagerTest.java</exclude>
          </excludes>
        </configuration>
        <executions>
          <execution>
            <id>surefire-jul</id>
            <phase>test</phase>
            <goals>
              <goal>test</goal>
            </goals>
            <configuration>
              <includes>
                <include>**/LogManagerTest.java</include>
              </includes>
              <excludes>
                <exclude>**/Slf4jLogManagerTest.java</exclude>
              </excludes>
              <argLine>-Djava.util.logging.manager=java.util.logging.LogManager ${argLine}</argLine>
            </configuration>
          </execution>
          <execution>
            <id>surefire-slf4j</id>
            <phase>test</phase>
            <goals>
              <goal>test</goal>
            </goals>
            <configuration>
              <includes>
                <include>**/Slf4jLogManagerTest.java</include>
              </includes>
              <excludes>
                <exclude>**/LogManagerTest.java</exclude>
              </excludes>
              <argLine>-Djava.util.logging.manager=nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager ${argLine}</argLine>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
```

**Note**: To prevent the default `excludes` from reverting the `include` add an `exclude` also.

## Custom Maven Skins

To use for example the [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "GitLab") Custom Reflow Skin](https://freedumbytes.gitlab.io/skins/maven-reflow-skin):

```xml
  <properties>
    <skinGroupId>nl.demon.shadowland.freedumbytes.maven.custom.skins</skinGroupId>
    <skinArtifactId>maven-reflow-skin</skinArtifactId>
    <skinVersion>${customMavenReflowSkinVersion}</skinVersion>

    <customSkinVersion>x.y.z</customSkinVersion>
    <customMavenPaperSkinVersion>${customSkinVersion}</customMavenPaperSkinVersion>
    <customMavenFluidoSkinVersion>${customSkinVersion}</customMavenFluidoSkinVersion>
    <customMavenReflowSkinVersion>${customSkinVersion}</customMavenReflowSkinVersion>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>${mavenSitePluginVersion}</version>
          <dependencies>
            <dependency>
              <groupId>nl.demon.shadowland.freedumbytes.patch.lt.velykis.maven.skins</groupId>
              <artifactId>reflow-velocity-tools</artifactId>
              <version>${customMavenReflowSkinVersion}</version>
            </dependency>
          </dependencies>
          <configuration>
            <generateSitemap>${generateSitemap}</generateSitemap>
            <inputEncoding>${project.build.sourceEncoding}</inputEncoding>
            <outputEncoding>${project.reporting.outputEncoding}</outputEncoding>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

And the corresponding `site.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/DECORATION/1.8.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/DECORATION/1.8.0 https://maven.apache.org/xsd/decoration-1.8.0.xsd"
         combine.self="override">
  <skin>
    <groupId>${skinGroupId}</groupId>
    <artifactId>${skinArtifactId}</artifactId>
    <version>${skinVersion}</version>
  </skin>

  <publishDate position="bottom" format="yyyy-MM-dd HH:mm:ss z" />
  <version position="navigation-bottom" />

  <custom>
    <reflowSkin>
      <localResources>true</localResources>

      <highlightJs>true</highlightJs>

      <topNav>parent|modules|reports</topNav>
      <navbarInverse>true</navbarInverse>

      <breadcrumbs>false</breadcrumbs>
      <toc>false</toc>

      <skinAttribution>false</skinAttribution>
    </reflowSkin>
  </custom>

  <body>
    <menu inherit="bottom" ref="parent" />
    <menu inherit="bottom" ref="modules" />
    <menu inherit="bottom" ref="reports" />
  </body>
</project>
```

### Maven Skins Overview

 * [maven-default-skin](https://maven.apache.org/skins/maven-default-skin) [1.3](https://github.com/apache/maven-default-skin/releases) ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Default Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Default-900 "Maven Default Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-default-skin-sample)
 * ~~[maven-classic-skin](https://maven.apache.org/skins/maven-classic-skin)~~ 1.1 **retired** ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Classic Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Classic-036 "Maven Classic Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-classic-skin-sample)
 * ~~[maven-stylus-skin](https://maven.apache.org/skins/maven-stylus-skin)~~ 1.5 **retired** ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Stylus Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Stylus-68A "Maven Stylus Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-stylus-skin-sample)
 * ~~[maven-application-skin](https://maven.apache.org/skins/maven-application-skin)~~ 1.0 **retired** ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Application Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Application-F3B455 "Maven Application Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-application-skin-sample)
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") maven-paper-skin](https://gitlab.com/freedumbytes/skins/-/tree/master/maven-paper-skin) [1.4.2](https://gitlab.com/freedumbytes/skins/-/tags)
   [![Paper Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Paper-E7E0CA "Paper Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-paper-skin)
 * [maven-fluido-skin](https://maven.apache.org/skins/maven-fluido-skin) [1.10.0](https://github.com/apache/maven-fluido-skin/releases) ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Fluido Skin](https://img.shields.io/badge/Maven_Skin-Fluido-08C "Maven Fluido Skin")](https://maven.apache.org/skins/maven-fluido-skin/sample.html)
 * Custom [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") maven-fluido-skin](https://gitlab.com/freedumbytes/skins/-/tree/master/maven-fluido-skin) [1.4.2](https://gitlab.com/freedumbytes/skins/-/tags)
   [![Custom Fluido Skin Sample](https://img.shields.io/badge/Custom_Skin-Fluido-08C "Custom Fluido Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-fluido-skin) versus
   [![Original Fluido Skin Sample](https://img.shields.io/badge/Original_Skin-Fluido-08C "Original Fluido Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-fluido-skin-sample)
 * [reflow-maven-skin](http://andriusvelykis.github.io/reflow-maven-skin) [1.1.1](https://github.com/andriusvelykis/reflow-maven-skin/releases) ([issues](https://github.com/andriusvelykis/reflow-maven-skin/milestones?state=closed))
   ([changelog](http://andriusvelykis.github.io/reflow-maven-skin/release-notes.html))
   [![Reflow Maven Skin](https://img.shields.io/badge/Maven_Skin-Reflow-225E9B "Reflow Maven Skin")](http://andriusvelykis.github.io/reflow-maven-skin/project-reports.html)
 * Custom [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") maven-reflow-skin](https://gitlab.com/freedumbytes/skins/-/tree/master/maven-reflow-skin) [1.4.2](https://gitlab.com/freedumbytes/skins/-/tags)
   [![Custom Reflow Skin Sample](https://img.shields.io/badge/Custom_Skin-Reflow-7F2B59 "Custom Reflow Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-reflow-skin)
   with [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") reflow-velocity-tools-patch](https://gitlab.com/freedumbytes/skins/-/tree/master/reflow-velocity-tools-patch)
   for [![Maven Site Plugin 3.5 and higher](https://img.shields.io/badge/Maven_Site_Plugin-3.5_%2B-D5AF1B "Maven Site Plugin 3.5 and higher")](https://maven.apache.org/plugins/maven-site-plugin)
 * [docs-maven-skin](https://docs.bernardomg.com/maven/docs-maven-skin) [2.2.5](https://github.com/Bernardo-MG/docs-maven-skin/releases) ([changelog](https://docs.bernardomg.com/maven/docs-maven-skin/changes-report.html))
   [![Docs Maven Skin](https://img.shields.io/badge/Maven_Skin-Docs-FFF "Docs Maven Skin")](https://docs.bernardomg.com/maven/docs-maven-skin/reports.html)

## JModalWindow

 * [![JModalWindow SUN Article](https://freedumbytes.gitlab.io/setup/images/icon/jmodalwindow.png "JModalWindow SUN Article") JModalWindow SUN Article](https://freedumbytes.gitlab.io/jmodalwindow.xhtml) about the
   [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") JModalWindow Project](https://gitlab.com/freedumbytes/jmodalwindow).
 * Options to run the SUN Article Sample and the newer complete Demo (don't forget to run `mvn compile` first):
   * `mvn exec:exec -pl sample`
   * `mvn exec:exec -pl demo [-Dextra.argument=-minimal]`

**Note**: The optional command line option `extra.argument` with value `-minimal` disables the blurring of a blocked window and the busy cursor, when moving the mouse cursor over it. It also disables iconify of a blocked internal frame.

## Basketball

 * [![Basketball](https://freedumbytes.gitlab.io/setup/images/icon/basketball.png "Basketball") Basketball](https://gitlab.com/freedumbytes/basketball) Data Storage of Game Statistics to demonstrate:
   * ![Maven Java Setup](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Java Setup") Maven Java Setup usage of this OpenSource project.
   * ![Maven BOM](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven BOM") Maven Bill Of Materials usage as defined by this OpenSource project.
   * [![Eclipse EE4J](https://freedumbytes.gitlab.io/setup/images/icon/jersey.png "Eclipse EE4J") Jersey](https://projects.eclipse.org/projects/ee4j.jersey) REST framework.
   * [![ReactJS](https://freedumbytes.gitlab.io/setup/images/icon/reactjs.png "ReactJS") React JavaScript (ReactJS)](https://reactjs.org) library for building user interfaces.

## SonarQube / SonarCloud Maven Report Plugin

Add the [![SonarQube](https://freedumbytes.gitlab.io/setup/images/icon/sonarqube.png "SonarQube") SonarQube / ![SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "SonarCloud")
         SonarCloud Maven Report Plugin](https://github.com/SonarQubeCommunity/sonar-maven-report) to the reporting section in the POM:

```xml
<project>
  <properties>
    <sonarCloudHost>https://sonarcloud.io</sonarCloudHost>

    <mavenSonarQubeReportPluginVersion>0.2.2</mavenSonarQubeReportPluginVersion>
  </properties>

  <profiles>
    <profile>
      <id>openSource</id>
      <properties>
        <sonarHost>${sonarCloudHost}</sonarHost>
      </properties>
    </profile>
  </profiles>

  <reporting>
    <plugins>
      <plugin>
        <groupId>nl.demon.shadowland.maven.plugins</groupId>
        <artifactId>sonarqube-maven-report</artifactId>
        <version>${mavenSonarQubeReportPluginVersion}</version>
        <configuration>
          <sonarHostURL>${sonarHost}</sonarHostURL>
        </configuration>
      </plugin>
    </plugins>
  </reporting>
</project>
```

## Releases

 * [![Semantic Versioning](https://freedumbytes.gitlab.io/setup/images/icon/creativecommons.png "Semantic Versioning") Semantic Versioning](https://semver.org) given a version number `MAJOR.MINOR.PATCH`.

## Git

[![Git](https://freedumbytes.gitlab.io/setup/images/icon/git.png "Git") Git](https://git-scm.com)
is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

 * Track Empty Directories with [`.gitkeep`](https://davidwalsh.name/git-empty-directory) or [`!.gitignore`](https://stackoverflow.com/a/14542835):

```
# ignore every file
*
# ignore every subfolder
*/
# except .gitignore
!.gitignore
```

## Bitbucket

[![Bitbucket](https://freedumbytes.gitlab.io/setup/images/logo/bitbucket.png "Bitbucket")](https://bitbucket.org) Built for professional teams.

[![Bitbucket](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket") Bitbucket](https://bitbucket.org)
is more than just Git code management. Bitbucket gives teams one place to plan projects, collaborate on code, test, and deploy.

 * [![Bitbucket Status](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket Status") Status](https://status.bitbucket.org)
 * [![Bitbucket Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "Bitbucket Twitter") @bitbucketstatus](https://twitter.com/bitbucketstatus)

## GitHub

[![GitHub](https://freedumbytes.gitlab.io/setup/images/logo/github-mark.png "GitHub")](https://github.com) Built for developers.

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") GitHub](https://github.com)
is a development platform inspired by the way you work. From open source to business, you can host and review code, manage projects, and build software alongside 50 million developers.

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") The Octocat](http://cameronmcefee.com/work/the-octocat) - a nerdy household name.
Walking around San Francisco's SoMa neighborhood, one of the hearts of the current startup boom and home to GitHub's headquarters, it's nearly impossible to travel more than a city block without seeing a friendly, cartoony octopus-cat.

 * [![GitHub Status](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub Status") Status](https://status.github.com)
 * [![GitHub Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "GitHub Twitter") @githubstatus](https://twitter.com/githubstatus)

## GitLab

[![GitLab](https://freedumbytes.gitlab.io/setup/images/logo/gitlab-black.png "GitLab")](https://gitlab.com) The complete DevOps platform.

One application, endless possibilities. Developers rely on [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") GitLab](https://gitlab.com) to deliver software with source code management, CI/CD, security and more.

[![Tanuki](https://freedumbytes.gitlab.io/setup/images/icon/tanuki.png "Tanuki")](https://about.gitlab.com/company/) GitLab used to have an angry looking raccoon dog, now it's an abstract one - or better sounding: Tanuki (Japanese for raccoon dog).

 * [![GitLab Status](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab Status") Status](https://status.gitlab.com)
 * [![GitLab Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "GitLab Twitter") @gitlabstatus](https://twitter.com/gitlabstatus)
 * [![GitLab CI/CD](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab CI/CD") GitLab's guide to safe deployment practices](https://about.gitlab.com/blog/2020/07/23/safe-deploys)
   * [Deployment safety](https://docs.gitlab.com/ce/ci/environments/deployment_safety.html)
 * [![GitLab CI/CD Templates](https://freedumbytes.gitlab.io/setup/images/icon/gitlab-bot.png "GitLab CI/CD Templates") Development guide for GitLab CI/CD templates](https://docs.gitlab.com/ce/development/cicd/templates.html)
   * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") GitLab FOSS](https://gitlab.com/gitlab-org/gitlab-foss) is a read-only mirror of GitLab, with all proprietary code removed.
     * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") .gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/.gitlab-ci.yml)
     * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") GitLab Templates](https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/.gitlab/ci)
       * [pages.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/.gitlab/ci/pages.gitlab-ci.yml)
       * [rules.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/.gitlab/ci/rules.gitlab-ci.yml)

## HTML

[![HTML](https://freedumbytes.gitlab.io/setup/images/icon/html.png "HTML") Hypertext Markup Language (HTML)](https://en.wikipedia.org/wiki/HTML)
is the standard markup language for documents designed to be displayed in a web browser.

 * [![web.dev](https://freedumbytes.gitlab.io/setup/images/icon/web-dev.png "web.dev") Best Practices audits](https://web.dev/lighthouse-best-practices)
   * [![web.dev](https://freedumbytes.gitlab.io/setup/images/icon/web-dev.png "web.dev") Page lacks the HTML doctype thus triggering quirks mode](https://web.dev/doctype)
   * [![web.dev](https://freedumbytes.gitlab.io/setup/images/icon/web-dev.png "web.dev") Links to cross-origin destinations are unsafe](https://web.dev/external-anchors-use-rel-noopener)
     * [![Can I use](https://freedumbytes.gitlab.io/setup/images/icon/caniuse.png "Can I use") Can I use](https://caniuse.com)
       provides up-to-date browser support tables for support of front-end web technologies on desktop and mobile web browsers.
       * [![Can I use](https://freedumbytes.gitlab.io/setup/images/icon/caniuse.png "Can I use") noopener](https://caniuse.com/?search=noopener)
       * [![Can I use](https://freedumbytes.gitlab.io/setup/images/icon/caniuse.png "Can I use") noreferrer](https://caniuse.com/?search=noreferrer)

## CSS

[![CSS](https://freedumbytes.gitlab.io/setup/images/icon/css.png "CSS") Cascading Style Sheets CSS](https://en.wikipedia.org/wiki/CSS)
is a style sheet language used for describing the presentation of a document written in a markup language such as HTML.

## JavaScript

[![JS](https://freedumbytes.gitlab.io/setup/images/icon/js.png "JS") JavaScript (JS)](https://en.wikipedia.org/wiki/JavaScript)
is a programming language that conforms to the ECMAScript specification.

 * [![cdnjs](https://freedumbytes.gitlab.io/setup/images/icon/cdnjs.png "cdnjs") cdnjs](https://cdnjs.com) a free and open-source CDN service. Simple. Fast. Reliable. Content delivery at its finest.
 * [![jsDelivr](https://freedumbytes.gitlab.io/setup/images/icon/jsdelivr.png "jsDelivr") jsDelivr](https://jsdelivr.com) a free CDN for Open Source. Fast, reliable, and automated.
 * Build fast, responsive sites with [![Bootstrap](https://freedumbytes.gitlab.io/setup/images/icon/bootstrap.png "Bootstrap") Bootstrap](https://getbootstrap.com) ([changelog](https://getbootstrap.com/docs/versions))
 * [![Lightbox](https://freedumbytes.gitlab.io/setup/images/icon/lightbox.png "Lightbox") Lightbox](https://lokeshdhakar.com/projects/lightbox2) overlay images on top of the current page ([changelog](https://github.com/lokesh/lightbox2/releases)) at
   [cdnjs](https://cdnjs.com/libraries/lightbox2)
 * [![jQuery](https://freedumbytes.gitlab.io/setup/images/icon/jquery.png "jQuery") jQuery](https://jquery.com) a fast, small, and feature-rich JavaScript library ([changelog](https://code.jquery.com/jquery)) at
   [cdnjs](https://cdnjs.com/libraries/jquery)
   * [![jQuery Migrate](https://freedumbytes.gitlab.io/setup/images/icon/jquery.png "jQuery Migrate") jQuery Migrate](https://github.com/jquery/jquery-migrate)
     makes upgrading, by restoring the APIs that were removed, and additionally shows warnings in the browser console (development version of jQuery Migrate only) when removed and/or deprecated APIs are used
     ([changelog](https://github.com/jquery/jquery-migrate/tags)) at [cdnjs](https://cdnjs.com/libraries/jquery-migrate)
 * [![highlight.js](https://freedumbytes.gitlab.io/setup/images/icon/highlightjs.png "highlight.js") highlight.js](https://highlightjs.org) syntax highlighting and [styling](https://highlightjs.org/static/demo) for the web
   ([changelog](https://github.com/highlightjs/highlight.js/releases)) at [cdnjs](https://cdnjs.com/libraries/highlight.js)
 * [![HTML](https://freedumbytes.gitlab.io/setup/images/icon/html.png "HTML") The Story of the HTML5 Shiv](https://paulirish.com/2011/the-history-of-the-html5-shiv) ([changelog](https://github.com/aFarkas/html5shiv/releases)) at
   [cdnjs](https://cdnjs.com/libraries/html5shiv)

## Jakarta Servlets

[![Jakarta EE](https://freedumbytes.gitlab.io/setup/images/icon/jakarta.png "Jakarta EE") Jakarta Servlets (formerly Java Servlets)](https://en.wikipedia.org/wiki/Jakarta_Servlet)
is a Java software component that extends the capabilities of a server.
Although servlets can respond to many types of requests, they most commonly implement web containers for hosting web applications on web servers and thus qualify as a server-side servlet web API.
Such web servlets are the Java counterpart to other dynamic web content technologies such as PHP and ASP.NET.

 * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") JavaServlet Specification](https://mvnrepository.com/artifact/javax.servlet/servlet-api) 2.x.
 * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") Java Servlets](https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api) 3.x - 4.0.1.
 * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") Jakarta Servlets](https://mvnrepository.com/artifact/jakarta.servlet/jakarta.servlet-api) 4.0.2 - 5.x.

## Java Platform

[![Java EE](https://freedumbytes.gitlab.io/setup/images/icon/java.png "Java EE") Java Platform, Standard Edition (Java SE)](https://docs.oracle.com/en/java/javase) helps you develop and deploy Java applications on desktops and servers.
Java offers the rich user interface, performance, versatility, portability, and security that today's applications require.

## Javadoc

[![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Javadoc (originally cased JavaDoc)](https://en.wikipedia.org/wiki/Javadoc)
is a documentation generator created by Sun Microsystems for the Java language for generating API documentation in HTML format from Java source code.

 * [Documentation Comment Specification for the Standard Doclet (JDK 16)](https://docs.oracle.com/en/java/javase/16/docs/specs/javadoc/doc-comment-spec.html)
   * `{@link [package.]class[#member] [label]}{@code <generictype>}` Java implements Generics with Type Erasure. Thus they are **not accessible at runtime** and are **not part of method's signature**.
   * `{@summary text }` introduced in JDK 1.5. Identify the summary of an API description, as an alternative to the default policy to identify and use the _first sentence_ of the API description.

To optionally include links to Javadoc of dependencies define for example the following locations:

```xml
  <properties>
    <tomcatVersion>9.0.43</tomcatVersion>
    <javadocApiDocsTomcat>https://tomcat.apache.org/tomcat-9.0-doc/api/</javadocApiDocsTomcat>
    <javadocApiDocsTomcatServlet>https://tomcat.apache.org/tomcat-9.0-doc/servletapi/</javadocApiDocsTomcatServlet>
    <javadocApiDocsTomcatJSP>https://tomcat.apache.org/tomcat-9.0-doc/jspapi/</javadocApiDocsTomcatJSP>
    <javadocApiDocsTomcatEL>https://tomcat.apache.org/tomcat-9.0-doc/elapi/</javadocApiDocsTomcatEL>
    <javadocApiDocsTomcatWebSocket>https://tomcat.apache.org/tomcat-9.0-doc/websocketapi/</javadocApiDocsTomcatWebSocket>
    <javadocApiDocsTomcatJASPIC>https://tomcat.apache.org/tomcat-9.0-doc/jaspicapi/</javadocApiDocsTomcatJASPIC>
  </properties>
```

Then configure the ones used by the module in the Maven Javadoc Plugin:

```xml
  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <configuration>
            <links>
              <link>${javadocApiDocsTomcatServlet}</link>
            </links>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

**Note**: A [package-list](https://tomcat.apache.org/tomcat-9.0-doc/servletapi/package-list) _must_ be available at the configured location.

To prevent impact of those external Javadoc locations during builds of projects that use the Maven Java Setup as `parent` they were configured only in the utilities modules.
As a result the aggregated Javadoc didn't create those link. Thus making it possible to see the difference between
[the ![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") project aggregated Javadoc](https://freedumbytes.gitlab.io/setup/apidocs/nl/demon/shadowland/freedumbytes/servlet/http/filter/session/killer/SessionKillerFilter.html)
and
[the ![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") module Javadoc](https://freedumbytes.gitlab.io/setup/bom/java/utilities/tomcat-utils/apidocs/nl/demon/shadowland/freedumbytes/servlet/http/filter/session/killer/SessionKillerFilter.html).

**Note**: For all **Java Utilities** modules the access level shown for classes and members in the Javadocs was also set to `private`.

## Ehcache

[![Ehcache](https://freedumbytes.gitlab.io/setup/images/icon/ehcache.png "Ehcache") Ehcache](https://www.ehcache.org) is an open source, standards-based cache that boosts performance, offloads your database, and simplifies scalability.
It's the most widely-used Java-based cache because it's robust, proven, full-featured, and integrates with other popular libraries and frameworks.
Ehcache scales from in-process caching, all the way to mixed in-process/out-of-process deployments with terabyte-sized caches.

 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Introduction To Ehcache](https://baeldung.com/ehcache)

## AspectJ

[![Eclipse Tools Project](https://freedumbytes.gitlab.io/setup/images/icon/eclipse.png "Eclipse Tools Project") AspectJ (AOP)](https://projects.eclipse.org/projects/tools.aspectj)
is a seamless aspect-oriented extension to the Java programming language.

In computing, [![AOP](https://freedumbytes.gitlab.io/setup/images/icon/wikipedia.png "AOP") aspect-oriented programming (AOP)](https://en.wikipedia.org/wiki/Aspect-oriented_programming)
is a programming paradigm that aims to increase modularity by allowing the separation of cross-cutting concerns.
It does so by adding additional behavior to existing code (an _advice_) without modifying the code itself, instead separately specifying which code is modified via a _pointcut_ specification.
This allows behaviors that are not central to the business logic (such as logging) to be added to a program without cluttering the code, core to the functionality.

 * [![Eclipse Tools Project](https://freedumbytes.gitlab.io/setup/images/icon/eclipse.png "Eclipse Tools Project") The AspectJ Programming Guide](https://www.eclipse.org/aspectj/doc/next/progguide)
 * Baeldung [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Tag: AspectJ](https://baeldung.com/tag/aspectj)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Intro to AspectJ](https://baeldung.com/aspectj)
   * [![MojoHaus](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "MojoHaus") aspectj-maven-plugin](https://www.mojohaus.org/aspectj-maven-plugin)

## Javassist

[![Javassist](https://freedumbytes.gitlab.io/setup/images/icon/redhat.png "Javassist") Java Programming Assistant (Javassist)](https://www.javassist.org)
makes Java bytecode manipulation simple. It is a class library for editing bytecodes in Java;
it enables Java programs to define a new class at runtime and to modify a class file when the JVM loads it.
Unlike other similar bytecode editors, Javassist provides two levels of API: source level and bytecode level.
If the users use the source-level API, they can edit a class file without knowledge of the specifications of the Java bytecode.
The whole API is designed with only the vocabulary of the Java language. You can even specify inserted bytecode in the form of source text;
Javassist compiles it on the fly. On the other hand, the bytecode-level API allows the users to directly edit a class file as other editors.

 * Baeldung [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Tag: Java Bytecode](https://baeldung.com/tag/java-bytecode)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Introduction to Javassist](https://baeldung.com/javassist)

## Hamcrest

[![Hamcrest](https://freedumbytes.gitlab.io/setup/images/icon/hamcrest.png "Hamcrest") Hamcrest](http://hamcrest.org) matchers that can be combined to create flexible expressions of intent.

 * [![Hamcrest](https://freedumbytes.gitlab.io/setup/images/icon/hamcrest.png "Hamcrest") Upgrading from Hamcrest 1.x with JUnit 4.x](http://hamcrest.org/JavaHamcrest/distributables)
 * Baeldung [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Tag: Hamcrest](https://baeldung.com/tag/hamcrest)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Testing with Hamcrest](https://baeldung.com/java-junit-hamcrest-guide)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Hamcrest Collections Cookbook](https://baeldung.com/hamcrest-collections-arrays)

## Monkey Island

Szczepan Faber and this is his own [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Monkey Island](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/monkey-island/about.html).

The term _Mock Objects_ has become a popular one to describe special case objects that mimic real objects for testing.

Expect-run-verify is the most common pattern in mock frameworks.
[![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") What's wrong with it](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/monkey-island/expect-run-verify-goodbye.html)?
[![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Can I test what I want, please](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/monkey-island/can-i-test-what-i-want-please.html)?
I'm a mockist but existing mock frameworks just don't appeal to me.
They spoil my TDD experience. They harm code readability. I needed something better. That's why I came up with
[![Mockito](https://freedumbytes.gitlab.io/setup/images/icon/mockito.png "Mockito") Mockito](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/monkey-island/mockito.html).

I'm about to show that it is very useful to distinguish two basic kinds of interactions:
[![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") asking an object of data and telling an object to do something](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/monkey-island/is-there-a-difference-between-asking-and-telling.html).
Fair enough, it may sound obvious but most mocking frameworks treat all interactions equally and don't take advantage of the distinction.

By now, you've probably figured out that Mockito is not strictly a mocking framework. It's rather a spying/stubbing framework but that's a story for a
[![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") different post](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/monkey-island/lets-spy.html).

 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Mockito - The New Mock Framework on the Block](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/behind-the-times/the-new-mock-framework-on-the-block.html).
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Mocks and Stubs aren't Spies](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/behind-the-times/mocks-and-stubs-arent-spies.html).
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Mocks Aren't Stubs](https://freedumbytes.gitlab.io/setup/bom/grouping/test/backup/martin-fowler/mocks-arent-stubs.html).

## Mockito

[![Mockito](https://freedumbytes.gitlab.io/setup/images/icon/mockito.png "Mockito") Mockito](https://github.com/mockito/mockito/wiki)
is a mocking framework for unit tests in Java. It has been designed to be intuitive to use when the test needs mocks.

 * [![Vogella](https://freedumbytes.gitlab.io/setup/images/icon/vogella.png "Vogella") Unit tests with Mockito - Tutorial](https://vogella.com/tutorials/Mockito/article.html)
 * [![Wunderman Thompson](https://freedumbytes.gitlab.io/setup/images/icon/wttech.png "Wunderman Thompson") Mocking static methods made possible in Mockito 3.4.0](https://wttech.blog/blog/2020/mocking-static-methods-made-possible-in-mockito-3.4.0)
 * Baeldung [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Tag: Mockito](https://baeldung.com/tag/mockito)
   [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Series](https://baeldung.com/mockito-series)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mockito vs EasyMock vs JMockit](https://baeldung.com/mockito-vs-easymock-vs-jmockit)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Getting Started with Mockito @Mock, @Spy, @Captor and @InjectMocks](https://baeldung.com/mockito-annotations)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Quick Guide to BDDMockito](https://baeldung.com/bdd-mockito)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mockito's Java 8 Features](https://baeldung.com/mockito-2-java-8)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Warning: The type MockitoJUnitRunner is deprecated](https://baeldung.com/mockito-deprecated-mockitojunitrunner)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mockito Strict Stubbing and The UnnecessaryStubbingException](https://baeldung.com/mockito-unnecessary-stubbing-exception)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Injecting Mockito Mocks into Spring Beans](https://baeldung.com/injecting-mocks-in-spring)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mockito.mock() vs @Mock vs @MockBean](https://baeldung.com/java-spring-mockito-mock-mockbean)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mockito and JUnit 5 - Using ExtendWith](https://baeldung.com/mockito-junit-5-extension)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mockito When/Then Cookbook](https://baeldung.com/mockito-behavior)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mockito Verify Cookbook](https://baeldung.com/mockito-verify)

### MockedStatic need for closure

```
java.lang.Exception: Unexpected exception, expected<java.lang.InternalError> but was<org.mockito.exceptions.base.MockitoException>
        at org.junit.internal.runners.statements.ExpectException.evaluate(ExpectException.java:30)
        at org.junit.rules.ExpectedException$ExpectedExceptionStatement.evaluate(ExpectedException.java:258)
        at org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)
Caused by: org.mockito.exceptions.base.MockitoException:
For java.security.MessageDigest, static mocking is already registered in the current thread

To create a new mock, the existing static mock registration must be deregistered
        at nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorMockitoInlineTest.type5UUID_invalidHashAlgorithm_expected(UUIDGeneratorMockitoInlineTest.java:23)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
```

The returned object's MockedStatic.close() method must be called upon completing the test or the mock will remain active on the current thread
(see also [StaticMockTest.java](https://github.com/mockito/mockito/blob/release/3.x/subprojects/inline/src/test/java/org/mockitoinline/StaticMockTest.java)):

```java
    @Test
    public void testStaticMockSimple() {
        assertEquals("foo", Dummy.foo());

        try (MockedStatic<Dummy> ignored = Mockito.mockStatic(Dummy.class)) {
            assertNull(Dummy.foo());
        }

        assertEquals("foo", Dummy.foo());
    }
```

## PowerMock

[![PowerMock](https://freedumbytes.gitlab.io/setup/images/icon/powermock.png "PowerMock") PowerMock](https://github.com/powermock/powermock) is a framework that extends other mock libraries such as EasyMock with more powerful capabilities.
PowerMock uses a custom classloader and bytecode manipulation to enable mocking of static methods, constructors, final classes and methods, private methods, removal of static initializers and more.
By using a custom classloader no changes need to be done to the IDE or continuous integration servers which simplifies adoption.

 * Bootstrapping alternatives instead of using the `PowerMockRunner`:
   * [![PowerMock](https://freedumbytes.gitlab.io/setup/images/icon/powermock.png "PowerMock") JUnit Rule](https://github.com/powermock/powermock/wiki/PowerMockRule)
     * `ExpectedException.expectMessage` AssertionError lost with `PowerMockRule`
       (see also comment [![issue 396](https://img.shields.io/github/issues/detail/state/powermock/powermock/396.svg "issue 396")](https://github.com/powermock/powermock/issues/396))
     * But still not `100%` [Jave Code Coverage](https://github.com/powermock/powermock/wiki/Code-coverage-with-JaCoCo) just like `PowerMockRunner`
       * PowerMock disables EclEmma code coverage
         (see also [![issue 422](https://img.shields.io/github/issues/detail/state/powermock/powermock/422.svg "issue 422")](https://github.com/powermock/powermock/issues/422))
       * PowerMockito `@PrepareForTest` disables JaCoCo
         (see also [![issue 49](https://img.shields.io/github/issues/detail/state/arturdm/jacoco-android-gradle-plugin/49.svg
                    "issue 49")](https://github.com/arturdm/jacoco-android-gradle-plugin/issues/49))
   * [![PowerMock](https://freedumbytes.gitlab.io/setup/images/icon/powermock.png "PowerMock") Java Agent](https://github.com/powermock/powermock/wiki/PowerMockAgent)
     * `ExpectedException.expectMessage` works again
     * Jave Code Coverage is working now also
 * [![PowerMock](https://freedumbytes.gitlab.io/setup/images/icon/powermock.png "PowerMock") Whitebox](https://github.com/powermock/powermock/wiki/Bypass-Encapsulation) class to bypass encapsulation
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") PowerMockito is a PowerMock's extension API to support Mockito](https://baeldung.com/intro-to-powermock)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mocking of Private Methods Using PowerMock](https://baeldung.com/powermock-private-method)
 * [![Captain Debug](https://freedumbytes.gitlab.io/setup/images/icon/blogger.png "Captain Debug") Why Mock Private Methods? ](http://www.captaindebug.com/2011/10/why-use-powermock-to-mock-private.html)
 * [![Captain Debug](https://freedumbytes.gitlab.io/setup/images/icon/blogger.png "Captain Debug") Using PowerMock to Mock Constructors](http://www.captaindebug.com/2011/10/using-powermock-to-mock-constructors.html)

### PowerMockRunner dependencies

```diff
    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.config.dependencies</groupId>
      <artifactId>test</artifactId>
      <type>pom</type>
      <scope>test</scope>
    </dependency>

+    <dependency>
+      <groupId>org.powermock</groupId>
+      <artifactId>powermock-api-mockito2</artifactId>
+      <scope>test</scope>
+    </dependency>
+
+    <dependency>
+      <groupId>org.powermock</groupId>
+      <artifactId>powermock-module-junit4</artifactId>
+      <scope>test</scope>
+    </dependency>
```

### PowerMock JUnit Rule dependencies

```diff
    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.config.dependencies</groupId>
      <artifactId>test</artifactId>
      <type>pom</type>
      <scope>test</scope>
    </dependency>

     <dependency>
       <groupId>org.powermock</groupId>
       <artifactId>powermock-api-mockito2</artifactId>
       <scope>test</scope>
     </dependency>

+    <dependency>
+      <groupId>org.powermock</groupId>
+      <artifactId>powermock-classloading-xstream</artifactId>
+      <scope>test</scope>
+    </dependency>
+
+    <dependency>
+      <groupId>org.powermock</groupId>
+      <artifactId>powermock-module-junit4-rule</artifactId>
+      <scope>test</scope>
+    </dependency>

     <dependency>
       <groupId>org.powermock</groupId>
       <artifactId>powermock-module-junit4</artifactId>
       <scope>test</scope>
     </dependency>
```

### PowerMock Java Agent dependencies

```diff
    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.config.dependencies</groupId>
      <artifactId>test</artifactId>
      <type>pom</type>
      <scope>test</scope>
    </dependency>

     <dependency>
       <groupId>org.powermock</groupId>
       <artifactId>powermock-api-mockito2</artifactId>
       <scope>test</scope>
     </dependency>

-    <dependency>
-      <groupId>org.powermock</groupId>
-      <artifactId>powermock-classloading-xstream</artifactId>
-      <scope>test</scope>
-    </dependency>

-
-    <dependency>
-      <groupId>org.powermock</groupId>
-      <artifactId>powermock-module-junit4-rule</artifactId>
-      <scope>test</scope>
-    </dependency>

+    <dependency>
+      <groupId>org.powermock</groupId>
+      <artifactId>powermock-module-junit4-rule-agent</artifactId>
+      <scope>test</scope>
+    </dependency>

     <dependency>
       <groupId>org.powermock</groupId>
       <artifactId>powermock-module-junit4</artifactId>
       <scope>test</scope>
     </dependency>
```

### Mockito Inline dependencies and conflicts

ByteBuddyAgent install method works with `tools.jar` but fails with `powermock-module-javaagent.jar`
(see also [![issue 813](https://img.shields.io/github/issues/detail/state/raphw/byte-buddy/813.svg "issue 813")](https://github.com/raphw/byte-buddy/issues/813)).

```
Fri 2020-11-20  18:40:24.925 CET [ERROR] type5UUID_invalidHashAlgorithm_exceptionRule(nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorPowerMockRunnerTest)  Time elapsed: 0.002 s  <<< ERROR!
java.lang.RuntimeException: Invoking the beforeTestMethod method on PowerMock test listener org.powermock.api.extension.listener.AnnotationEnabler@5e158f33 failed.
        at org.powermock.tests.utils.impl.PowerMockTestNotifierImpl.notifyBeforeTestMethod(PowerMockTestNotifierImpl.java:84)
        at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl$PowerMockJUnit44MethodRunner.executeTest(PowerMockJUnit44RunnerDelegateImpl.java:308)
        at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner.executeTestInSuper(PowerMockJUnit47RunnerDelegateImpl.java:131)
        at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner.access$100(PowerMockJUnit47RunnerDelegateImpl.java:59)
        at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner$TestExecutorStatement.evaluate(PowerMockJUnit47RunnerDelegateImpl.java:147)
        at org.junit.rules.ExpectedException$ExpectedExceptionStatement.evaluate(ExpectedException.java:258)
Caused by: java.lang.IllegalStateException: java.lang.IllegalStateException: Could not initialize plugin: interface org.mockito.plugins.MockMaker (alternate: null)
        at org.mockito.internal.configuration.InjectingAnnotationEngine.injectMocks(InjectingAnnotationEngine.java:92)
        at org.powermock.api.mockito.internal.configuration.PowerMockitoInjectingAnnotationEngine.process(PowerMockitoInjectingAnnotationEngine.java:40)
Caused by: java.lang.IllegalStateException: Could not initialize plugin: interface org.mockito.plugins.MockMaker (alternate: null)
        at org.mockito.internal.configuration.plugins.PluginLoader$1.invoke(PluginLoader.java:84)
        at com.sun.proxy.$Proxy10.getHandler(Unknown Source)
Caused by: java.lang.IllegalStateException: Failed to load interface org.mockito.plugins.MockMaker implementation declared in sun.misc.CompoundEnumeration@74707df8
        at org.mockito.internal.configuration.plugins.PluginInitializer.loadImpl(PluginInitializer.java:57)
        at org.mockito.internal.configuration.plugins.PluginLoader.loadPlugin(PluginLoader.java:65)
Caused by: java.lang.reflect.InvocationTargetException
        at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
        at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
        at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
        at java.lang.reflect.Constructor.newInstance(Constructor.java:423)
Caused by: org.mockito.exceptions.base.MockitoInitializationException:
Could not initialize inline Byte Buddy mock maker.

It appears as if you are running an incomplete JVM installation that might not support all tooling APIs
Java               : 1.8
JVM vendor name    : Oracle Corporation
JVM vendor version : 25.202-b08
JVM name           : Java HotSpot(TM) 64-Bit Server VM
JVM version        : 1.8.0_202-b08
JVM info           : mixed mode
OS name            : Windows 10
OS version         : 10.0

        at org.mockito.internal.creation.bytebuddy.InlineByteBuddyMockMaker.<init>(InlineByteBuddyMockMaker.java:234)
Caused by: java.lang.IllegalStateException: Error during attachment using: net.bytebuddy.agent.ByteBuddyAgent$AttachmentProvider$Compound@1a2724d3
        at net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:613)
        at org.mockito.internal.creation.bytebuddy.InlineByteBuddyMockMaker.<clinit>(InlineByteBuddyMockMaker.java:117)
Caused by: java.lang.reflect.InvocationTargetException
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
Caused by: java.lang.NullPointerException
        at com.sun.tools.attach.VirtualMachine.attach(VirtualMachine.java:182)

Fri 2020-11-20  18:40:24.943 CET [INFO] Running nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorPowerWhiteboxTest
Fri 2020-11-20  18:40:24.975 CET [ERROR] Tests run: 1, Failures: 0, Errors: 1, Skipped: 0, Time elapsed: 0.032 s <<< FAILURE!
                                        - in nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorPowerWhiteboxTest
Fri 2020-11-20  18:40:24.975 CET [ERROR] powerMockWhitebox_finalClass(nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorPowerWhiteboxTest)  Time elapsed: 0.032 s  <<< ERROR!
java.lang.IllegalStateException: Cannot clear JavaAgentClassRegister. Set method has not been called.
        at org.powermock.api.extension.agent.JavaAgentFrameworkRegisterImpl.clear(JavaAgentFrameworkRegisterImpl.java:41)
        at org.powermock.modules.junit4.rule.PowerMockStatement.clearFrameworkAgentClassRegister(PowerMockRule.java:84)
        at org.powermock.modules.junit4.rule.PowerMockStatement.evaluate(PowerMockRule.java:78)
        at org.junit.rules.ExpectedException$ExpectedExceptionStatement.evaluate(ExpectedException.java:258)
```

Thus removed `powermock-module-junit4-rule-agent`:

```diff
    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.config.dependencies</groupId>
      <artifactId>test</artifactId>
      <type>pom</type>
      <scope>test</scope>
    </dependency>

+    <dependency>
+      <groupId>org.mockito</groupId>
+      <artifactId>mockito-inline</artifactId>
+      <scope>test</scope>
+    </dependency>

     <dependency>
       <groupId>org.powermock</groupId>
       <artifactId>powermock-api-mockito2</artifactId>
       <scope>test</scope>
     </dependency>

-    <dependency>
-      <groupId>org.powermock</groupId>
-      <artifactId>powermock-module-junit4-rule-agent</artifactId>
-      <scope>test</scope>
-    </dependency>

     <dependency>
       <groupId>org.powermock</groupId>
       <artifactId>powermock-module-junit4</artifactId>
       <scope>test</scope>
     </dependency>
```

But it is still possible to run into PowerMockRunner with `mock-maker-inline` Classloader issues
(see also [![issue 1034](https://img.shields.io/github/issues/detail/state/powermock/powermock/1034.svg "issue 1034")](https://github.com/powermock/powermock/issues/1034)).

**Tip**: Using dependency `mockito-inline` removes the need for the following configuration of file `org.mockito.plugins.MockMaker` in folder `src/test/resources/mockito-extensions` containing the following line of text:
`mock-maker-inline` as described in:

 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") Mock Final Classes and Methods with Mockito](https://baeldung.com/mockito-final)

### PowerMock VerifyError

Powermock agent dies with `VerifyError` (see also [![issue 558](https://img.shields.io/github/issues/detail/state/powermock/powermock/558.svg "issue 558")](https://github.com/powermock/powermock/issues/558)):

```
Thu 2019-07-04  18:42:53.772 CEST [INFO] Running nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorPowerMockTest
Thu 2019-07-04  18:42:54.145 CEST [ERROR] Tests run: 1, Failures: 0, Errors: 1, Skipped: 0, Time elapsed: 0.359 s <<< FAILURE!
                                         - in nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorPowerMockTest
Thu 2019-07-04  18:42:54.147 CEST [ERROR] initializationError(nl.demon.shadowland.freedumbytes.uuid.UUIDGeneratorPowerMockTest)  Time elapsed: 0.046 s  <<< ERROR!
java.lang.VerifyError:
Expecting a stackmap frame at branch target 30
Exception Details:
  Location:
    nl/demon/shadowland/freedumbytes/uuid/UUIDGeneratorPowerMockTest.<init>()V @8: aload_1
  Reason:
    Expected stackmap frame at this location.
  Bytecode:
    0x0000000: b800 184c 2ab7 001a 2b03 0454 2a2b 0404
    0x0000010: 54b8 0020 b500 222b 0504 54a7 0012 3a04
    0x0000020: 014e 2ab8 0028 1904 2b06 0454 bf01 4e2a
    0x0000030: b800 282b 0704 54b1
  Exception Handler Table:
    bci [8, 30] => handler: 30

        at java.lang.Class.getDeclaredMethods0(Native Method)
        at java.lang.Class.privateGetDeclaredMethods(Class.java:2701)
        at java.lang.Class.privateGetPublicMethods(Class.java:2902)
        at java.lang.Class.getMethods(Class.java:1615)
        at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl.getTestMethods(PowerMockJUnit44RunnerDelegateImpl.java:109)
        at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl.<init>(PowerMockJUnit44RunnerDelegateImpl.java:85)
```

Workaround by adding the jvm parameter `-noverify`:

```xml
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <argLine>-noverify ${argLine}</argLine>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
        <configuration>
          <argLine>-noverify ${argLine}</argLine>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

Possibly solved in [![PowerMock 2.0.7 and higher](https://img.shields.io/badge/PowerMock-2.0.7_%2B-D5AF1B "PowerMock 2.0.7 and higher")](https://github.com/powermock/powermock/compare/powermock-2.0.6...powermock-2.0.7)
with Fix missing stackmap frames (see also [![pull request 1043](https://img.shields.io/github/issues/detail/state/powermock/powermock/1043.svg "pull request 1043")](https://github.com/powermock/powermock/pull/1043)).

## Maven

[![Maven](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven") Maven](https://maven.apache.org) a Yiddish word _meaning accumulator of knowledge_, began as an attempt to simplify the build processes in the Jakarta Turbine project.
Based on the concept of a project object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information.

### Log4j2 Appenders Console

The following Log4j2 Console Appender configuration:

```xml
  <Appenders>
    <Console name="StdOut">
      <PatternLayout pattern="${LOG_PATTERN}" charset="UTF-8" disableAnsi="false" />
    </Console>
  </Appenders>
```

Could cause the following message by Surefire Plugin during `mvn test`:

```
Mon 2021-09-20  14:42:50.630 CEST [INFO] --- maven-surefire-plugin:2.22.2:test (default-test) @ test-utils ---
Mon 2021-09-20  14:42:50.892 CEST [INFO]
Mon 2021-09-20  14:42:50.892 CEST [INFO] -------------------------------------------------------
Mon 2021-09-20  14:42:50.892 CEST [INFO]  T E S T S
Mon 2021-09-20  14:42:50.892 CEST [INFO] -------------------------------------------------------
Mon 2021-09-20  14:42:53.469 CEST [INFO] Running nl.demon.shadowland.freedumbytes.unit.junit.jupiter.ExceptionAssertionUnitTest
Mon 2021-09-20  14:42:53.539 CEST [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.049 s
Mon 2021-09-20  14:42:53.545 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.CommonsLoggingLoggerRuleTest
Mon 2021-09-20  14:42:54.278 CEST [WARNING] Corrupted STDOUT by directly writing to native stream in forked JVM 1.
                                            See FAQ web page and the dump file test-utils\target\surefire-reports\2021-09-20T14-42-50_483-jvmRun1.dumpstream
Mon 2021-09-20  14:42:54.401 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.852 s
Mon 2021-09-20  14:42:54.402 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.JavaUtilLoggingLoggerRuleTest
Mon 2021-09-20  14:42:54.409 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 s
Mon 2021-09-20  14:42:54.410 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Log4j1LoggerRuleTest
Mon 2021-09-20  14:42:54.424 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.011 s
Mon 2021-09-20  14:42:54.424 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Log4jLoggerRulePackageTest
Mon 2021-09-20  14:42:54.439 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.013 s
Mon 2021-09-20  14:42:54.440 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Log4jLoggerRuleTest
Mon 2021-09-20  14:42:54.489 CEST [INFO] Tests run: 10, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.046 s
Mon 2021-09-20  14:42:54.490 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Slf4jLoggerRuleTest
Mon 2021-09-20  14:42:54.508 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.015 s
Mon 2021-09-20  14:42:54.508 CEST [INFO] Running nl.demon.shadowland.freedumbytes.unit.junit.ExceptionAssertionUnitTest
Mon 2021-09-20  14:42:54.510 CEST [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 s
Mon 2021-09-20  14:42:55.204 CEST [INFO]
Mon 2021-09-20  14:42:55.204 CEST [INFO] Results:
Mon 2021-09-20  14:42:55.204 CEST [INFO]
Mon 2021-09-20  14:42:55.204 CEST [INFO] Tests run: 22, Failures: 0, Errors: 0, Skipped: 0
```

And the following stacktraces in `test-utils\target\surefire-reports\2021-09-20T14-42-50_483-jvmRun1.dumpstream`:

```
# Created at 2021-09-20T14:42:54.277
Corrupted STDOUT by directly writing to native stream in forked JVM 1.
  Stream '2021-09-20 14:42:54.267  FATAL 10224 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : fatal message'.
java.lang.IllegalArgumentException: Stream stdin corrupted.
  Expected comma after third character in command '2021-09-20 14:42:54.267  FATAL 10224 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : fatal message'.
	at org.apache.maven.plugin.surefire.booterclient.output.ForkClient$OperationalData.<init>(ForkClient.java:507)
	at org.apache.maven.plugin.surefire.booterclient.output.ForkClient.processLine(ForkClient.java:210)
	at org.apache.maven.plugin.surefire.booterclient.output.ForkClient.consumeLine(ForkClient.java:177)
	at org.apache.maven.plugin.surefire.booterclient.output.ThreadedStreamConsumer$Pumper.run(ThreadedStreamConsumer.java:88)
	at java.lang.Thread.run(Thread.java:748)


# Created at 2021-09-20T14:42:54.279
Corrupted STDOUT by directly writing to native stream in forked JVM 1.
  Stream '2021-09-20 14:42:54.275  ERROR 10224 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : error message'.
java.lang.IllegalArgumentException: Stream stdin corrupted.
  Expected comma after third character in command '2021-09-20 14:42:54.275  ERROR 10224 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : error message'.
	at org.apache.maven.plugin.surefire.booterclient.output.ForkClient$OperationalData.<init>(ForkClient.java:507)
	at org.apache.maven.plugin.surefire.booterclient.output.ForkClient.processLine(ForkClient.java:210)
	at org.apache.maven.plugin.surefire.booterclient.output.ForkClient.consumeLine(ForkClient.java:177)
	at org.apache.maven.plugin.surefire.booterclient.output.ThreadedStreamConsumer$Pumper.run(ThreadedStreamConsumer.java:88)
	at java.lang.Thread.run(Thread.java:748)
```

Just add the `follow` attribute:

```xml
  <Appenders>
    <Console name="StdOut" follow="true">
      <PatternLayout pattern="${LOG_PATTERN}" charset="UTF-8" disableAnsi="false" />
    </Console>
  </Appenders>
```

With the following result:

```
Mon 2021-09-20  15:12:27.810 CEST [INFO] --- maven-surefire-plugin:2.22.2:test (default-test) @ test-utils ---
Mon 2021-09-20  15:12:27.971 CEST [INFO]
Mon 2021-09-20  15:12:27.971 CEST [INFO] -------------------------------------------------------
Mon 2021-09-20  15:12:27.971 CEST [INFO]  T E S T S
Mon 2021-09-20  15:12:27.971 CEST [INFO] -------------------------------------------------------
Mon 2021-09-20  15:12:29.844 CEST [INFO] Running nl.demon.shadowland.freedumbytes.unit.junit.jupiter.ExceptionAssertionUnitTest
Mon 2021-09-20  15:12:29.915 CEST [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.048 s
Mon 2021-09-20  15:12:29.925 CEST [INFO] Running nl.demon.shadowland.freedumbytes.unit.junit.ExceptionAssertionUnitTest
Mon 2021-09-20  15:12:29.942 CEST [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.012 s
Mon 2021-09-20  15:12:29.942 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Slf4jLoggerRuleTest
2021-09-20 15:12:30.579  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Slf4jEventLogger         : error message
2021-09-20 15:12:30.586   WARN 11820 --- [           main] n.d.s.f.t.u.r.l.Slf4jEventLogger         : warn message
2021-09-20 15:12:30.587   INFO 11820 --- [           main] n.d.s.f.t.u.r.l.Slf4jEventLogger         : info message
2021-09-20 15:12:30.587  DEBUG 11820 --- [           main] n.d.s.f.t.u.r.l.Slf4jEventLogger         : debug message
2021-09-20 15:12:30.588  TRACE 11820 --- [           main] n.d.s.f.t.u.r.l.Slf4jEventLogger         : trace message
Mon 2021-09-20  15:12:30.695 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.749 s
Mon 2021-09-20  15:12:30.696 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Log4jLoggerRuleTest
2021-09-20 15:12:30.693  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : fatal message
2021-09-20 15:12:30.694  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.694   WARN 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : warn message
2021-09-20 15:12:30.694   INFO 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : info message
2021-09-20 15:12:30.695  DEBUG 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : debug message
2021-09-20 15:12:30.695  TRACE 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : trace message
2021-09-20 15:12:30.699  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.722  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.725  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.729  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : fatal message
2021-09-20 15:12:30.729  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.729   WARN 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : warn message
2021-09-20 15:12:30.730   INFO 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : info message
2021-09-20 15:12:30.734  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.736  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.738  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : fatal message
2021-09-20 15:12:30.738  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.739   WARN 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : warn message
2021-09-20 15:12:30.739   INFO 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : info message
2021-09-20 15:12:30.739  DEBUG 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : debug message
2021-09-20 15:12:30.740  TRACE 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : trace message
2021-09-20 15:12:30.741  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLoggerExtra    : fatal extra message
Mon 2021-09-20  15:12:30.744 CEST [INFO] Tests run: 10, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.046 s
Mon 2021-09-20  15:12:30.745 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Log4jLoggerRulePackageTest
2021-09-20 15:12:30.743  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : fatal message
2021-09-20 15:12:30.744  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.744   WARN 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : warn message
2021-09-20 15:12:30.745   INFO 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : info message
2021-09-20 15:12:30.745  DEBUG 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : debug message
2021-09-20 15:12:30.745  TRACE 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : trace message
2021-09-20 15:12:30.748  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : fatal message
2021-09-20 15:12:30.748  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : error message
2021-09-20 15:12:30.749   WARN 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : warn message
2021-09-20 15:12:30.751   INFO 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : info message
2021-09-20 15:12:30.751  DEBUG 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : debug message
2021-09-20 15:12:30.751  TRACE 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLogger         : trace message
2021-09-20 15:12:30.752  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4jEventLoggerExtra    : fatal extra message
Mon 2021-09-20  15:12:30.755 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.008 s
Mon 2021-09-20  15:12:30.756 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.Log4j1LoggerRuleTest
2021-09-20 15:12:30.763  FATAL 11820 --- [           main] n.d.s.f.t.u.r.l.Log4j1EventLogger        : fatal message
2021-09-20 15:12:30.764  ERROR 11820 --- [           main] n.d.s.f.t.u.r.l.Log4j1EventLogger        : error message
2021-09-20 15:12:30.764   WARN 11820 --- [           main] n.d.s.f.t.u.r.l.Log4j1EventLogger        : warn message
2021-09-20 15:12:30.764   INFO 11820 --- [           main] n.d.s.f.t.u.r.l.Log4j1EventLogger        : info message
2021-09-20 15:12:30.764  DEBUG 11820 --- [           main] n.d.s.f.t.u.r.l.Log4j1EventLogger        : debug message
2021-09-20 15:12:30.764  TRACE 11820 --- [           main] n.d.s.f.t.u.r.l.Log4j1EventLogger        : trace message
Mon 2021-09-20  15:12:30.771 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.012 s
Mon 2021-09-20  15:12:30.772 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.JavaUtilLoggingLoggerRuleTest
2021-09-20 15:12:30.770  ERROR 11820 --- [           main] d.s.f.t.u.r.l.JavaUtilLoggingEventLogger : severe message
2021-09-20 15:12:30.771   WARN 11820 --- [           main] d.s.f.t.u.r.l.JavaUtilLoggingEventLogger : warning message
2021-09-20 15:12:30.771   INFO 11820 --- [           main] d.s.f.t.u.r.l.JavaUtilLoggingEventLogger : info message
2021-09-20 15:12:30.772 CONFIG 11820 --- [           main] d.s.f.t.u.r.l.JavaUtilLoggingEventLogger : config message
2021-09-20 15:12:30.772  DEBUG 11820 --- [           main] d.s.f.t.u.r.l.JavaUtilLoggingEventLogger : fine message
2021-09-20 15:12:30.773  TRACE 11820 --- [           main] d.s.f.t.u.r.l.JavaUtilLoggingEventLogger : finer message
2021-09-20 15:12:30.773 FINEST 11820 --- [           main] d.s.f.t.u.r.l.JavaUtilLoggingEventLogger : finest message
Mon 2021-09-20  15:12:30.779 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.005 s
Mon 2021-09-20  15:12:30.780 CEST [INFO] Running nl.demon.shadowland.freedumbytes.test.unit.rule.logger.CommonsLoggingLoggerRuleTest
2021-09-20 15:12:30.799  FATAL 11820 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : fatal message
2021-09-20 15:12:30.800  ERROR 11820 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : error message
2021-09-20 15:12:30.800   WARN 11820 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : warn message
2021-09-20 15:12:30.801   INFO 11820 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : info message
2021-09-20 15:12:30.801  DEBUG 11820 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : debug message
2021-09-20 15:12:30.802  TRACE 11820 --- [           main] .d.s.f.t.u.r.l.CommonsLoggingEventLogger : trace message
Mon 2021-09-20  15:12:30.808 CEST [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.026 s
Mon 2021-09-20  15:12:31.517 CEST [INFO]
Mon 2021-09-20  15:12:31.517 CEST [INFO] Results:
Mon 2021-09-20  15:12:31.518 CEST [INFO]
Mon 2021-09-20  15:12:31.518 CEST [INFO] Tests run: 22, Failures: 0, Errors: 0, Skipped: 0
```

### Corrupted STDOUT by directly writing to native stream in forked JVM

The `forkNode` configuration is an alternative solution for `Corrupted STDOUT` that is available
from [![Maven Surefire Plugin 3.0.0-M5 and higher](https://img.shields.io/badge/Maven_Surefire_Plugin-3.0.0_M5_%2B-D5AF1B "Maven Surefire Plugin 3.0.0-M5 and higher")](http://maven.apache.org/surefire/maven-surefire-plugin/test-mojo.html):

```xml
  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>3.0.0-M5</version>
          <configuration>
            <forkNode implementation="org.apache.maven.plugin.surefire.extensions.SurefireForkNodeFactory" />
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

Also see related issue:

 * [![issue SUREFIRE-1614](https://img.shields.io/jira/issue/https/issues.apache.org/jira/SUREFIRE-1614.svg "issue SUREFIRE-1614")](https://issues.apache.org/jira/browse/SUREFIRE-1614)
   JUnit Runner that writes to System.out corrupts Surefire's STDOUT when using JUnit's Vintage Engine.
 * [![issue SUREFIRE-1659](https://img.shields.io/jira/issue/https/issues.apache.org/jira/SUREFIRE-1659.svg "issue SUREFIRE-1659")](https://issues.apache.org/jira/browse/SUREFIRE-1659)
   Log4j logger in TestExecutionListener corrupts Surefire's STDOUT.
 * [![issue SUREFIRE-1881](https://img.shields.io/jira/issue/https/issues.apache.org/jira/SUREFIRE-1881.svg "issue SUREFIRE-1881")](https://issues.apache.org/jira/browse/SUREFIRE-1881)
   Java agent printing to native console makes build block when using SurefireForkNodeFactory.
 * [![issue SUREFIRE-1788](https://img.shields.io/jira/issue/https/issues.apache.org/jira/SUREFIRE-1788.svg "issue SUREFIRE-1788")](https://issues.apache.org/jira/browse/SUREFIRE-1788)
   Unhandled native logs in SurefireForkChannel.

### Log4j2 JMX

Log4j 2 has built-in support for [JMX](https://logging.apache.org/log4j//2.x/manual/jmx.html).
The StatusLogger, ContextSelector, and all LoggerContexts, LoggerConfigs and Appenders are instrumented with MBeans and can be remotely monitored and controlled.

When running the Surefire Plugin with extra `executions` to test different `java.util.logging.manager` configurations:

```xml
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <excludes>
            <exclude>**/LogManagerTest.java</exclude>
            <exclude>**/Slf4jLogManagerTest.java</exclude>
          </excludes>
        </configuration>
        <executions>
          <execution>
            <id>surefire-jul</id>
            <phase>test</phase>
            <goals>
              <goal>test</goal>
            </goals>
            <configuration>
              <includes>
                <include>**/LogManagerTest.java</include>
              </includes>
              <excludes>
                <exclude>**/Slf4jLogManagerTest.java</exclude>
              </excludes>
              <systemPropertyVariables>
                <java.util.logging.manager>java.util.logging.LogManager</java.util.logging.manager>
              </systemPropertyVariables>
            </configuration>
          </execution>
          <execution>
            <id>surefire-slf4j</id>
            <phase>test</phase>
            <goals>
              <goal>test</goal>
            </goals>
            <configuration>
              <forkNode implementation="org.apache.maven.plugin.surefire.extensions.SurefireForkNodeFactory" />
              <includes>
                <include>**/Slf4jLogManagerTest.java</include>
              </includes>
              <excludes>
                <exclude>**/LogManagerTest.java</exclude>
              </excludes>
              <systemPropertyVariables>
                <java.util.logging.manager>nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager</java.util.logging.manager>
              </systemPropertyVariables>
            </configuration>
          </execution>
        </executions>
      </plugin>
```

The default JMX activation of Log4j2 might cause the following reconfigure ERROR:

```
Tue 2021-09-21  19:09:03.794 CEST [INFO] --- maven-surefire-plugin:3.0.0-M5:test (default-test) @ log-utils ---
Tue 2021-09-21  19:09:04.086 CEST [INFO]
Tue 2021-09-21  19:09:04.087 CEST [INFO] -------------------------------------------------------
Tue 2021-09-21  19:09:04.087 CEST [INFO]  T E S T S
Tue 2021-09-21  19:09:04.087 CEST [INFO] -------------------------------------------------------
Tue 2021-09-21  19:09:05.237 CEST [INFO] Running nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLoggerWrapperTest
Tue 2021-09-21  19:09:06.746 CEST [INFO] Tests run: 18, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.506 s
Tue 2021-09-21  19:09:06.747 CEST [INFO] Running nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapperTest
Tue 2021-09-21  19:09:06.787 CEST [INFO] Tests run: 17, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.051 s
Tue 2021-09-21  19:09:06.959 CEST [INFO]
Tue 2021-09-21  19:09:06.959 CEST [INFO] Results:
Tue 2021-09-21  19:09:06.959 CEST [INFO]
Tue 2021-09-21  19:09:06.960 CEST [INFO] Tests run: 35, Failures: 0, Errors: 0, Skipped: 0
Tue 2021-09-21  19:09:06.960 CEST [INFO]
Tue 2021-09-21  19:09:06.973 CEST [INFO]
Tue 2021-09-21  19:09:06.974 CEST [INFO] --- maven-surefire-plugin:3.0.0-M5:test (surefire-jul) @ log-utils ---
Tue 2021-09-21  19:09:07.002 CEST [INFO]
Tue 2021-09-21  19:09:07.002 CEST [INFO] -------------------------------------------------------
Tue 2021-09-21  19:09:07.003 CEST [INFO]  T E S T S
Tue 2021-09-21  19:09:07.003 CEST [INFO] -------------------------------------------------------
Tue 2021-09-21  19:09:08.096 CEST [INFO] Running nl.demon.shadowland.freedumbytes.java.util.logging.manager.LogManagerTest
Tue 2021-09-21  19:09:09.611 CEST [INFO] Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.508 s
Tue 2021-09-21  19:09:09.802 CEST [INFO]
Tue 2021-09-21  19:09:09.803 CEST [INFO] Results:
Tue 2021-09-21  19:09:09.803 CEST [INFO]
Tue 2021-09-21  19:09:09.803 CEST [INFO] Tests run: 4, Failures: 0, Errors: 0, Skipped: 0
Tue 2021-09-21  19:09:09.804 CEST [INFO]
Tue 2021-09-21  19:09:09.810 CEST [INFO]
Tue 2021-09-21  19:09:09.810 CEST [INFO] --- maven-surefire-plugin:3.0.0-M5:test (surefire-slf4j) @ log-utils ---
Tue 2021-09-21  19:09:09.839 CEST [INFO]
Tue 2021-09-21  19:09:09.840 CEST [INFO] -------------------------------------------------------
Tue 2021-09-21  19:09:09.840 CEST [INFO]  T E S T S
Tue 2021-09-21  19:09:09.840 CEST [INFO] -------------------------------------------------------
Tue 2021-09-21  19:09:11.317 CEST [INFO] 2021-09-21 19:09:11,314 main ERROR Could not reconfigure JMX java.lang.ExceptionInInitializerError
Tue 2021-09-21  19:09:11.318 CEST [INFO]        at javax.management.MBeanServerDelegate.<init>(MBeanServerDelegate.java:72)
Tue 2021-09-21  19:09:11.319 CEST [INFO]        at com.sun.jmx.mbeanserver.MBeanServerDelegateImpl.<init>(MBeanServerDelegateImpl.java:100)
Tue 2021-09-21  19:09:11.319 CEST [INFO]        at com.sun.jmx.mbeanserver.JmxMBeanServer.newMBeanServerDelegate(JmxMBeanServer.java:1374)
Tue 2021-09-21  19:09:11.320 CEST [INFO]        at javax.management.MBeanServerBuilder.newMBeanServerDelegate(MBeanServerBuilder.java:66)
Tue 2021-09-21  19:09:11.320 CEST [INFO]        at javax.management.MBeanServerFactory.newMBeanServer(MBeanServerFactory.java:321)
Tue 2021-09-21  19:09:11.321 CEST [INFO]        at javax.management.MBeanServerFactory.createMBeanServer(MBeanServerFactory.java:231)
Tue 2021-09-21  19:09:11.321 CEST [INFO]        at javax.management.MBeanServerFactory.createMBeanServer(MBeanServerFactory.java:192)
Tue 2021-09-21  19:09:11.322 CEST [INFO]        at java.lang.management.ManagementFactory.getPlatformMBeanServer(ManagementFactory.java:469)
Tue 2021-09-21  19:09:11.322 CEST [INFO]        at org.apache.logging.log4j.core.jmx.Server.reregisterMBeansAfterReconfigure(Server.java:140)
Tue 2021-09-21  19:09:11.328 CEST [INFO]        at nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager.<clinit>(Slf4jLogManager.java:23)
Tue 2021-09-21  19:09:11.337 CEST [INFO]        at org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:548)
Tue 2021-09-21  19:09:11.338 CEST [INFO] Caused by: java.lang.NullPointerException
Tue 2021-09-21  19:09:11.338 CEST [INFO]        at java.util.logging.Logger.demandLogger(Logger.java:455)
Tue 2021-09-21  19:09:11.338 CEST [INFO]        at java.util.logging.Logger.getLogger(Logger.java:502)
Tue 2021-09-21  19:09:11.339 CEST [INFO]        at com.sun.jmx.remote.util.ClassLogger.<init>(ClassLogger.java:55)
Tue 2021-09-21  19:09:11.339 CEST [INFO]        at javax.management.NotificationBroadcasterSupport.<clinit>(NotificationBroadcasterSupport.java:365)
Tue 2021-09-21  19:09:11.340 CEST [INFO]        ... 50 more
Tue 2021-09-21  19:09:11.340 CEST [INFO]
Tue 2021-09-21  19:09:11.379 CEST [INFO] 2021-09-21 19:09:11.374   INFO 752 --- [           main] org.junit.platform.launcher.core.ServiceLoaderTestEngineRegistry            :
                                         Discovered TestEngines with IDs:
                                           [junit-jupiter (group ID: org.junit.jupiter, artifact ID: junit-jupiter-engine, version: 5.7.2, location: JupiterTestEngine.class),
                                            junit-vintage (group ID: org.junit.vintage, artifact ID: junit-vintage-engine, version: 5.7.2, location: VintageTestEngine.class)]
Tue 2021-09-21  19:09:11.388 CEST [INFO] 2021-09-21 19:09:11.387   INFO 752 --- [           main] org.junit.platform.launcher.core.ServiceLoaderPostDiscoveryFilterRegistry   : Loaded PostDiscoveryFilter instances: []
Tue 2021-09-21  19:09:11.403 CEST [INFO] 2021-09-21 19:09:11.403   INFO 752 --- [           main] org.junit.platform.launcher.core.ServiceLoaderTestExecutionListenerRegistry : Loaded TestExecutionListener instances: []
Tue 2021-09-21  19:09:11.720 CEST [INFO] Running nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManagerTest
Tue 2021-09-21  19:09:12.457 CEST [INFO] 2021-09-21 19:09:12.453   INFO 752 --- [           main] nl.demon.shadowla.freedumb.java.util.logging.manager.Slf4jLogManager        : Slf4jLogManager is installed.
Tue 2021-09-21  19:09:12.491 CEST [INFO] Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.765 s
Tue 2021-09-21  19:09:12.495 CEST [INFO] 2021-09-21 19:09:12,494 pool-2-thread-1 ERROR Unable to unregister MBeans java.lang.NoClassDefFoundError: Could not initialize class javax.management.NotificationBroadcasterSupport
Tue 2021-09-21  19:09:12.496 CEST [INFO]        at javax.management.MBeanServerDelegate.<init>(MBeanServerDelegate.java:72)
Tue 2021-09-21  19:09:12.496 CEST [INFO]        at com.sun.jmx.mbeanserver.MBeanServerDelegateImpl.<init>(MBeanServerDelegateImpl.java:100)
Tue 2021-09-21  19:09:12.497 CEST [INFO]        at com.sun.jmx.mbeanserver.JmxMBeanServer.newMBeanServerDelegate(JmxMBeanServer.java:1374)
Tue 2021-09-21  19:09:12.497 CEST [INFO]        at javax.management.MBeanServerBuilder.newMBeanServerDelegate(MBeanServerBuilder.java:66)
Tue 2021-09-21  19:09:12.498 CEST [INFO]        at javax.management.MBeanServerFactory.newMBeanServer(MBeanServerFactory.java:321)
Tue 2021-09-21  19:09:12.498 CEST [INFO]        at javax.management.MBeanServerFactory.createMBeanServer(MBeanServerFactory.java:231)
Tue 2021-09-21  19:09:12.499 CEST [INFO]        at javax.management.MBeanServerFactory.createMBeanServer(MBeanServerFactory.java:192)
Tue 2021-09-21  19:09:12.499 CEST [INFO]        at java.lang.management.ManagementFactory.getPlatformMBeanServer(ManagementFactory.java:469)
Tue 2021-09-21  19:09:12.500 CEST [INFO]        at org.apache.logging.log4j.core.jmx.Server.unregisterLoggerContext(Server.java:248)
Tue 2021-09-21  19:09:12.500 CEST [INFO]        at org.apache.logging.log4j.core.LoggerContext.stop(LoggerContext.java:381)
Tue 2021-09-21  19:09:12.500 CEST [INFO]        at org.apache.logging.log4j.core.LoggerContext$1.run(LoggerContext.java:322)
Tue 2021-09-21  19:09:12.501 CEST [INFO]        at org.apache.logging.log4j.core.util.DefaultShutdownCallbackRegistry$RegisteredCancellable.run(DefaultShutdownCallbackRegistry.java:119)
Tue 2021-09-21  19:09:12.501 CEST [INFO]        at org.apache.logging.log4j.core.util.DefaultShutdownCallbackRegistry.run(DefaultShutdownCallbackRegistry.java:78)
Tue 2021-09-21  19:09:12.502 CEST [INFO]        at java.lang.Thread.run(Thread.java:748)
Tue 2021-09-21  19:09:12.502 CEST [INFO]
Tue 2021-09-21  19:09:13.170 CEST [INFO]
Tue 2021-09-21  19:09:13.170 CEST [INFO] Results:
Tue 2021-09-21  19:09:13.170 CEST [INFO]
Tue 2021-09-21  19:09:13.171 CEST [INFO] Tests run: 4, Failures: 0, Errors: 0, Skipped: 0
```

Just disable JMX for the exection with id `surefire-slf4j` that ran into the error:

```diff
           <execution>
             <id>surefire-slf4j</id>
             <phase>test</phase>
             <goals>
               <goal>test</goal>
             </goals>
             <configuration>
               <forkNode implementation="org.apache.maven.plugin.surefire.extensions.SurefireForkNodeFactory" />
               <includes>
                 <include>**/Slf4jLogManagerTest.java</include>
               </includes>
               <excludes>
                 <exclude>**/LogManagerTest.java</exclude>
               </excludes>
               <systemPropertyVariables>
                 <java.util.logging.manager>nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager</java.util.logging.manager>
+                <log4j2.disableJmx>true</log4j2.disableJmx>
               </systemPropertyVariables>
             </configuration>
           </execution>
```

### Versioning and Releasing

To increment the project version with the `build-helper-maven-plugin` in combination with the `versions-maven-plugin ` via command line:

 * [![DEV Community](https://freedumbytes.gitlab.io/setup/images/icon/dev-to.png "DEV Community") Maven Plugin Configuration - The (Unknown) Tiny Details](https://dev.to/khmarbaise/maven-plugin-configuration-the-unknown-tiny-details-1emm)
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Build Helper Maven Plugin - parse-version](https://www.mojohaus.org/build-helper-maven-plugin/parse-version-mojo.html)
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache")  Versions Maven Plugin - set](https://www.mojohaus.org/versions-maven-plugin/set-mojo.html)

Configure the Maven Setup `pom.xml` as follows:

```xml
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>versions-maven-plugin</artifactId>
          <version>${versionsMavenPluginVersion}</version>
          <configuration>
            <generateBackupPoms>false</generateBackupPoms>
          </configuration>
          <executions>
            <execution>
              <id>major</id>
              <goals>
                <goal>set</goal>
              </goals>
              <configuration>
                <newVersion>${parsedVersion.nextMajorVersion}.0.0-SNAPSHOT</newVersion>
              </configuration>
            </execution>
            <execution>
              <id>minor</id>
              <goals>
                <goal>set</goal>
              </goals>
              <configuration>
                <newVersion>${parsedVersion.majorVersion}.${parsedVersion.nextMinorVersion}.0-SNAPSHOT</newVersion>
              </configuration>
            </execution>
            <execution>
              <id>patch</id>
              <goals>
                <goal>set</goal>
              </goals>
              <configuration>
                <newVersion>${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.nextIncrementalVersion}-SNAPSHOT</newVersion>
              </configuration>
            </execution>
          </executions>
        </plugin>
      </plugins>
    </pluginManagement>
```

With this configuration it is now possible to release the project and override the default, by _prepare for next development iteration_ created, patch version with the next desired minor or major version without the need to manually enter it:

```
git checkout -b release-x.y.z
mvn release:clean
mvn -B release:prepare
mvn build-helper:parse-version versions:set@minor
git add . -u
git commit --amend --no-edit
mvn release:perform
git push origin release-x.y.z
git push origin tag-x.y.z
```

**Note**: In case the CI system deploys the Maven artifacts replace `mvn release:perform` by `mvn release:clean`.

**Tip**: When the next release will be a major one use `versions:set@major` or remove the option `-B` upon `release:prepare` to change the suggested `release version` and then again accept the other suggested settings.

In similar fashion create a patch branch based on release `tag-x.y.z` with the next patch version:

```
git checkout tag-x.y.z
git checkout -b patch-x.y.z
mvn build-helper:parse-version versions:set@patch
git add . -u
git commit -m "Prepare patch x.y.z++."
```

 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Maven Release Plugin](https://maven.apache.org/maven-release/maven-release-plugin)
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Clean a Release](https://maven.apache.org/maven-release/maven-release-plugin/examples/clean-release.html)
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Performing a Non-interactive Release](https://maven.apache.org/maven-release/maven-release-plugin/examples/non-interactive-release.html)
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Prepare a Release](https://maven.apache.org/maven-release/maven-release-plugin/examples/prepare-release.html)
   * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Perform a Release](https://maven.apache.org/maven-release/maven-release-plugin/examples/perform-release.html)

### Resolving conflicts using the dependency tree

 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Introduction to the Dependency Mechanism](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html)
 * [![Apache](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache") Enforcer Plugin - Dependency Convergence](https://maven.apache.org/enforcer/enforcer-rules/dependencyConvergence.html)
 * [![DZone](https://freedumbytes.gitlab.io/setup/images/icon/dzone.png "DZone") Solving Dependency Conflicts in Maven](https://dzone.com/articles/solving-dependency-conflicts-in-maven)
 * [![Baeldung](https://freedumbytes.gitlab.io/setup/images/icon/baeldung.png "Baeldung") How to Resolve a Version Collision of Artifacts in Maven](https://baeldung.com/maven-version-collision)

A project's dependency tree can be expanded to display dependency conflicts. For example, to find out why Commons Collections 2.0 is being used by the Maven Dependency Plugin, we can execute the following in the project's directory:

```
mvn dependency:tree -Dverbose -Dincludes=commons-collections
```

The `verbose` flag instructs the dependency tree to display conflicting dependencies that were omitted from the resolved dependency tree. In this case, the goal outputs:

```
[INFO] [dependency:tree]
[INFO] org.apache.maven.plugins:maven-dependency-plugin:maven-plugin:2.0-alpha-5-SNAPSHOT
[INFO] +- org.apache.maven.reporting:maven-reporting-impl:jar:2.0.4:compile
[INFO] |  \- commons-validator:commons-validator:jar:1.2.0:compile
[INFO] |     \- commons-digester:commons-digester:jar:1.6:compile
[INFO] |        \- (commons-collections:commons-collections:jar:2.1:compile - omitted for conflict with 2.0)
[INFO] \- org.apache.maven.doxia:doxia-site-renderer:jar:1.0-alpha-8:compile
[INFO]    \- org.codehaus.plexus:plexus-velocity:jar:1.1.3:compile
[INFO]       \- commons-collections:commons-collections:jar:2.0:compile
```

Thus we can see that Commons Collections 2.0 was chosen over 2.1 since it is nearer, and by default Maven resolves version conflicts with a nearest-wins strategy.

More specifically, in `verbose` mode the dependency tree shows dependencies that were omitted for: being a duplicate of another; conflicting with another's version and/or scope; and introducing a cycle into the dependency tree.

#### Verbose flag support in 3.0.0+ version

Support for `verbose` flag has been removed since ![Maven Dependency Plugin 3.0.0 and higher](https://img.shields.io/badge/Maven_Dependency_Plugin-3.0.0_%2B-D5AF1B "Maven Dependency Plugin 3.0.0 and higher").

It was reintroduce since ![Maven Dependency Plugin 3.1.3 and higher](https://img.shields.io/badge/Maven_Dependency_Plugin-3.1.3_%2B-D5AF1B "Maven Dependency Plugin 3.1.3 and higher")
(see also [![issue MDEP-644](https://img.shields.io/jira/issue/https/issues.apache.org/jira/MDEP-644.svg "issue MDEP-644")](https://issues.apache.org/jira/browse/MDEP-644)) but is sometimes very slow:

<img height="435px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/maven-dependency-plugin-3.2.0-tree-goal.png"
                    alt="Reactor Dependency Convergence error" title="Reactor Dependency Convergence error"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/maven-dependency-plugin-3.2.0-tree-goal.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

Extra output can be obtained by using an older version until proper support is reintroduced:

```
mvn org.apache.maven.plugins:maven-dependency-plugin:2.10:tree -Dverbose=true -Dincludes=org.ow2.asm,net.bytebuddy
```

<img height="435px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/maven-dependency-plugin-2.10-tree-goal.png"
                    alt="Reactor Dependency Convergence error" title="Reactor Dependency Convergence error"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/maven-dependency-plugin-2.10-tree-goal.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

**Note**: Using Maven 2 dependency tree to get verbose output may be inconsistent with actual Maven 3 resolution.

### Reactor Dependency Convergence

Strange error in `Reactor Dependency Convergence` with `Convergence (NOD/NOA): 99%` but no dependencies listed:

<img height="435px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/reactor-dependency-convergence-error.png"
                    alt="Reactor Dependency Convergence error" title="Reactor Dependency Convergence error"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/reactor-dependency-convergence-error.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

[Resolving conflicts](https://maven.apache.org/plugins/maven-dependency-plugin/examples/resolving-conflicts-using-the-dependency-tree.html) using the dependency tree didn't work.

In the end removing and adding blocks of dependencies narrowed the problem down to `mockito-core`. Drilling down to its transitive dependencies the dependency `asm` showed up in `byte-buddy`.
Although this artifact is a build of Byte Buddy with all ASM dependencies repackaged into its own name space it seemed to clash with `cglib` and `wiremock-jre8`.

Testing with an extra dependency `byte-buddy-dep` resulted in:

<img height="693px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/reactor-dependency-convergence-asm.png"
                    alt="Reactor Dependency Convergence asm" title="Reactor Dependency Convergence asm"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/reactor-dependency-convergence-asm.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

But disabling `cglib` and `wiremock-jre8` *didn't* resolve the issue.

Only explicit definition of the dependency `byte-buddy` in `dependencyManagement` fixed the problem and that one extra unique artifact (NOA) was gone:

<img height="322px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/reactor-dependency-convergence-success.png"
                    alt="Reactor Dependency Convergence success" title="Reactor Dependency Convergence success"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/reactor-dependency-convergence-success.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

## Custom Dependency Check

To use the [![Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Dependency Check") Custom Dependency Check](https://gitlab.com/freedumbytes/dependency-check)
`suppressionFiles` and `hintsFile` containing the rules that control how to suppress false positives and resolve false negatives:

```xml
  <properties>
    <ossrhHost>https://oss.sonatype.org</ossrhHost>
    <nexusHost>${ossrhHost}</nexusHost>
    <nexusRepositoriesUri>/index.html#view-repositories</nexusRepositoriesUri>
    <nexusMavenGroupUri>/content/groups/public</nexusMavenGroupUri>
    <nexusMavenReleasesUri>/content/repositories/releases</nexusMavenReleasesUri>
    <nexusMavenSnapshotsUri>/content/repositories/snapshots</nexusMavenSnapshotsUri>

    <owaspReportsPath>${project.build.directory}/owasp-reports</owaspReportsPath>
    <owaspReportFormat>all</owaspReportFormat>
    <cveValidForHours>24</cveValidForHours>
    <skipDependencyManagement>true</skipDependencyManagement>

    <customSuppressionHintsVersion>x.y.z</customSuppressionHintsVersion>
    <customSuppressionHintsFolderVersion>${customSuppressionHintsVersion}</customSuppressionHintsFolderVersion>
    <customSuppressionHintsPath>
      ${nexusHost}${nexusMavenGroupUri}/nl/demon/shadowland/freedumbytes/maven/owasp/dependency-check/${customSuppressionHintsFolderVersion}
    </customSuppressionHintsPath>
    <customSuppressionFile>
      ${customSuppressionHintsPath}/dependency-check-${customSuppressionHintsVersion}-suppression.xml
    </customSuppressionFile>
    <customHintsFile>
      ${customSuppressionHintsPath}/dependency-check-${customSuppressionHintsVersion}-hints.xml
    </customHintsFile>
  </properties>

  <profiles>
    <profile>
      <id>enableDependencyCheckReport</id>

      <reporting>
        <plugins>
          <plugin>
            <groupId>org.owasp</groupId>
            <artifactId>dependency-check-maven</artifactId>
            <reportSets>
              <reportSet>
                <reports>
                  <report>aggregate</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.owasp</groupId>
          <artifactId>dependency-check-maven</artifactId>
          <version>${mavenDependencyCheckVersion}</version>
          <configuration>
            <name>Dependency Check Report</name>
            <outputDirectory>${owaspReportsPath}</outputDirectory>
            <format>${owaspReportFormat}</format>
            <cveValidForHours>${cveValidForHours}</cveValidForHours>
            <skipDependencyManagement>${skipDependencyManagement}</skipDependencyManagement>

            <suppressionFiles>
              <suppressionFile>${customSuppressionFile}</suppressionFile>
            </suppressionFiles>
            <hintsFile>${customHintsFile}</hintsFile>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

### Sample Report Showing Vulnerable Dependencies

<img height="831px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-default.png"
                    alt="Dependency Check Sample Default" title="Dependency Check Sample Default"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-default.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

 **Note**: As of [![OWASP Dependency Check Plugin 3.1.0 and higher](https://img.shields.io/badge/OWASP_Dependency_Check_Plugin-3.1.0_%2B-D5AF1B
                    "OWASP Dependency Check Plugin 3.1.0 and higher")](https://github.com/jeremylong/DependencyCheck/compare/v3.0.2...v3.1.0)
 (see also [![pull request 1028](https://img.shields.io/github/issues/detail/state/jeremylong/DependencyCheck/1028.svg "pull request 1028")](https://github.com/jeremylong/DependencyCheck/pull/1028) False Positive Reduction)
 these custom suppression and hints files are obsolete unless the `Vulnerabilities` should show up as `Suppressed`.

<img height="812px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom.png"
                    alt="Dependency Check Sample Custom" title="Dependency Check Sample Custom"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

### Sample Report Showing All Dependencies

<img height="951px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom-all.png"
                    alt="Dependency Check Sample Custom All" title="Dependency Check Sample Custom All"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom-all.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

### Sample Report Showing Custom Suppressed Vulnerabilities

<img height="83px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-collapsed.png"
                    alt="Dependency Check Sample Custom Suppressed Collapsed" title="Dependency Check Sample Custom Suppressed Collapsed"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-collapsed.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

<img height="443px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-expanded.png"
                    alt="Dependency Check Sample Custom Suppressed Expanded" title="Dependency Check Sample Custom Suppressed Expanded"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-expanded.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

## Open Source Patches

A complete list of the available open sources patches:

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "GitHub") Extra Enforcer Rules](https://github.com/mojohaus/extra-enforcer-rules):

 * [![issue 35](https://img.shields.io/github/issues/detail/state/mojohaus/extra-enforcer-rules/35.svg "issue 35")](https://github.com/mojohaus/extra-enforcer-rules/issues/35) Fail to detect dependency with java 9 `module-info.class` only
   (see also [![pull request 36](https://img.shields.io/github/issues/detail/state/mojohaus/extra-enforcer-rules/36.svg "pull request 36")](https://github.com/mojohaus/extra-enforcer-rules/pull/36))

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/mojohaus.png "GitHub") MojoHaus Build Helper Maven Plugin](https://github.com/mojohaus/build-helper-maven-plugin):

 * [![issue 61](https://img.shields.io/github/issues/detail/state/mojohaus/build-helper-maven-plugin/61.svg "issue 61")](https://github.com/mojohaus/build-helper-maven-plugin/issues/61) Set a property based on the `maven.build.timestamp`
   (see also [![pull request 32](https://img.shields.io/github/issues/detail/state/mojohaus/build-helper-maven-plugin/32.svg "pull request 32")](https://github.com/mojohaus/build-helper-maven-plugin/pull/32))

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "GitHub") Apache Maven Javadoc Plugin](https://github.com/apache/maven-javadoc-plugin):

 * [![issue MJAVADOC-512](https://img.shields.io/jira/issue/https/issues.apache.org/jira/MJAVADOC-512.svg "issue MJAVADOC-512")](https://issues.apache.org/jira/browse/MJAVADOC-512) `WARNING` even when `<javadocVersion>` matches `1.8.0`
   (see also [![pull request 1](https://img.shields.io/github/issues/detail/state/apache/maven-javadoc-plugin/1.svg "pull request 1")](https://github.com/apache/maven-javadoc-plugin/pull/1))

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "GitHub") OWASP Dependency Check Plugin](https://github.com/jeremylong/DependencyCheck):

 * Fix groovy script failure because of unescaped Windows `File.separator`
   (see also [![pull request 1101](https://img.shields.io/github/issues/detail/state/jeremylong/DependencyCheck/1101.svg "pull request 1101")](https://github.com/jeremylong/DependencyCheck/pull/1101))
 * [![issue 1102](https://img.shields.io/github/issues/detail/state/jeremylong/DependencyCheck/1102.svg "issue 1102")](https://github.com/jeremylong/DependencyCheck/issues/1102) Time Saver H2 database creation
   (see also [![pull request 1143](https://img.shields.io/github/issues/detail/state/jeremylong/DependencyCheck/1143.svg "pull request 1143")](https://github.com/jeremylong/DependencyCheck/pull/1143))

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/qualinsight.png "GitHub") SVG Badges plugin for SonarQube](https://github.com/QualInsight/qualinsight-plugins-sonarqube-badges):

 * [![issue 75](https://img.shields.io/github/issues/detail/state/QualInsight/qualinsight-plugins-sonarqube-badges/75.svg "issue 75")](https://github.com/QualInsight/qualinsight-plugins-sonarqube-badges/issues/75)
   Change measures badge neutral color to SonarCloud blue
   (see also [![pull request 78](https://img.shields.io/github/issues/detail/state/QualInsight/qualinsight-plugins-sonarqube-badges/78.svg "pull request 78")](https://github.com/QualInsight/qualinsight-plugins-sonarqube-badges/pull/78))

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/sonarqube.png "GitHub") Sonar Maven Report Plugin](https://github.com/SonarQubeCommunity/sonar-maven-report):

[![Sonar Maven Report Plugin 0.2.2 and higher](https://img.shields.io/badge/Sonar_Maven_Report_Plugin-0.2.2_%2B-D5AF1B "Sonar Maven Report Plugin 0.2.2 and higher")](https://freedumbytes.gitlab.io/sonar-maven-report):

 * [![issue 6](https://img.shields.io/github/issues/detail/state/SonarQubeCommunity/sonar-maven-report/6.svg "issue 6")](https://github.com/SonarQubeCommunity/sonar-maven-report/issues/6) Link to SonarQube is no longer working
   (see also [![pull request 5](https://img.shields.io/github/issues/detail/state/SonarQubeCommunity/sonar-maven-report/5.svg "pull request 5")](https://github.com/SonarQubeCommunity/sonar-maven-report/pull/5))
 * [![issue 4](https://img.shields.io/github/issues/detail/state/SonarQubeCommunity/sonar-maven-report/4.svg "issue 4")](https://github.com/SonarQubeCommunity/sonar-maven-report/issues/4)
   Since Sonar was renamed to SonarQube update the menu link and report header (see also
   [![pull request 3](https://img.shields.io/github/issues/detail/state/SonarQubeCommunity/sonar-maven-report/3.svg "pull request 3")](https://github.com/SonarQubeCommunity/sonar-maven-report/pull/3))
 * [![issue 7](https://img.shields.io/github/issues/detail/state/SonarQubeCommunity/sonar-maven-report/7.svg "issue 7")](https://github.com/SonarQubeCommunity/sonar-maven-report/issues/7)
   When the project url points to `://sonarcloud.io/` use SonarCloud for menu and report
   (see also [![pull request 8](https://img.shields.io/github/issues/detail/state/SonarQubeCommunity/sonar-maven-report/8.svg "pull request 8")](https://github.com/SonarQubeCommunity/sonar-maven-report/pull/8))
 * [![issue 9](https://img.shields.io/github/issues/detail/state/SonarQubeCommunity/sonar-maven-report/9.svg "issue 9")](https://github.com/SonarQubeCommunity/sonar-maven-report/issues/9) Place released version `0.2.x` into Maven Repository

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") Reflow Maven Skin](https://github.com/andriusvelykis/reflow-maven-skin):

 * [![issue 65](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/65.svg "issue 65")](https://github.com/andriusvelykis/reflow-maven-skin/issues/65) Fix Html5 shiv URL HTTP `404`
   (see also [![pull request 62](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/62.svg "pull request 62")](https://github.com/andriusvelykis/reflow-maven-skin/pull/62))
 * [![issue 66](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/66.svg "issue 66")](https://github.com/andriusvelykis/reflow-maven-skin/issues/66)
   Fix `Back to top` link which isn't working because of smooth scrolling for anchor links
   (see also [![pull request 61](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/61.svg "pull request 61")](https://github.com/andriusvelykis/reflow-maven-skin/pull/61))
 * [![issue 67](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/67.svg "issue 67")](https://github.com/andriusvelykis/reflow-maven-skin/issues/67)
   Fix `nav` menu item double highlighting when last submenu item is selected
   (see also [![pull request 60](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/60.svg "pull request 60")](https://github.com/andriusvelykis/reflow-maven-skin/pull/60))
 * [![issue 68](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/68.svg "issue 68")](https://github.com/andriusvelykis/reflow-maven-skin/issues/68)
   Fix `Versions Plugin Updates Report` layout by disabling `fixTableHeads`
   (see also [![pull request 59](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/59.svg "pull request 59")](https://github.com/andriusvelykis/reflow-maven-skin/pull/59))
 * [![Maven Site Plugin 3.5 and higher](https://img.shields.io/badge/Maven_Site_Plugin-3.5_%2B-D5AF1B "Maven Site Plugin 3.5 and higher")](https://maven.apache.org/plugins/maven-site-plugin)
   * [![issue 64](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/64.svg "issue 64")](https://github.com/andriusvelykis/reflow-maven-skin/issues/64) Patch `reflow-velocity-tools`
     (see also [![pull request 63](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/63.svg "pull request 63")](https://github.com/andriusvelykis/reflow-maven-skin/pull/63))
     * See also [![issue MSITE-782](https://img.shields.io/jira/issue/https/issues.apache.org/jira/MSITE-782.svg "issue MSITE-782")](https://issues.apache.org/jira/browse/MSITE-782)
       about `classpath:/META-INF/maven/site-tools.xml`
   * [![issue 69](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/69.svg "issue 69")](https://github.com/andriusvelykis/reflow-maven-skin/issues/69)
     Fix `site.xml` `<head>` and `<footer>` after upgrade to `decoration-1.7.0.xsd`
     (see also [![pull request 58](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/58.svg "pull request 58")](https://github.com/andriusvelykis/reflow-maven-skin/pull/58))
 * [![issue 70](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/70.svg "issue 70")](https://github.com/andriusvelykis/reflow-maven-skin/issues/70)
   Hosting Maven site on `https` causes `Blocked loading mixed active content`
   (see also [![pull request 71](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/71.svg "pull request 71")](https://github.com/andriusvelykis/reflow-maven-skin/pull/71))

> Aha, that is were _open_ stands for in **open source**. :grinning:
