 -----
 Index
 -----
 Jene Jasper
 ------
 2010-02-23
 ------

~~ NOTE: For help with the syntax of this file, see:
~~ https://maven.apache.org/doxia/references/apt-format.html

${organizationName} Maven Setup

 Setup the site, license and plugin configuration (such as the required version of the basic plugins and the generated site reports).

* Project Information

 This POM defines the following <default> information:

 * license.

 * organization.

 * developers.

 * contributors.

 * distributionManagement of the releases and snapshots.

 []

 Projects that extend from this POM <must at least override> the following settings:

 * name.

 * description.

 * url.

 * inceptionYear.

 * scm.

 * ciManagement.

 * issueManagement.

 * distributionManagement of the site.

 []

 <<Note>>: With the profile <<<openSource>>> it is possible to switch the location information on SCM, CI, Issue, Maven Documentation and Artifacts between LAN and Open Source tools.
 That is one of the reasons why mavenHost, projectFolderName, projectRoot, organizationHost, nexusHost, sonarHost, sourceConnection, sourceDevConnection, sourceWebRoot, ciSystem, ciWebRoot, issueSystem and issueWebRoot properties were introduced.
 The other reason can be found at the following {{{${gitlabHost}/dpl/html/complete-manual.html#prj.maven.env}Tip}} about changed hosts.

* Examples of Setup usage

** Java Development Project

+--
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
    <artifactId>java</artifactId>
    <version>${project.version}</version>
  </parent>

  <groupId>nl.demon.shadowland.freedumbytes.swingx</groupId>
  <artifactId>jmodalwindow</artifactId>
  <version>2.4.2-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>JModalWindow</name>
  <description>The JModalWindow project was created to support modal functionality, similar to JDialog, without blocking all frames.</description>
  <url>\${projectRoot}</url>
  <inceptionYear>2004</inceptionYear>

  <licenses>
    <license>
      <name>GNU Lesser General Public License</name>
      <url>https://www.gnu.org/licenses/lgpl-3.0.html</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <scm>
    <connection>\${sourceConnection}</connection>
    <developerConnection>\${sourceDevConnection}</developerConnection>
    <url>\${sourceWebRoot}</url>
    <tag />
  </scm>

  <ciManagement>
    <system>\${ciSystem}</system>
    <url>\${ciWebRoot}</url>
  </ciManagement>

  <issueManagement>
    <system>\${issueSystem}</system>
    <url>\${issueWebRoot}</url>
  </issueManagement>

  <distributionManagement>
    <site>
      <id>mvn-sites</id>
      <name>Maven Documentation Sites</name>
      <url>\${distributionManagementSiteUrlPrefix}\${projectRoot}</url>
    </site>
  </distributionManagement>

  <properties>
    <projectFolderName>jmodalwindow</projectFolderName>
    <projectRoot>\${mavenHost}/\${projectFolderName}</projectRoot>

    <sourceConnection>scm:git:\${gitRepo}/\${projectFolderName}.git</sourceConnection>
    <sourceDevConnection>scm:git:\${gitRepo}/\${projectFolderName}.git</sourceDevConnection>
    <sourceWebRoot>\${fisheyeHost}/browse/\${projectFolderName}</sourceWebRoot>

    <ciSystem>Jenkins</ciSystem>
    <ciWebRoot>\${jenkinsHost}/job/\${projectFolderName}</ciWebRoot>

    <issueSystem>JIRA</issueSystem>
    <issueWebRoot>\${jiraHost}/projects/JMW</issueWebRoot>

    <processDependencyManagement>false</processDependencyManagement>

    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    <maven.compiler.source>${maven.compiler.target}</maven.compiler.source>
    <maven.compiler.target>${maven.compiler.compilerVersion}</maven.compiler.target>
    <maven.compiler.testSource>${maven.compiler.testTarget}</maven.compiler.testSource>
    <maven.compiler.testTarget>${maven.compiler.target}</maven.compiler.testTarget>
    <!--
      Requires Java9+, otherwise: 'mvn compile' results in "javac: invalid flag: --release".
      <maven.compiler.release>8</maven.compiler.release>
    -->

    <mojoSignatureArtifactId>java18</mojoSignatureArtifactId>
    <ignoreDependencies>true</ignoreDependencies>

    <extraEnforcerRulesMaxJdkVersion>${maven.compiler.target}</extraEnforcerRulesMaxJdkVersion>
    <enforcerRulesFailEnforceBytecodeVersion>\${enforcerRulesFail}</enforcerRulesFailEnforceBytecodeVersion>

    <javadocVersion>1.8.0</javadocVersion>
    <javadocDoclint>none</javadocDoclint>

    <aggregateSurefireReports>true</aggregateSurefireReports>
    <surefire.useSystemClassLoader>true</surefire.useSystemClassLoader>
    <failsafe.useSystemClassLoader>true</failsafe.useSystemClassLoader>
  </properties>
</project>
+--

 <<Note>>: Single module projects should use <<<\<aggregateSurefireReports\>false\</aggregateSurefireReports\>>>>,
 otherwise the surefire report will show number of tests is 0 and the failsafe report will be missing altogether.

** Java Open Source Project

+--
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
    <artifactId>java</artifactId>
    <version>${project.version}</version>
  </parent>

  <groupId>nl.demon.shadowland.freedumbytes.swingx</groupId>
  <artifactId>jmodalwindow</artifactId>
  <version>2.4.2-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>JModalWindow</name>
  <description>The JModalWindow project was created to support modal functionality, similar to JDialog, without blocking all frames.</description>
  <url>\${projectRoot}</url>
  <inceptionYear>2004</inceptionYear>

  <licenses>
    <license>
      <name>Apache License, Version 2.0</name>
      <url>https://www.apache.org/licenses/LICENSE-2.0.html</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <scm>
    <connection>\${sourceConnection}</connection>
    <developerConnection>\${sourceDevConnection}</developerConnection>
    <url>\${sourceWebRoot}</url>
    <tag />
  </scm>

  <ciManagement>
    <system>\${ciSystem}</system>
    <url>\${ciWebRoot}</url>
  </ciManagement>

  <issueManagement>
    <system>\${issueSystem}</system>
    <url>\${issueWebRoot}</url>
  </issueManagement>

  <distributionManagement>
    <site>
      <id>mvn-sites</id>
      <name>Maven Documentation Sites</name>
      <url>\${distributionManagementSiteUrlPrefix}\${projectRoot}</url>
    </site>
  </distributionManagement>

  <properties>
    <projectFolderName>jmodalwindow</projectFolderName>
    <projectRoot>\${mavenHost}/\${projectFolderName}</projectRoot>

    <sourceConnection>scm:git:\${gitlabRepo}/\${projectFolderName}.git</sourceConnection>
    <sourceDevConnection>scm:git:\${gitlabRepoSSH}/\${projectFolderName}.git</sourceDevConnection>
    <sourceWebRoot>\${gitlabRepo}/\${projectFolderName}/tree/master</sourceWebRoot>

    <ciSystem>GitLab CI</ciSystem>
    <ciWebRoot>\${gitlabRepo}/\${projectFolderName}/pipelines</ciWebRoot>

    <issueSystem>GitLab</issueSystem>
    <issueWebRoot>\${gitlabRepo}/\${projectFolderName}/issues</issueWebRoot>

    <processDependencyManagement>false</processDependencyManagement>

    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    <maven.compiler.source>${maven.compiler.target}</maven.compiler.source>
    <maven.compiler.target>${maven.compiler.compilerVersion}</maven.compiler.target>
    <maven.compiler.testSource>${maven.compiler.testTarget}</maven.compiler.testSource>
    <maven.compiler.testTarget>${maven.compiler.target}</maven.compiler.testTarget>
    <!--
      Requires Java9+, otherwise: 'mvn compile' results in "javac: invalid flag: --release".
      <maven.compiler.release>8</maven.compiler.release>
    -->

    <mojoSignatureArtifactId>java18</mojoSignatureArtifactId>
    <ignoreDependencies>true</ignoreDependencies>

    <extraEnforcerRulesMaxJdkVersion>${maven.compiler.target}</extraEnforcerRulesMaxJdkVersion>
    <enforcerRulesFailEnforceBytecodeVersion>\${enforcerRulesFail}</enforcerRulesFailEnforceBytecodeVersion>

    <javadocVersion>1.8.0</javadocVersion>
    <javadocDoclint>none</javadocDoclint>

    <aggregateSurefireReports>true</aggregateSurefireReports>
    <surefire.useSystemClassLoader>true</surefire.useSystemClassLoader>
    <failsafe.useSystemClassLoader>true</failsafe.useSystemClassLoader>
  </properties>
</project>
+--

** Java Project combined with Open Source profile

+--
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
    <artifactId>java</artifactId>
    <version>${project.version}</version>
  </parent>

  <groupId>nl.demon.shadowland.freedumbytes.swingx</groupId>
  <artifactId>jmodalwindow</artifactId>
  <version>2.4.2-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>JModalWindow</name>
  <description>The JModalWindow project was created to support modal functionality, similar to JDialog, without blocking all frames.</description>
  <url>\${projectRoot}</url>
  <inceptionYear>2004</inceptionYear>

  <licenses>
    <license>
      <name>Apache License, Version 2.0</name>
      <url>https://www.apache.org/licenses/LICENSE-2.0.html</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <scm>
    <connection>\${sourceConnection}</connection>
    <developerConnection>\${sourceDevConnection}</developerConnection>
    <url>\${sourceWebRoot}</url>
    <tag />
  </scm>

  <ciManagement>
    <system>\${ciSystem}</system>
    <url>\${ciWebRoot}</url>
  </ciManagement>

  <issueManagement>
    <system>\${issueSystem}</system>
    <url>\${issueWebRoot}</url>
  </issueManagement>

  <distributionManagement>
    <site>
      <id>mvn-sites</id>
      <name>Maven Documentation Sites</name>
      <url>\${distributionManagementSiteUrlPrefix}\${projectRoot}</url>
    </site>
  </distributionManagement>

  <properties>
    <projectFolderName>jmodalwindow</projectFolderName>
    <projectRoot>\${mavenHost}/\${projectFolderName}</projectRoot>

    <sourceConnection>scm:git:\${gitRepo}/\${projectFolderName}.git</sourceConnection>
    <sourceDevConnection>scm:git:\${gitRepo}/\${projectFolderName}.git</sourceDevConnection>
    <sourceWebRoot>\${fisheyeHost}/browse/\${projectFolderName}</sourceWebRoot>

    <ciSystem>Jenkins</ciSystem>
    <ciWebRoot>\${jenkinsHost}/job/\${projectFolderName}</ciWebRoot>

    <issueSystem>JIRA</issueSystem>
    <issueWebRoot>\${jiraHost}/projects/JMW</issueWebRoot>

    <processDependencyManagement>false</processDependencyManagement>

    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    <maven.compiler.source>${maven.compiler.target}</maven.compiler.source>
    <maven.compiler.target>${maven.compiler.compilerVersion}</maven.compiler.target>
    <maven.compiler.testSource>${maven.compiler.testTarget}</maven.compiler.testSource>
    <maven.compiler.testTarget>${maven.compiler.target}</maven.compiler.testTarget>
    <!--
      Requires Java9+, otherwise: 'mvn compile' results in "javac: invalid flag: --release".
      <maven.compiler.release>8</maven.compiler.release>
    -->

    <mojoSignatureArtifactId>java18</mojoSignatureArtifactId>
    <ignoreDependencies>true</ignoreDependencies>

    <extraEnforcerRulesMaxJdkVersion>${maven.compiler.target}</extraEnforcerRulesMaxJdkVersion>
    <enforcerRulesFailEnforceBytecodeVersion>\${enforcerRulesFail}</enforcerRulesFailEnforceBytecodeVersion>

    <javadocVersion>1.8.0</javadocVersion>
    <javadocDoclint>none</javadocDoclint>

    <aggregateSurefireReports>true</aggregateSurefireReports>
    <surefire.useSystemClassLoader>true</surefire.useSystemClassLoader>
    <failsafe.useSystemClassLoader>true</failsafe.useSystemClassLoader>
  </properties>

  <profiles>
    <profile>
      <id>openSource</id>

      <properties>
        <projectRoot>\${gitlabHost}/\${projectFolderName}</projectRoot>

        <sourceConnection>scm:git:\${gitlabRepo}/\${projectFolderName}.git</sourceConnection>
        <sourceDevConnection>scm:git:\${gitlabRepoSSH}/\${projectFolderName}.git</sourceDevConnection>
        <sourceWebRoot>\${gitlabRepo}/\${projectFolderName}/tree/master</sourceWebRoot>

        <ciSystem>GitLab CI</ciSystem>
        <ciWebRoot>\${gitlabRepo}/\${projectFolderName}/pipelines</ciWebRoot>

        <issueSystem>GitLab</issueSystem>
        <issueWebRoot>\${gitlabRepo}/\${projectFolderName}/issues</issueWebRoot>
      </properties>
    </profile>

    <profile>
      <id>enableGitLabRepo</id>

      <activation>
        <property>
          <name>env.CI_PROJECT_ID</name>
        </property>
      </activation>

      <repositories>
        <repository>
          <id>gitlab-maven</id>
          <name>GitLab Group Maven Packages</name>
          <url>https://gitlab.com/api/v4/groups/freedumbytes/-/packages/maven</url>
        </repository>
      </repositories>

      <properties>
        <projectRoot>\${gitlabHost}/\${projectFolderName}</projectRoot>

        <sourceConnection>scm:git:\${gitlabRepo}/\${projectFolderName}.git</sourceConnection>
        <sourceDevConnection>scm:git:\${gitlabRepoSSH}/\${projectFolderName}.git</sourceDevConnection>
        <sourceWebRoot>\${gitlabRepo}/\${projectFolderName}/tree/master</sourceWebRoot>

        <ciSystem>GitLab CI</ciSystem>
        <ciWebRoot>\${gitlabRepo}/\${projectFolderName}/pipelines</ciWebRoot>

        <issueSystem>GitLab</issueSystem>
        <issueWebRoot>\${gitlabRepo}/\${projectFolderName}/issues</issueWebRoot>
      </properties>
    </profile>
  </profiles>
</project>
+--

 To create the site with the default LAN tools FishEye, JIRA and Jenkins run <<<mvn site-deploy>>>.

 To create the site with the Open Source tools {{{${gitlabHost}/index.xhtml}GitLab.io}},
 {{{${ossrhHost}/#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.config~~~~}OSS.Sonatype.org}} (OSS Repository Hosting)
 and {{{${sonarCloudRepo}/projects?sort=-analysis_date}SonarCloud.io}} (Continuous Code Quality) run <<<mvn site -PopenSource>>>.

 <<Note>>: Latest releases are available at {{{${gitlabRepo}/../groups/freedumbytes/-/packages/?type=Maven&orderBy=created_at&sort=desc}GitLab.com}} (Package Registry).

 As an alternative host Maven artifacts in GitLab Maven Package Registry itself with <<<mvn site -PenableGitLabRepo>>>.

 <<Note>>: Followed by <<<mvn site:deploy>>> (without openSource profile option) to preview the site in the LAN hosted Maven sites because GitLab variant uses {{{https://docs.gitlab.com/ce/user/project/pages/index.html}Pages}} to deploy a site.
