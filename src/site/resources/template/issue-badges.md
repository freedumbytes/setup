# Open Source Patches

 * ggg = Project Group
 * ppp = Project Name
 * yyy = Issue Number
 * zzz = Pulll/MergeRequest Number
 * ddd = Issue Description
 * lll = Project Description
 * hhh = Commit Hash
 * kkk = JIRA Key

```md
[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") lll](https://github.com/ggg/ppp):

[![issue yyy](https://img.shields.io/github/issues/detail/state/ggg/ppp/yyy.svg "issue yyy")](https://github.com/ggg/ppp/issues/yyy) ddd
(see also [![pull request zzz](https://img.shields.io/github/issues/detail/state/ggg/ppp/zzz.svg "pull request zzz")](https://github.com/ggg/ppp/pull/zzz))

(see also [![issue yyy](https://img.shields.io/github/issues/detail/state/ggg/ppp/yyy.svg "issue yyy")](https://github.com/ggg/ppp/issues/yyy))
(see also [![pull request zzz](https://img.shields.io/github/issues/detail/state/ggg/ppp/zzz.svg "pull request zzz")](https://github.com/ggg/ppp/pull/zzz))
(see also [![commit hhh](https://img.shields.io/badge/commit-hhh-green "commit hhh")](https://gitlab.com/freedumbytes/setup/-/commit/hhh))
(see also [![issue kkk-999](https://img.shields.io/jira/issue/https/issues.apache.org/jira/kkk-999.svg "issue kkk-999")](https://issues.apache.org/jira/browse/kkk-999))
```

## Demo

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") Reflow Maven Skin](https://github.com/andriusvelykis/reflow-maven-skin):

 * [![issue 70](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/70.svg "issue 70")](https://github.com/andriusvelykis/reflow-maven-skin/issues/70)
   Hosting Maven site on `https` causes `Blocked loading mixed active content`
   (see also [![pull request 71](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/71.svg "pull request 71")](https://github.com/andriusvelykis/reflow-maven-skin/pull/71))

Types:

 1. Issue (see also [![issue 70](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/70.svg "issue 70")](https://github.com/andriusvelykis/reflow-maven-skin/issues/70))
 2. Pull Request (see also [![pull request 71](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/71.svg "pull request 71")](https://github.com/andriusvelykis/reflow-maven-skin/pull/71))
 3. Commit (see also [![commit 00b54368](https://img.shields.io/badge/commit-00b54368-green "commit 00b54368")](https://gitlab.com/freedumbytes/setup/-/commit/00b54368))
 4. JIRA (see also [![issue MSITE-782](https://img.shields.io/jira/issue/https/issues.apache.org/jira/MSITE-782.svg "issue MSITE-782")](https://issues.apache.org/jira/browse/MSITE-782)
