# Layout Shift Images

 * iii = Image
 * hhh = Height
 * ddd = Description

```xml
<img height="hhhpx" src="iii"
                    alt="ddd" title="ddd"
     data-canonical-src="iii" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">
```

# Demo

<img height="831px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-default.png"
                    alt="Dependency Check Sample Default" title="Dependency Check Sample Default"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-default.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy"></a>

<img height="812px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom.png"
                    alt="Dependency Check Sample Default" title="Dependency Check Sample Default"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="lazy">

<img height="951px" src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom-all.png"
                    alt="Dependency Check Sample Default" title="Dependency Check Sample Default"
     data-canonical-src="https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom-all.png" class="js-lazy-loaded qa-js-lazy-loaded" loading="eager">

# End of Demo
