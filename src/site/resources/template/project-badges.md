# Additional Maven Documentation / Javadoc / Releases / Quality Assurance

# LLL

 * GGG = (35) Project GroupId (nl.demon.shadowland.freedumbytes.manual)
 * TTT = (32) Project ArtifactId (development-production-line)
 * PPP = (17) Project Name (development-production-line)
 * LLL = (90) Project Description (Production Development Line)
 * UUU =  (3) Project Description underscore (Production_Development_Line)

```md
[![LLL pipeline](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "LLL pipeline")
 ![LLL pipeline](https://gitlab.com/freedumbytes/PPP/badges/master/pipeline.svg "LLL pipeline")](https://gitlab.com/freedumbytes/PPP)

[![LLL License](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "LLL License")
 ![LLL License](https://img.shields.io/badge/Apache_License-2.0-orange.svg "LLL License")](https://www.apache.org/licenses/LICENSE-2.0.html)

[![LLL Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "LLL Maven Site")
 ![LLL Maven Site](https://img.shields.io/badge/UUU-latest_release-802C59 "LLL Maven Site")](https://freedumbytes.gitlab.io/PPP)
[![LLL Maven Site](https://img.shields.io/badge/UUU-current_snapshot-802C59 "LLL Maven Site")](https://freedumbytes.gitlab.io/PPP/snapshot)

 * [![LLL Reactor Dependency Convergence](https://img.shields.io/badge/Reactor_Dependency_Convergence-latest_release-802C59
      "LLL Reactor Dependency Convergence")](https://freedumbytes.gitlab.io/PPP/dependency-convergence.html)
   [![LLL Reactor Dependency Convergence](https://img.shields.io/badge/Reactor_Dependency_Convergence-current_snapshot-802C59
      "LLL Reactor Dependency Convergence")](https://freedumbytes.gitlab.io/PPP/snapshot/dependency-convergence.html)
 * [![LLL Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-latest_release-802C59
      "LLL Plugin Updates Report")](https://freedumbytes.gitlab.io/PPP/plugin-updates-report.html)
   [![LLL Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-current_snapshot-802C59
      "LLL Plugin Updates Report")](https://freedumbytes.gitlab.io/PPP/snapshot/plugin-updates-report.html)
 * [![LLL Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-latest_release-802C59
      "LLL Property Updates Report")](https://freedumbytes.gitlab.io/PPP/property-updates-report.html)
   [![LLL Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-current_snapshot-802C59
      "LLL Property Updates Report")](https://freedumbytes.gitlab.io/PPP/snapshot/property-updates-report.html)
 * [![LLL Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-latest_release-802C59
      "LLL Dependency Updates Report")](https://freedumbytes.gitlab.io/PPP/dependency-updates-report.html)
   [![LLL Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-current_snapshot-802C59
      "LLL Dependency Updates Report")](https://freedumbytes.gitlab.io/PPP/snapshot/dependency-updates-report.html)

[![LLL Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "LLL Javadoc")
 ![LLL Javadoc](https://img.shields.io/badge/Javadoc-latest_release-4D7A97 "LLL Javadoc")](https://freedumbytes.gitlab.io/PPP/apidocs)
[![LLL Javadoc](https://img.shields.io/badge/Javadoc-current_snapshot-4D7A97 "LLL Javadoc")](https://freedumbytes.gitlab.io/PPP/snapshot/apidocs)

[![LLL Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "LLL Dependency Check")
 ![LLL Dependency Check](https://img.shields.io/badge/Dependency_Check-latest_release-F78D0A "LLL Dependency Check")](https://freedumbytes.gitlab.io/PPP/dependency-check-report.html)
[![LLL Dependency Check](https://img.shields.io/badge/Dependency_Check-current_snapshot-F78D0A "LLL Dependency Check")](https://freedumbytes.gitlab.io/PPP/snapshot/dependency-check-report.html)

[![LLL Quality Gate Status](https://sonarcloud.io/api/project_badges/quality_gate?project=GGG%3ATTT "LLL Quality Gate Status")](https://sonarcloud.io/dashboard?id=GGG%3ATTT)

 * [![LLL Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=reliability_rating
      "LLL Reliability Rating")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=reliability_rating&view=treemap)
   [![LLL Bugs](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=bugs
      "LLL Bugs")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=bugs)
 * [![LLL Security Rating](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=security_rating
      "LLL Security Rating")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=security_rating&view=treemap)
   [![LLL Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=vulnerabilities
      "LLL Vulnerabilities")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=vulnerabilities)
 * [![LLL Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=sqale_rating
      "LLL Maintainability Rating")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=sqale_rating&view=treemap)
   [![LLL Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=sqale_index
      "LLL Technical Debt")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=sqale_index)
   [![LLL Code Smells](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=code_smells
      "LLL Code Smells")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=code_smells)
 * [![LLL Coverage](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=coverage
      "LLL Coverage")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=coverage&view=treemap)
 * [![LLL Duplicated Lines Density](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=duplicated_lines_density
      "LLL Duplicated Lines Density")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=duplicated_lines_density&view=treemap)
 * [![LLL Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=GGG%3ATTT&metric=ncloc
      "LLL Lines of Code")](https://sonarcloud.io/component_measures?id=GGG%3ATTT&metric=ncloc)
```

## Information (GroupId specific option)

```md
[![LLL Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "LLL Maven Central")
 ![LLL Maven Central](https://img.shields.io/maven-central/v/GGG/TTT.svg?label=Maven%20Central
   "LLL Maven Central")](https://search.maven.org/search?q=g%3AGGG)

[![LLL Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "LLL Nexus")
 ![LLL Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/GGG/TTT.svg?label=Nexus
   "LLL Nexus")](https://oss.sonatype.org/#nexus-search;gav~GGG~~~~)

[![LLL MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "LLL MvnRepository")
 ![LLL MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/GGG/TTT.svg?label=MvnRepository
   "LLL MvnRepository")](https://mvnrepository.com/artifact/GGG)
```

## Information (ArtifactId specific option)

```md
[![LLL Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "LLL Maven Central")
 ![LLL Maven Central](https://img.shields.io/maven-central/v/GGG/TTT.svg?label=Maven%20Central
   "LLL Maven Central")](https://search.maven.org/search?q=g%3AGGG%20AND%20a%3ATTT)

[![LLL Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "LLL Nexus")
 ![LLL Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/GGG/TTT.svg?label=Nexus
   "LLL Nexus")](https://oss.sonatype.org/#nexus-search;gav~GGG~TTT~~~)

[![LLL MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "LLL MvnRepository")
 ![LLL MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/GGG/TTT.svg?label=MvnRepository
   "LLL MvnRepository")](https://mvnrepository.com/artifact/GGG/TTT)
```

## Legend

 * [![Apache License](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache License") Apache License](https://apache.org/licenses) terms and conditions for use, reproduction, and distribution.
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Maven generated Site](https://maven.apache.org/plugins/maven-site-plugin) includes the project's reports that were configured in the POM.
 * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Javadoc](https://en.wikipedia.org/wiki/Javadoc) (originally cased JavaDoc) is a documentation generator created by Sun Microsystems for the Java language.
 * [![Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Dependency Check") Dependency Check](https://owasp.org/www-project-dependency-check)
   is a utility that identifies project dependencies and checks if there are any known, publicly disclosed, vulnerabilities.
 * [![SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "SonarCloud") SonarCloud](https://sonarcloud.io/explore/projects?sort=-analysis_date)
   inspects the quality of source code and detect bugs, vulnerabilities and code smells in more than 20 different languages.
 * [![Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Maven Central") The Central Repository](https://search.maven.org) official search.
 * [![Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Nexus") Nexus Repository Manager](https://oss.sonatype.org) manages binaries and build artifacts across your software supply chain.
 * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") MvnRepository](https://mvnrepository.com) is like Google for Maven artifacts with Dependencies Updates information.

## Demo Information (GroupId specific option)

[![Maven Setup Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Maven Setup Maven Central")
 ![Maven Setup Maven Central](https://img.shields.io/maven-central/v/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=Maven%20Central
   "Maven Setup Maven Central")](https://search.maven.org/search?q=g%3Anl.demon.shadowland.freedumbytes.maven.config)

[![Maven Setup Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Maven Setup Nexus")
 ![Maven Setup Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=Nexus
   "Maven Setup Nexus")](https://oss.sonatype.org/#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.config~~~~)

[![Maven Setup MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "Maven Setup MvnRepository")
 ![Maven Setup MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=MvnRepository
   "Maven Setup MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.config)

## Demo Information (ArtifactId specific option)

[![Maven Setup Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Maven Setup Maven Central")
 ![Maven Setup Maven Central](https://img.shields.io/maven-central/v/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=Maven%20Central
   "Maven Setup Maven Central")](https://search.maven.org/search?q=g%3Anl.demon.shadowland.freedumbytes.maven.config%20AND%20a%3Asetup)

[![Maven Setup Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Maven Setup Nexus")
 ![Maven Setup Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=Nexus
   "Maven Setup Nexus")](https://oss.sonatype.org/#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.config~setup~~~)

[![Maven Setup MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "Maven Setup MvnRepository")
 ![Maven Setup MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.config/setup.svg?label=MvnRepository
   "Maven Setup MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.config/setup)

## Demo Information (SonarCloud with some extra options)

[![Maven Setup Quality Gate Status](https://sonarcloud.io/api/project_badges/quality_gate?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup
   "Maven Setup Quality Gate Status")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup)

[![Maven Setup SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "Maven Setup SonarCloud")
 ![Maven Setup Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=alert_status
   "Maven Setup Quality Gate Status")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup)

 * [![Maven Setup Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=reliability_rating
      "Maven Setup Reliability Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=reliability_rating&view=treemap)
   [![Maven Setup Bugs](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=bugs
      "Maven Setup Bugs")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=bugs)
 * [![Maven Setup Security Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=security_rating
      "Maven Setup Security Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=security_rating&view=treemap)
   [![Maven Setup Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=vulnerabilities
      "Maven Setup Vulnerabilities")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=vulnerabilities)
 * [![Maven Setup Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_rating
      "Maven Setup Maintainability Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_rating&view=treemap)
   [![Maven Setup Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_index
      "Maven Setup Technical Debt")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=sqale_index)
   [![Maven Setup Code Smells](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=code_smells
      "Maven Setup Code Smells")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=code_smells)
 * [![Maven Setup Coverage](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=coverage
      "Maven Setup Coverage")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=coverage&view=treemap)
 * [![Maven Setup Duplicated Lines Density](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=duplicated_lines_density
     "Maven Setup Duplicated Lines Density")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=duplicated_lines_density&view=treemap)
 * [![Maven Setup Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=ncloc
      "Maven Setup Lines of Code")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup&metric=ncloc)

[![Maven Setup SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg
   "Maven Setup SonarCloud")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.config%3Asetup)
