package nl.demon.shadowland.freedumbytes.servlet.http.filter.session.killer;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


@RunWith(MockitoJUnitRunner.class)
public class SessionKillerFilterTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(SessionKillerFilter.class);

  @Mock
  private FilterConfig filterConfig;

  @Mock
  private ServletRequest servletRequest;

  @Mock
  private ServletResponse servletResponse;

  @Mock
  private HttpServletRequest httpServletRequest;

  @Mock
  private HttpSession httpSession;

  @Mock
  private FilterChain filterChain;

  private SessionKillerFilter killerFilter = new SessionKillerFilter();


  @Test
  public void init() throws ServletException
  {
    killerFilter.init(filterConfig);

    LogEvent event = loggerRule.verifyLogEvent();

    assertThat(event.getLevel()).isEqualTo(Level.INFO);
    assertThat(event.getMessage().getFormattedMessage()).isEqualTo("SessionKillerFilter created.");
  }


  @Test
  public void doFilterServletRequest() throws IOException, ServletException
  {
    when(servletRequest.getProtocol()).thenReturn("http");
    when(servletRequest.getServerPort()).thenReturn(80);

    killerFilter.doFilter(servletRequest, servletResponse, filterChain);

    List<LogEvent> events = loggerRule.verifyLogEvents(times(3));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("SessionKillerFilter doFilter.");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("Request       getProtocol = http");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("Request     getServerPort = 80");
  }


  @Test
  public void doFilterHttpServletRequest() throws IOException, ServletException
  {
    when(httpServletRequest.getProtocol()).thenReturn("https");
    when(httpServletRequest.getServerPort()).thenReturn(443);

    when(httpServletRequest.getRequestURI()).thenReturn("/webapp/servlet/info/index.html");
    when(httpServletRequest.getServletPath()).thenReturn("/servlet");
    when(httpServletRequest.getPathInfo()).thenReturn("/info/index.html");
    when(httpServletRequest.getPathTranslated()).thenReturn("/www/docs/info/index.html");

    when(httpServletRequest.getSession()).thenReturn(httpSession);
    when(httpSession.getMaxInactiveInterval()).thenReturn(300, 1);

    killerFilter.doFilter(httpServletRequest, servletResponse, filterChain);

    verify(httpSession).setMaxInactiveInterval(1);

    List<LogEvent> events = loggerRule.verifyLogEvents(times(9));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("SessionKillerFilter doFilter.");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("Request       getProtocol = https");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("Request     getServerPort = 443");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("Request     getRequestURI = /webapp/servlet/info/index.html");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage().getFormattedMessage()).isEqualTo("Request    getServletPath = /servlet");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(5).getMessage().getFormattedMessage()).isEqualTo("Request       getPathInfo = /info/index.html");

    assertThat(events.get(6).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(6).getMessage().getFormattedMessage()).isEqualTo("Request getPathTranslated = /www/docs/info/index.html");

    assertThat(events.get(7).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(7).getMessage().getFormattedMessage()).isEqualTo("Session found with getMaxInactiveInterval 300.");

    assertThat(events.get(8).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(8).getMessage().getFormattedMessage()).isEqualTo("Session reset setMaxInactiveInterval to 1.");
  }


  @Test
  public void destroy()
  {
    killerFilter.destroy();

    LogEvent event = loggerRule.verifyLogEvent();

    assertThat(event.getLevel()).isEqualTo(Level.INFO);
    assertThat(event.getMessage().getFormattedMessage()).isEqualTo("SessionKillerFilter destroyed.");
  }
}
