package nl.demon.shadowland.freedumbytes.test.unit.rule;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.answerVoid;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.rules.ExternalResource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;

import lombok.extern.log4j.Log4j2;


/**
 * Assertions for <a href="https://logging.apache.org/log4j/" target="_blank" rel="noopener noreferrer">Log4j</a> logging behaviour in unit tests.
 *
 * @see <a href="https://slackhacker.com/2009/12/08/testing-logging-behaviour-in-four-code-lines-flat/" target="_blank" rel="noopener noreferrer">Testing logging behaviour in four code lines flat</a>
 * @see <a href="http://alexandregiannini.blogspot.com/2015/04/testing-logging-behavior-is.html" target="_blank" rel="noopener noreferrer">Easy verifying of logging events in jUnit tests with Mockito</a>
 * @see <a href="https://github.com/junit-team/junit4/wiki/rules" target="_blank" rel="noopener noreferrer">Rules allow very flexible addition or redefinition of the behavior of each test method in a test class</a>
 */
@Log4j2
public class LoggerRule extends ExternalResource
{
  @Mock
  private Appender appender;

  private AutoCloseable closeableMocks;
  private List<LogEvent> logEvents = new ArrayList<>();
  private String name;
  private Level originalLevel;


  public LoggerRule(Class<?> clazz)
  {
    this(clazz.getName());
  }


  public LoggerRule(String name)
  {
    this.name = name;
  }


  public void verifyLogEventNever()
  {
    verifyLogEvent(never());
  }


  public void verifyLogEventNever(Level level)
  {
    List<LogEvent> events = verifyLogEventsWithFilter(level);

    assertThat(events).hasSize(0);
  }


  public LogEvent verifyLogEvent(Level level)
  {
    List<LogEvent> events = verifyLogEventsWithFilter(level);

    assertThat(events).hasSize(1);

    return events.get(0);
  }


  public List<LogEvent> verifyLogEvents(Level level)
  {
    List<LogEvent> events = verifyLogEventsWithFilter(level);

    assertThat(events.size()).isGreaterThanOrEqualTo(1);

    return events;
  }


  public List<LogEvent> verifyLogEvents(Level level, int wantedNumberOfInvocations)
  {
    List<LogEvent> events = verifyLogEventsWithFilter(level);

    assertThat(events.size()).isEqualTo(wantedNumberOfInvocations);

    return events;
  }


  private List<LogEvent> verifyLogEventsWithFilter(Level level)
  {
    List<LogEvent> events = verifyLogEvents();
    events = filter(events, level);

    return events;
  }


  private static List<LogEvent> filter(List<LogEvent> events, Level level)
  {
    return events.stream().filter(evt -> evt.getLevel().equals(level)).collect(Collectors.toList());
  }


  public LogEvent verifyLogEvent()
  {
    return verifyLogEvent(times(1)).get(0);
  }


  public List<LogEvent> verifyLogEvents()
  {
    return verifyLogEvent(atLeastOnce());
  }


  public List<LogEvent> verifyLogEvents(VerificationMode mode)
  {
    return verifyLogEvent(mode);
  }


  private List<LogEvent> verifyLogEvent(VerificationMode mode)
  {
    verify(getAppender(), mode).append(any(LogEvent.class));

    return logEvents;
  }


  public Appender getAppender()
  {
    return appender;
  }


  @Override
  protected void before() throws Throwable
  {
    closeableMocks = MockitoAnnotations.openMocks(this);

    originalLevel = addAppender(getAppender(), name, logEvents);
  }


  @Override
  protected void after()
  {
    removeAppender(getAppender(), name, originalLevel);

    try
    {
      closeableMocks.close();
    }
    catch (Exception e)
    {
      log.error("Failed to close mocks.", e);
    }
  }


  protected static Level addAppender(Appender appender, String name, List<LogEvent> logEvents)
  {
    when(appender.getName()).thenReturn("LoggerRuleAppender");
    when(appender.isStarted()).thenReturn(true);

    doAnswer(answerVoid((LogEvent logEvent) -> logEvents.add(logEvent.toImmutable()))).when(appender).append(any(LogEvent.class));

    getLogger(name).addAppender(appender);

    Level originalLevel = getLogger(name).getLevel();
    withLevel(name, Level.ALL);

    return originalLevel;
  }


  protected static void removeAppender(Appender appender, String name, Level originalLevel)
  {
    getLogger(name).removeAppender(appender);

    withLevel(name, originalLevel);
  }


  public void withLevel(Level level)
  {
    withLevel(name, level);
  }


  private static void withLevel(String name, Level level)
  {
    Configurator.setAllLevels(getLogger(name).getName(), level); // NOSONAR - Unit testing only.
  }


  private static Logger getLogger(String name)
  {
    return (Logger) LogManager.getLogger(name);
  }
}
