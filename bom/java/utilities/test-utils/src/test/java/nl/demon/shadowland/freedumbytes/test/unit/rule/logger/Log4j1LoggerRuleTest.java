package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.never;

import org.apache.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


public class Log4j1LoggerRuleTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Log4jEventLogger.class);

  private Log4j1EventLogger eventLogger = new Log4j1EventLogger();
  private Level originalLevel;

  @Before
  public void setUp()
  {
    originalLevel = eventLogger.getLogger().getLevel();
    eventLogger.getLogger().setLevel(Level.ALL);
  }


  @After
  public void tearDown()
  {
    eventLogger.getLogger().setLevel(originalLevel);
  }


  @Test
  public void logAll()
  {
    eventLogger.logAll();

    loggerRule.verifyLogEvents(never());
  }


  @Test
  public void logInterfaceClassSourceCodeLocation()
  {
    assertThat(Logger.class.getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/log4j-api-");
    assertThat(eventLogger.getLogger().getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/log4j-1.2-api-");
    assertThat(Level.class.getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/log4j-1.2-api-");
  }
}
