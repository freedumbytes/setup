package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import org.apache.commons.logging.Log;

import lombok.extern.apachecommons.CommonsLog;


/**
 * Apache Commons Logging (previously known as Jakarta Commons Logging or JCL).
 */
@CommonsLog
public class CommonsLoggingEventLogger
{
  public void logAll()
  {
    log.fatal("fatal message");
    log.error("error message");
    log.warn("warn message");
    log.info("info message");
    log.debug("debug message");
    log.trace("trace message");
  }


  public Log getLogger()
  {
    return log;
  }
}
