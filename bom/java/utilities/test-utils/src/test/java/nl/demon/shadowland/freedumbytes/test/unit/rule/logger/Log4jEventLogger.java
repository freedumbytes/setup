package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;



import org.apache.logging.log4j.Logger;

import lombok.extern.log4j.Log4j2;


/**
 * Apache Log4j2 is a Java-based logging utility.
 */
@Log4j2
public class Log4jEventLogger
{
  public void logNothing()
  {
  }


  public void logError()
  {
    log.error("error message");
  }


  public void logAll()
  {
    log.fatal("fatal message");
    log.error("error message");
    log.warn("warn message");
    log.info("info message");
    log.debug("debug message");
    log.trace("trace message");
  }


  public void logExtraClass()
  {
    logAll();

    new Log4jEventLoggerExtra().logExtra();
  }


  public Logger getLogger()
  {
    return log;
  }
}
