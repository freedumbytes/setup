package nl.demon.shadowland.freedumbytes.unit.junit;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import org.junit.Test;


public class ExceptionAssertionUnitTest
{
  @Test
  public void whenAssertingException_thenThrown()
  {
    Throwable exception = assertThrows(IllegalArgumentException.class, () ->
    {
      throw new IllegalArgumentException("Exception message");
    });

    assertEquals("Exception message", exception.getMessage());
  }
}
