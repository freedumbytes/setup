package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import org.apache.log4j.Logger;

import lombok.extern.log4j.Log4j;


/**
 * Apache Log4j1 is a Java-based logging utility.
 */
@Log4j
public class Log4j1EventLogger
{
  public void logAll()
  {
    log.fatal("fatal message");
    log.error("error message");
    log.warn("warn message");
    log.info("info message");
    log.debug("debug message");
    log.trace("trace message");
  }


  public Logger getLogger()
  {
    return log;
  }
}
