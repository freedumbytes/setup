package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


public class Log4jLoggerRulePackageTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Log4jEventLogger.class.getPackage().getName());

  private Log4jEventLogger eventLogger = new Log4jEventLogger();


  @Test
  public void logAll()
  {
    eventLogger.logAll();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(6));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("info message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage().getFormattedMessage()).isEqualTo("debug message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(5).getMessage().getFormattedMessage()).isEqualTo("trace message");
  }


  @Test
  public void logExtraClass()
  {
    eventLogger.logExtraClass();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(7));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("info message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage().getFormattedMessage()).isEqualTo("debug message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(5).getMessage().getFormattedMessage()).isEqualTo("trace message");

    assertThat(events.get(6).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(6).getMessage().getFormattedMessage()).isEqualTo("fatal extra message");
  }
}
