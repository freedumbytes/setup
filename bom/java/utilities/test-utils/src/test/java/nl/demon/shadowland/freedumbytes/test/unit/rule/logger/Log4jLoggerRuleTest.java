package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


public class Log4jLoggerRuleTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Log4jEventLogger.class);

  private Log4jEventLogger eventLogger = new Log4jEventLogger();


  @Test
  public void logNothing()
  {
    eventLogger.logNothing();

    loggerRule.verifyLogEventNever();
  }


  @Test
  public void logError()
  {
    eventLogger.logError();

    LogEvent event = loggerRule.verifyLogEvent();

    assertThat(event.getLevel()).isEqualTo(Level.ERROR);
    assertThat(event.getMessage().getFormattedMessage()).isEqualTo("error message");
  }


  @Test
  public void logErrorNoFatal()
  {
    eventLogger.logError();

    loggerRule.verifyLogEventNever(Level.FATAL);
  }


  @Test
  public void logErrorFilterFatal()
  {
    eventLogger.logError();

    loggerRule.verifyLogEvents(Level.FATAL, 0);
  }


  @Test
  public void logErrorFilterError()
  {
    eventLogger.logError();

    LogEvent event = loggerRule.verifyLogEvent(Level.ERROR);

    assertThat(event.getLevel()).isEqualTo(Level.ERROR);
    assertThat(event.getMessage().getFormattedMessage()).isEqualTo("error message");
  }


  @Test
  public void logAllFilterErrors()
  {
    eventLogger.logError();

    List<LogEvent> events = loggerRule.verifyLogEvents(Level.ERROR);

    assertThat(events).hasSize(1);
    assertThat(events.get(0).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("error message");
  }


  @Test
  public void logAll()
  {
    eventLogger.logAll();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(6));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("info message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage().getFormattedMessage()).isEqualTo("debug message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(5).getMessage().getFormattedMessage()).isEqualTo("trace message");
  }


  @Test
  public void logExtraClass()
  {
    eventLogger.logExtraClass();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(6));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("info message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage().getFormattedMessage()).isEqualTo("debug message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(5).getMessage().getFormattedMessage()).isEqualTo("trace message");
  }


  @Test
  public void dontLogAllWithLevelInfo()
  {
    loggerRule.withLevel(Level.INFO);

    eventLogger.logAll();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(4));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("info message");
  }


  @Test
  public void logInterfaceClassSourceCodeLocation()
  {
    assertThat(Logger.class.getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/log4j-api-");
    assertThat(eventLogger.getLogger().getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/log4j-core-");
  }
}
