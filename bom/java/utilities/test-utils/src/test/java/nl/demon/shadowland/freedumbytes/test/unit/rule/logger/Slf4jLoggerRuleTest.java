package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


public class Slf4jLoggerRuleTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Slf4jEventLogger.class);

  private Slf4jEventLogger eventLogger = new Slf4jEventLogger();


  @Test
  public void logAll()
  {
    eventLogger.logAll();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(5));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("error message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("warn message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("info message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("debug message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(4).getMessage().getFormattedMessage()).isEqualTo("trace message");
  }


  @Test
  public void logInterfaceClassSourceCodeLocation()
  {
    assertThat(Logger.class.getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/slf4j-api-");
    assertThat(eventLogger.getLogger().getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/log4j-slf4j-impl-");
  }
}
