package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.jul.LevelTranslator;
import org.apache.logging.log4j.jul.LogManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


public class JavaUtilLoggingLoggerRuleTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(JavaUtilLoggingEventLogger.class);

  private JavaUtilLoggingEventLogger eventLogger = new JavaUtilLoggingEventLogger();


  @BeforeClass
  public static void setUpClass()
  {
    System.setProperty("java.util.logging.manager", LogManager.class.getName());
  }


  @AfterClass
  public static void tearDownClass()
  {
    System.clearProperty("java.util.logging.manager");
  }


  @Test
  public void logAll()
  {
    eventLogger.logAll();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(7));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("severe message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo("warning message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(2).getMessage().getFormattedMessage()).isEqualTo("info message");

    assertThat(events.get(3).getLevel()).isEqualTo(LevelTranslator.CONFIG);
    assertThat(events.get(3).getMessage().getFormattedMessage()).isEqualTo("config message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage().getFormattedMessage()).isEqualTo("fine message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(5).getMessage().getFormattedMessage()).isEqualTo("finer message");

    assertThat(events.get(6).getLevel()).isEqualTo(LevelTranslator.FINEST);
    assertThat(events.get(6).getMessage().getFormattedMessage()).isEqualTo("finest message");
  }


  @Test
  public void logClassSourceCodeLocation()
  {
    assertThat(eventLogger.getLogger().getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).contains("/log4j-jul-");
  }
}
