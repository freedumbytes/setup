package nl.demon.shadowland.freedumbytes.test.unit.rule;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class LoggerRuleTest
{
  @Mock
  private Appender appender;

  private List<LogEvent> logEvents = new ArrayList<>();


  @Test
  public void afterException()
  {
    Level originalLevel = LoggerRule.addAppender(appender, LoggerRule.class.getName(), logEvents);

    new LoggerRule(LoggerRuleTest.class)
    {
      @Override
      public Appender getAppender()
      {
        return appender;
      }
    }.after();

    LoggerRule.removeAppender(appender, LoggerRule.class.getName(), originalLevel);

    assertThat(logEvents).hasSize(1);

    LogEvent logEvent = logEvents.get(0);
    assertThat(logEvent.getLevel()).isEqualTo(Level.ERROR);
    assertThat(logEvent.getMessage().getFormattedMessage()).isEqualTo("Failed to close mocks.");
    assertThat(logEvent.getThrown()).isInstanceOf(NullPointerException.class);
  }
}
