package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import java.util.logging.Logger;

import lombok.extern.java.Log;


/**
 * Package java.util.logging provides the classes and interfaces of the Java 2 platform's core logging facilities.
 */
@Log
public class JavaUtilLoggingEventLogger
{
  public void logAll()
  {
    log.severe("severe message");
    log.warning("warning message");
    log.info("info message");
    log.config("config message");
    log.fine("fine message");
    log.finer("finer message");
    log.finest("finest message");
  }


  public Logger getLogger()
  {
    return log;
  }
}
