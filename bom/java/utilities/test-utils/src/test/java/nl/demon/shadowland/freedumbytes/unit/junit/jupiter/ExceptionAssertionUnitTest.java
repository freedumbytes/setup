package nl.demon.shadowland.freedumbytes.unit.junit.jupiter;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;


class ExceptionAssertionUnitTest
{
  @Test
  void whenAssertingException_thenThrown()
  {
    Throwable exception = assertThrows(IllegalArgumentException.class, () ->
    {
      throw new IllegalArgumentException("Exception message");
    });

    assertEquals("Exception message", exception.getMessage());
  }
}
