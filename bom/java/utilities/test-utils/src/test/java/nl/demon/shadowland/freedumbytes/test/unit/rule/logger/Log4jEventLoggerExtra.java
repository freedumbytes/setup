package nl.demon.shadowland.freedumbytes.test.unit.rule.logger;


import lombok.extern.log4j.Log4j2;


@Log4j2
public class Log4jEventLoggerExtra
{
  public void logExtra()
  {
    log.fatal("fatal extra message");
  }
}
