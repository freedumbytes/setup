package nl.demon.shadowland.freedumbytes.java.util.logging.jersey;


import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.slf4j.LoggerFactory;

import lombok.extern.slf4j.Slf4j;


/**
 * Jersey {@code LoggingFeature} Slf4j Wrapper to handle all logging of package or class as if of supplied level and uses {@link #isLoggable} to check if {@code log4j2.xml} is set to this level or higher.
 *
 * <p>
 * Alternative Logger for Jersey {@code LoggingFeature} that doesn't require {@code SLF4JBridgeHandler}.
 * </p>
 * <p>
 * The jul-to-slf4j.jar artifact includes a {@code java.util.logging} (jul) handler, namely {@code SLF4JBridgeHandler}, which routes all incoming jul records to the SLF4J API.
 * </p>
 * <p>
 * If you are concerned about application performance, then use of {@code SLF4JBridgeHandler} is appropriate only if any one of the following two conditions is true:
 * </p>
 *
 * <ol>
 * <li>few j.u.l. logging statements are in play;</li>
 * <li>{@code LevelChangePropagator} has been installed.</li>
 * </ol>
 *
 * @see <a href="https://www.slf4j.org/legacy.html" target="_blank" rel="noopener noreferrer">Bridging legacy APIs</a>
 * @see <a href="https://stackoverflow.com/questions/4121722/how-to-make-jersey-to-use-slf4j-instead-of-jul" target="_blank" rel="noopener noreferrer">How to make Jersey to use SLF4J instead of JUL?</a>
 * @see <a href="https://github.com/eclipse-ee4j/jersey/tree/master/core-common/src/main/java/org/glassfish/jersey/logging" target="_blank" rel="noopener noreferrer">Eclipse EE4J Jersey logging</a>
 * @see <a href="https://github.com/apache/cxf/tree/master/core/src/main/java/org/apache/cxf/common/logging" target="_blank" rel="noopener noreferrer">Apache CXF logging</a>
 * @see <a href="https://codingcraftsman.wordpress.com/2015/04/28/log4j2-mocking-with-mockito-and-junit" target="_blank" rel="noopener noreferrer">Apache CXF logging</a>
 */
@Slf4j
public class Slf4jLoggingFeatureLevelWrapper extends Logger
{
  private static final int TRACE_LEVEL_THRESHOLD = Level.FINEST.intValue();
  private static final int DEBUG_LEVEL_THRESHOLD = Level.FINE.intValue();
  private static final int INFO_LEVEL_THRESHOLD = Level.INFO.intValue();
  private static final int WARN_LEVEL_THRESHOLD = Level.WARNING.intValue();

  private String name;
  private Level asIfLevel;


  public Slf4jLoggingFeatureLevelWrapper(Class<?> clazz, String resourceBundleName, Level asIfLevel)
  {
    this(clazz.getName(), resourceBundleName, asIfLevel);
  }


  public Slf4jLoggingFeatureLevelWrapper(String name, String resourceBundleName, Level asIfLevel)
  {
    super(name, resourceBundleName);

    this.name = name;
    this.asIfLevel = asIfLevel;
  }


  @Override
  public boolean isLoggable(Level julLevel)
  {
    boolean isLoggable;
    int asIfLevelValue = asIfLevel.intValue();

    if (asIfLevelValue <= TRACE_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isTraceEnabled();
    }
    else if (asIfLevelValue <= DEBUG_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isDebugEnabled();
    }
    else if (asIfLevelValue <= INFO_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isInfoEnabled();
    }
    else if (asIfLevelValue <= WARN_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isWarnEnabled();
    }
    else
    {
      isLoggable = getLogger().isErrorEnabled();
    }

    log.debug("isLoggable {} for original level {} but overruled with asIfLevel {}", isLoggable, julLevel, asIfLevel);

    return isLoggable;
  }


  @Override
  public void log(LogRecord logRecord)
  {
    int julLevelValue = asIfLevel.intValue();
    log.debug("record loggername {}, level {} and overrule asIfLevel {}", logRecord.getLoggerName(), logRecord.getLevel(), asIfLevel);

    if (julLevelValue <= TRACE_LEVEL_THRESHOLD)
    {
      getLogger().trace(logRecord.getMessage());
    }
    else if (julLevelValue <= DEBUG_LEVEL_THRESHOLD)
    {
      getLogger().debug(logRecord.getMessage());
    }
    else if (julLevelValue <= INFO_LEVEL_THRESHOLD)
    {
      getLogger().info(logRecord.getMessage());
    }
    else if (julLevelValue <= WARN_LEVEL_THRESHOLD)
    {
      getLogger().warn(logRecord.getMessage());
    }
    else
    {
      getLogger().error(logRecord.getMessage());
    }
  }


  private org.slf4j.Logger getLogger()
  {
    return LoggerFactory.getLogger(name);
  }
}
