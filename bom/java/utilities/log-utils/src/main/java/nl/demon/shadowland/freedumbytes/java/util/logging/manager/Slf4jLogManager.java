package nl.demon.shadowland.freedumbytes.java.util.logging.manager;


import java.util.logging.LogManager;
import java.util.logging.Logger;

import lombok.extern.slf4j.Slf4j;


/**
 * Custom {@code LogManager} for {@code java.util.logging} that doesn't require {@code SLF4JBridgeHandler} and uses {@link Slf4jLoggerWrapper#isLoggable} thus removing the performance issue of the bridge handler.
 *
 * <p>
 * At startup the {@code LogManager} class is located using the {@code java.util.logging.manager} system property. Thus to activate this custom one use the following command line option
 * {@code -Djava.util.logging.manager=nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager}.
 * </p>
 *
 * @see <a href="https://www.slf4j.org/legacy.html" target="_blank" rel="noopener noreferrer">Bridging legacy APIs</a>
 * @see <a href="https://stackoverflow.com/questions/4121722/how-to-make-jersey-to-use-slf4j-instead-of-jul" target="_blank" rel="noopener noreferrer">How to make Jersey to use SLF4J instead of JUL?</a>
 * @see <a href="https://github.com/eclipse-ee4j/jersey/tree/master/core-common/src/main/java/org/glassfish/jersey/logging" target="_blank" rel="noopener noreferrer">Eclipse EE4J Jersey logging</a>
 * @see <a href="https://github.com/apache/cxf/tree/master/core/src/main/java/org/apache/cxf/common/logging" target="_blank" rel="noopener noreferrer">Apache CXF logging</a>
 */
@Slf4j
public class Slf4jLogManager extends LogManager
{
  public static boolean isInstalled()
  {
    boolean installed = (LogManager.getLogManager() instanceof Slf4jLogManager);

    log.info("Slf4jLogManager is {}installed.", (installed ? "" : "NOT "));

    return installed;
  }


  @Override
  public boolean addLogger(Logger logger)
  {
    boolean addedLogger;

    if (needsWrapping(logger))
    {
      super.addLogger(new Slf4jLoggerWrapper(logger));

      addedLogger = false;
    }
    else
    {
      addedLogger = super.addLogger(logger);
    }

    return addedLogger;
  }


  private static boolean needsWrapping(Logger logger)
  {
    return !(logger instanceof Slf4jLoggerWrapper);
  }
}
