package nl.demon.shadowland.freedumbytes.java.util.logging.jersey;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.apache.logging.log4j.core.LogEvent;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.logger.Slf4jEventLogger;
import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


public class Slf4jLoggingFeatureLevelWrapperTest
{
  private static final String SOME_MESSAGE = "some message";

  @Rule
  public LoggerRule loggerRule = new LoggerRule(Slf4jLoggingFeatureLevelWrapperTest.class.getPackage().getName());


  @Test
  public void logFinestAsIfTrace()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST);

    slf4jLoggingFeatureLevelWrapper.log(createLogRecord());

    List<LogEvent> events = loggerRule.verifyLogEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("record loggername nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper, level OFF and overrule asIfLevel FINEST");

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.logging.log4j.Level.TRACE);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logFinerAsIfDebug()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINER);

    slf4jLoggingFeatureLevelWrapper.log(createLogRecord());

    List<LogEvent> events = loggerRule.verifyLogEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("record loggername nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper, level OFF and overrule asIfLevel FINER");

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logFineAsIfDebug()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINE);

    slf4jLoggingFeatureLevelWrapper.log(createLogRecord());

    List<LogEvent> events = loggerRule.verifyLogEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("record loggername nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper, level OFF and overrule asIfLevel FINE");

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logConfigAsIfInfo()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.CONFIG);

    slf4jLoggingFeatureLevelWrapper.log(createLogRecord());

    List<LogEvent> events = loggerRule.verifyLogEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("record loggername nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper, level OFF and overrule asIfLevel CONFIG");

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.logging.log4j.Level.INFO);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logInfoAsIfInfo()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.INFO);

    slf4jLoggingFeatureLevelWrapper.log(createLogRecord());

    List<LogEvent> events = loggerRule.verifyLogEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("record loggername nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper, level OFF and overrule asIfLevel INFO");

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.logging.log4j.Level.INFO);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logWarningAsIfWarn()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.WARNING);

    slf4jLoggingFeatureLevelWrapper.log(createLogRecord());

    List<LogEvent> events = loggerRule.verifyLogEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("record loggername nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper, level OFF and overrule asIfLevel WARNING");

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.logging.log4j.Level.WARN);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logSevereAsIfError()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.SEVERE);

    slf4jLoggingFeatureLevelWrapper.log(createLogRecord());

    List<LogEvent> events = loggerRule.verifyLogEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("record loggername nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper, level OFF and overrule asIfLevel SEVERE");

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.logging.log4j.Level.ERROR);
    assertThat(events.get(1).getMessage().getFormattedMessage()).isEqualTo(SOME_MESSAGE);
  }


  private LogRecord createLogRecord()
  {
    LogRecord logRecord = new LogRecord(Level.OFF, SOME_MESSAGE);
    logRecord.setLoggerName(Slf4jLoggingFeatureLevelWrapper.class.getName());

    return logRecord;
  }


  @Test
  public void isLoggableWithTrace()
  {
    loggerRule.withLevel(org.apache.logging.log4j.Level.TRACE);

    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINER).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINE).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.CONFIG).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.INFO).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.WARNING).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.SEVERE).isLoggable(Level.OFF)).isTrue();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(7));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("isLoggable true for original level OFF but overruled with asIfLevel FINEST");

    assertThat(events.get(6).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(6).getMessage().getFormattedMessage()).isEqualTo("isLoggable true for original level OFF but overruled with asIfLevel SEVERE");
  }


  @Test
  public void isLoggableWithDebug()
  {
    loggerRule.withLevel(org.apache.logging.log4j.Level.DEBUG);

    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINER).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINE).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.CONFIG).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.INFO).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.WARNING).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.SEVERE).isLoggable(Level.OFF)).isTrue();

    List<LogEvent> events = loggerRule.verifyLogEvents(times(7));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("isLoggable false for original level OFF but overruled with asIfLevel FINEST");

    assertThat(events.get(6).getLevel()).isEqualTo(org.apache.logging.log4j.Level.DEBUG);
    assertThat(events.get(6).getMessage().getFormattedMessage()).isEqualTo("isLoggable true for original level OFF but overruled with asIfLevel SEVERE");
  }


  @Test
  public void isLoggableWithInfo()
  {
    loggerRule.withLevel(org.apache.logging.log4j.Level.INFO);

    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINER).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINE).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.CONFIG).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.INFO).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.WARNING).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.SEVERE).isLoggable(Level.OFF)).isTrue();
  }


  @Test
  public void isLoggableWithWarn()
  {
    loggerRule.withLevel(org.apache.logging.log4j.Level.WARN);

    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINER).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINE).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.CONFIG).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.INFO).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.WARNING).isLoggable(Level.OFF)).isTrue();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.SEVERE).isLoggable(Level.OFF)).isTrue();
  }


  @Test
  public void isLoggableWithError()
  {
    loggerRule.withLevel(org.apache.logging.log4j.Level.ERROR);

    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINER).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINE).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.CONFIG).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.INFO).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.WARNING).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.SEVERE).isLoggable(Level.OFF)).isTrue();
  }


  @Test
  public void isLoggableWithOff()
  {
    loggerRule.withLevel(org.apache.logging.log4j.Level.OFF);

    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINER).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINE).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.CONFIG).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.INFO).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.WARNING).isLoggable(Level.OFF)).isFalse();
    assertThat(createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.SEVERE).isLoggable(Level.OFF)).isFalse();
  }


  @Test
  public void isLoggableClass()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level.FINEST);

    assertThat(slf4jLoggingFeatureLevelWrapper.isLoggable(Level.OFF)).isTrue();
  }


  @Test
  public void isLoggablePackage()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = new Slf4jLoggingFeatureLevelWrapper("nl.demon.shadowland.freedumbytes.java.util.logging.jersey", null, Level.FINEST);

    assertThat(slf4jLoggingFeatureLevelWrapper.isLoggable(Level.OFF)).isTrue();
  }


  @Test
  public void isLoggableDifferentClass()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = new Slf4jLoggingFeatureLevelWrapper(Slf4jEventLogger.class, null, Level.FINEST);

    assertThat(slf4jLoggingFeatureLevelWrapper.isLoggable(Level.OFF)).isFalse();
  }


  @Test
  public void isLoggableDifferentPackage()
  {
    Slf4jLoggingFeatureLevelWrapper slf4jLoggingFeatureLevelWrapper = new Slf4jLoggingFeatureLevelWrapper("nl.demon.shadowland.freedumbytes.rest", null, Level.FINEST);

    assertThat(slf4jLoggingFeatureLevelWrapper.isLoggable(Level.OFF)).isFalse();
  }


  private Slf4jLoggingFeatureLevelWrapper createSlf4jLoggingFeatureLevelWrapperAsIfLevel(Level asIfLevel)
  {
    return new Slf4jLoggingFeatureLevelWrapper(Slf4jLoggingFeatureLevelWrapper.class, null, asIfLevel);
  }
}
