package nl.demon.shadowland.freedumbytes.java.util.logging.manager;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.logging.log4j.core.LogEvent;
import org.junit.Rule;
import org.junit.Test;

import lombok.extern.java.Log;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


@Log
public class LogManagerTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Slf4jLogManager.class.getPackage().getName());


  @Test
  public void isInstalled()
  {
    Slf4jLogManager.isInstalled();

    List<LogEvent> events = loggerRule.verifyLogEvents();

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.logging.log4j.Level.INFO);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("Slf4jLogManager is NOT installed.");
  }


  @Test
  public void addLoggerAnnotation()
  {
    assertThat(log).isExactlyInstanceOf(Logger.class);
  }


  @Test
  public void addLogger()
  {
    SomeLogger logger = new SomeLogger();
    assertThat(Slf4jLogManager.getLogManager().addLogger(logger)).isTrue();

    Logger someLogger = Slf4jLogManager.getLogManager().getLogger(SomeLogger.class.getName());

    assertThat(someLogger).isNotNull() //
        .isInstanceOf(Logger.class) //
        .isExactlyInstanceOf(SomeLogger.class) //
        .isNotInstanceOf(Slf4jLoggerWrapper.class);
  }


  @Test
  public void addSlf4jLoggerWrapper()
  {
    int retryCounter = 0;
    Logger anotherLogger = null;

    while (anotherLogger == null)
    {
      retryCounter++;
      assertThat(Slf4jLogManager.getLogManager().addLogger(new Slf4jLoggerWrapper(new AnotherLogger()))).isTrue();

      anotherLogger = Slf4jLogManager.getLogManager().getLogger(AnotherLogger.class.getName());
    }

    assertThat(anotherLogger).isNotNull();

    anotherLogger.log(Level.INFO, "RetryCounter: {0}", retryCounter);

    assertThat(anotherLogger) //
        .isInstanceOf(Logger.class) //
        .isNotInstanceOf(AnotherLogger.class) //
        .isExactlyInstanceOf(Slf4jLoggerWrapper.class);
  }
}
