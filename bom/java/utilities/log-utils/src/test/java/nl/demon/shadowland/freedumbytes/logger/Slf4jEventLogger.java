package nl.demon.shadowland.freedumbytes.logger;


import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Slf4jEventLogger
{
  public void logAll()
  {
    log.error("error message");
    log.warn("warn message");
    log.info("info message");
    log.debug("debug message");
    log.trace("trace message");
  }
}
