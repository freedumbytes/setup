package nl.demon.shadowland.freedumbytes.java.util.logging.manager;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.logging.Logger;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.test.unit.rule.LoggerRule;


public class Slf4jLogManagerTest
{
  @BeforeClass
  public static void setUpClass()
  {
    System.setProperty("java.util.logging.manager", Slf4jLogManager.class.getName());
  }


  @AfterClass
  public static void tearDownClass()
  {
    System.clearProperty("java.util.logging.manager");
  }


  @Rule
  public LoggerRule loggerRule = new LoggerRule(Slf4jLogManager.class.getPackage().getName());


  @Test
  public void isInstalled()
  {
    Slf4jLogManager.isInstalled();

    List<LogEvent> events = loggerRule.verifyLogEvents();

    assertThat(events.get(0).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(0).getMessage().getFormattedMessage()).isEqualTo("Slf4jLogManager is installed.");
  }


  @Test
  public void addLoggerAnnotation()
  {
    Sample sample = new Sample();

    assertThat(sample.getLogger()).isInstanceOf(Logger.class);
    assertThat(sample.getLogger()).isExactlyInstanceOf(Slf4jLoggerWrapper.class);
  }


  @Test
  public void addLogger()
  {
    SomeLogger logger = new SomeLogger();
    assertThat(Slf4jLogManager.getLogManager().addLogger(logger)).isFalse();

    Logger someLogger = Slf4jLogManager.getLogManager().getLogger(SomeLogger.class.getName());

    assertThat(someLogger).isNotNull() //
        .isInstanceOf(Logger.class) //
        .isNotInstanceOf(SomeLogger.class) //
        .isExactlyInstanceOf(Slf4jLoggerWrapper.class);
  }


  @Test
  public void addSlf4jLoggerWrapper()
  {
    Slf4jLoggerWrapper logger = new Slf4jLoggerWrapper(new AnotherLogger());
    assertThat(Slf4jLogManager.getLogManager().addLogger(logger)).isTrue();

    Logger anotherLogger = Slf4jLogManager.getLogManager().getLogger(AnotherLogger.class.getName());

    assertThat(anotherLogger).isNotNull() //
        .isInstanceOf(Logger.class) //
        .isNotInstanceOf(AnotherLogger.class) //
        .isExactlyInstanceOf(Slf4jLoggerWrapper.class);
  }
}
