package nl.demon.shadowland.freedumbytes.hibernate.repository;


import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.SingularAttribute;

import lombok.AccessLevel;
import lombok.Getter;

import nl.demon.shadowland.freedumbytes.reflect.Parameterizable;


/**
 * {@code JPA} implementation of {@link GenericDao}.
 *
 * @param <E>
 *          The entity type.
 * @param <K>
 *          The primary key type.
 *
 * @see <a href="https://in.relation.to/2005/09/09/generic-dao-pattern-with-jdk-50/" target="_blank" rel="noopener noreferrer">Generic DAO pattern with JDK 5.0 posted by Christian Bauer</a>
 * @see <a href="https://developer.jboss.org/wiki/GenericDataAccessObjects" target="_blank" rel="noopener noreferrer">Generic Data Access Objects posted by Anthony Patricio</a>
 */
public abstract class JpaDao<E extends Identifiable<K>, K extends Serializable> implements GenericDao<E, K>
{
  @PersistenceContext()
  @Getter(AccessLevel.PROTECTED)
  private EntityManager entityManager;

  @Getter
  private Class<E> entityType;

  @Getter
  private Class<K> primaryKeyType;


  @SuppressWarnings("unchecked")
  protected JpaDao()
  {
    entityType = (Class<E>) Parameterizable.getActualTypeArgument(getClass(), JpaDao.class, JpaDaoGenericParams.ENTITY.ordinal());
    primaryKeyType = (Class<K>) Parameterizable.getActualTypeArgument(getClass(), JpaDao.class, JpaDaoGenericParams.KEY.ordinal());
  }


  @Override()
  public E create(E entity)
  {
    return createOrUpdate(entity);
  }


  @Override()
  public E read(K id)
  {
    return getEntityManager().find(getEntityType(), id);
  }


  @Override()
  public E update(E entity)
  {
    return createOrUpdate(entity);
  }


  private E createOrUpdate(E entity)
  {
    if (entity.getId() == null)
    {
      getEntityManager().persist(entity);

      return entity;
    }
    else
    {
      return getEntityManager().merge(entity);
    }
  }


  @Override()
  public void delete(E entity)
  {
    if (isManaged(entity))
    {
      getEntityManager().remove(entity);
    }
    else
    {
      getEntityManager().remove(getEntityManager().merge(entity));
    }
  }


  @Override()
  public boolean exists(K id)
  {
    CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<K> criteriaQuery = builder.createQuery(getPrimaryKeyType());
    Root<E> from = criteriaQuery.from(getEntityType());

    SingularAttribute<E, K> idAttribute = getIdAttribute();
    Predicate whereClause = builder.equal(from.get(idAttribute), id);
    criteriaQuery.where(whereClause);

    criteriaQuery.select(from.get(idAttribute));

    TypedQuery<K> typedQuery = getEntityManager().createQuery(criteriaQuery);

    return !(typedQuery.getResultList().isEmpty());
  }


  protected SingularAttribute<E, K> getIdAttribute()
  {
    Metamodel metamodel = getEntityManager().getMetamodel();
    EntityType<E> selectOf = metamodel.entity(getEntityType());

    return selectOf.getDeclaredId(getPrimaryKeyType());
  }


  @Override()
  public boolean isManaged(E entity)
  {
    return getEntityManager().contains(entity);
  }
}
