package nl.demon.shadowland.freedumbytes.reflect;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


public interface Parameterizable
{
  /**
   * Returns the actual type argument with given index of a generic super class or a generic interface in the supplied class.
   *
   * @param theClass
   *          The class to search.
   * @param matchClass
   *          The generic super class or the generic interface to match with.
   * @param index
   *          The index of the actual type argument of the parameterized type.
   *
   * @return The actual type argument with given index.
   */
  default Class<?> actualTypeArgument(Class<?> theClass, Class<?> matchClass, Integer index)
  {
    return getActualTypeArgument(theClass, matchClass, index);
  }


  /**
   * Returns the actual type argument with given index of a generic super class or a generic interface in the supplied class.
   *
   * @param theClass
   *          The class to search.
   * @param matchClass
   *          The generic super class or the generic interface to match with.
   * @param index
   *          The index of the actual type argument of the parameterized type.
   *
   * @return The actual type argument with given index.
   */
  static Class<?> getActualTypeArgument(Class<?> theClass, Class<?> matchClass, Integer index)
  {
    return findParameterizedType(theClass, matchClass).get(index);
  }


  /**
   * Find the parameterized type of a generic super class or a generic interface in the supplied class.
   *
   * @param theClass
   *          The class to search.
   * @param matchClass
   *          The generic super class or the generic interface to match with.
   *
   * @return The {@link ParameterMatchMix} helper to find the correct actual type argument for every index.
   */
  static ParameterMatchMix findParameterizedType(Class<?> theClass, Class<?> matchClass)
  {
    ParameterMatchMix parameterMatchMix = new ParameterMatchMix();

    visitClass(theClass, matchClass, parameterMatchMix);

    return parameterMatchMix;
  }


  static boolean visitClass(Class<?> theClass, Class<?> matchClass, ParameterMatchMix parameterMatchMix)
  {
    if (theClass == null)
    {
      return false;
    }

    if (theClass.getTypeName().equals(matchClass.getTypeName()))
    {
      return true;
    }

    for (Class<?> anInterface : theClass.getInterfaces())
    {
      if (visitClass(anInterface, matchClass, parameterMatchMix))
      {
        mixAndMatch(theClass, anInterface, parameterMatchMix);

        return true;
      }
    }

    if (visitClass(theClass.getSuperclass(), matchClass, parameterMatchMix))
    {
      Type theSuperclass = theClass.getGenericSuperclass();

      if (theSuperclass instanceof ParameterizedType)
      {
        ParameterizedType parameterizedType = (ParameterizedType) theSuperclass;

        parameterMatchMix.add(theClass.getSuperclass().getTypeParameters(), parameterizedType);
      }

      return true;
    }
    else
    {
      return false;
    }
  }


  static void mixAndMatch(Class<?> theClass, Class<?> anInterface, ParameterMatchMix parameterMatchMix)
  {
    ParameterizedType matchParameterizedType = null;

    for (Type interfaceType : theClass.getGenericInterfaces())
    {
      if (interfaceType instanceof ParameterizedType)
      {
        ParameterizedType parameterizedType = (ParameterizedType) interfaceType;

        if (parameterizedType.getRawType().getTypeName().equals(anInterface.getTypeName()))
        {
          matchParameterizedType = parameterizedType;
        }
      }
    }

    parameterMatchMix.add(anInterface.getTypeParameters(), matchParameterizedType);
  }
}
