package nl.demon.shadowland.freedumbytes.hibernate.repository;


import java.io.Serializable;


/**
 * An interface shared by all business data access objects.
 *
 * <p>
 * All {@code CRUD} (create, read, update, delete) basic data access operations are isolated in this interface and shared across all {@code DAO} implementations.
 * </p>
 *
 * @param <E>
 *          The entity type.
 * @param <K>
 *          The primary key type.
 */
public interface GenericDao<E extends Identifiable<K>, K extends Serializable>
{
  /**
   * Persist the specified entity to the database, returning the newly persisted entity.
   *
   * @param entity
   *          The entity to persist.
   *
   * @return The created entity.
   *
   * @see javax.persistence.EntityManager#persist(Object)
   */
  E create(E entity);


  /**
   * Finds an entity by its primary key.
   *
   * @param id
   *          The primary key of the entity.
   *
   * @return The entity if it exists for {@code id} or {@code null} if entity does not exist.
   *
   * @see javax.persistence.EntityManager#find(Class, Object)
   */
  E read(K id);


  /**
   * Merge the specified entity to the database, returning the updated entity.
   *
   * @param entity
   *          The entity to merge.
   *
   * @return The updated entity.
   *
   * @see javax.persistence.EntityManager#merge(Object)
   */
  E update(E entity);


  /**
   * Remove the specified entity from the database.
   *
   * @param entity
   *          The entity to remove.
   *
   * @see javax.persistence.EntityManager#remove(Object)
   */
  void delete(E entity);


  /**
   * Check if a persisted record exits for the given {@code id}.
   *
   * @param id
   *          The primary key of the entity.
   *
   * @return {@code True} if the entity exists for {@code id} or {@code false} if entity does not exist.
   */
  boolean exists(K id);


  /**
   * Check if the given entity is associated with the current persistence context.
   *
   * @param entity
   *          The entity to check.
   *
   * @return {@code True} if the entity is managed or {@code false} if not.
   *
   * @see javax.persistence.EntityManager#contains(Object)
   */
  boolean isManaged(E entity);
}
