package nl.demon.shadowland.freedumbytes.hibernate.repository;


/**
 * Order of {@link Identifiable} generic parameters.
 */
enum IdentifiableGenericParams
{
  KEY;
}
