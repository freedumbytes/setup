package nl.demon.shadowland.freedumbytes.hibernate.id.uuid;


import java.util.UUID;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.UUIDGenerationStrategy;

import nl.demon.shadowland.freedumbytes.uuid.UUIDGenerator;


/**
 * Implements a type 5 (name based) {@code UUID} generation strategy as defined by the {@link UUIDGenerator#type5NameUUIDFromBytes(byte[])} method.
 */
public class CustomVersionFiveStrategy implements UUIDGenerationStrategy
{
  private static final long serialVersionUID = 1L;

  private static final int UUID_TYPE = 5;
  private static final String CLASS_LOAD_TIMESTAMP = String.valueOf(System.currentTimeMillis());


  /**
   * A type 5 (name based) strategy.
   *
   * @return The used {@code UUID} type.
   */
  @Override()
  public int getGeneratedVersion()
  {
    return UUID_TYPE;
  }


  /**
   * Delegates to {@link UUIDGenerator#type5NameUUIDFromBytes(byte[])}.
   *
   * @param session
   *          The session asking for the generation.
   *
   * @return The generated {@code UUID}.
   */
  @Override()
  public UUID generateUUID(SharedSessionContractImplementor session)
  {
    String random = UUID.randomUUID().toString();

    return UUIDGenerator.type5NameUUIDFromBytes(CLASS_LOAD_TIMESTAMP.concat(random).getBytes());
  }
}
