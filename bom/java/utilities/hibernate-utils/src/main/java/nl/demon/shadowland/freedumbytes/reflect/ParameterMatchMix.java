package nl.demon.shadowland.freedumbytes.reflect;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;


/**
 * How to get a class instance of a generic parameter type with given index.
 */
public class ParameterMatchMix
{
  private List<TypeVariable<?>[]> typeVariablesList = new ArrayList<>();
  private List<ParameterizedType> parameterizedTypeList = new ArrayList<>();


  /**
   * Supply the two items of a generic super class or a generic interface required to track back the correct {@code Class<?>}.
   *
   * @param typeVariables
   *          The {@code TypeVariable<?>[]} linked to {@code ParameterizedType}.
   * @param parameterizedType
   *          The {@code ParameterizedType} linked to {@code TypeVariable<?>[]}.
   */
  public void add(TypeVariable<?>[] typeVariables, ParameterizedType parameterizedType)
  {
    typeVariablesList.add(typeVariables);
    parameterizedTypeList.add(parameterizedType);
  }


  /**
   * Determine the correct {@code Class<?>} of a generic super class or a generic interface parameter type with given index.
   *
   * @param index
   *          The index of the parameterized type of a generic super class or a generic interface.
   *
   * @return The correct {@code Class<?>}.
   */
  public Class<?> get(Integer index)
  {
    if (parameterizedTypeList.get(0) == null)
    {
      return null;
    }

    Type type = parameterizedTypeList.get(0).getActualTypeArguments()[index];

    if (type instanceof Class<?>)
    {
      return (Class<?>) type;
    }
    else
    {
      return backTrack(1, type.getTypeName());
    }
  }


  private Class<?> backTrack(Integer level, String typeName)
  {
    Integer typeVariableMatchIndex = null;

    for (int typeVariableIndex = 0; typeVariableIndex < typeVariablesList.get(level).length; typeVariableIndex++)
    {
      TypeVariable<?> typeVariable = typeVariablesList.get(level)[typeVariableIndex];

      if (typeName.equals(typeVariable.getTypeName()))
      {
        typeVariableMatchIndex = typeVariableIndex;
      }
    }

    Type type = parameterizedTypeList.get(level).getActualTypeArguments()[typeVariableMatchIndex];

    if (type instanceof Class<?>)
    {
      return (Class<?>) type;
    }
    else
    {
      return backTrack(level + 1, type.getTypeName());
    }
  }
}
