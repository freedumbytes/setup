package nl.demon.shadowland.freedumbytes.uuid;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;


public final class UUIDGenerator
{
  private static final UUIDGenerator theInstance = new UUIDGenerator();
  private static final String TYPE_5_HASHING_ALGORITHM = "SHA-1";


  private UUIDGenerator()
  {
  }


  /**
   * Static factory to retrieve a type 5 (name based) {@code UUID} based on the specified byte array.
   *
   * @param name
   *          A byte array to be used to construct a {@code UUID}
   *
   * @return A {@code UUID} generated from the specified array
   */
  public static UUID type5NameUUIDFromBytes(byte[] name)
  {
    return theInstance.generateType5(name);
  }


  private UUID generateType5(byte[] name)
  {
    MessageDigest md;

    try
    {
      md = MessageDigest.getInstance(TYPE_5_HASHING_ALGORITHM); // NOSONAR - UUIDGenerator type 5 (name based).
    }
    catch (NoSuchAlgorithmException e)
    {
      String message = String.format("%s not supported", TYPE_5_HASHING_ALGORITHM);

      throw new InternalError(message, e);
    }

    byte[] hashBytes = md.digest(name);

    setVersion5(hashBytes);
    setVariant2(hashBytes);

    return createUUID(hashBytes);
  }


  private void setVersion5(byte[] hashBytes)
  {
    hashBytes[6] &= 0x0f;
    hashBytes[6] |= 0x50;
  }


  private void setVariant2(byte[] hashBytes)
  {
    hashBytes[8] &= 0x3f;
    hashBytes[8] |= 0x80;
  }


  private UUID createUUID(byte[] hashBytes)
  {
    long msb = 0;
    long lsb = 0;

    for (int i = 0; i < 8; i++)
    {
      msb = (msb << 8) | (hashBytes[i] & 0xff);
    }

    for (int i = 8; i < 16; i++)
    {
      lsb = (lsb << 8) | (hashBytes[i] & 0xff);
    }

    return new UUID(msb, lsb);
  }
}
