package nl.demon.shadowland.freedumbytes.hibernate.id.uuid;


import java.util.UUID;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.UUIDGenerationStrategy;


/**
 * Implements a type 3 (name based) {@code UUID} generation strategy as defined by the {@link UUID#nameUUIDFromBytes(byte[])} method.
 */
public class CustomVersionThreeStrategy implements UUIDGenerationStrategy
{
  private static final long serialVersionUID = 1L;

  private static final int UUID_TYPE = 3;
  private static final String CLASS_LOAD_TIMESTAMP = String.valueOf(System.currentTimeMillis());


  /**
   * A type 3 (name based) strategy.
   *
   * @return The used {@code UUID} type.
   */
  @Override()
  public int getGeneratedVersion()
  {
    return UUID_TYPE;
  }


  /**
   * Delegates to {@link UUID#nameUUIDFromBytes(byte[])}.
   *
   * @param session
   *          The session asking for the generation.
   *
   * @return The generated {@code UUID}.
   */
  @Override()
  public UUID generateUUID(SharedSessionContractImplementor session)
  {
    String random = UUID.randomUUID().toString();

    return UUID.nameUUIDFromBytes(CLASS_LOAD_TIMESTAMP.concat(random).getBytes());
  }
}
