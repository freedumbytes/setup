package nl.demon.shadowland.freedumbytes.hibernate.repository;


/**
 * Order of {@link JpaDao} generic parameters.
 */
enum JpaDaoGenericParams
{
  ENTITY, KEY;
}
