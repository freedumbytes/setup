package nl.demon.shadowland.freedumbytes.hibernate.repository;


import java.io.Serializable;

import nl.demon.shadowland.freedumbytes.reflect.Parameterizable;


/**
 * Interface to mark objects that are identifiable by an {@code ID} of any {@link Serializable} type.
 *
 * @param <K>
 *          The identifier type.
 */
public interface Identifiable<K extends Serializable> extends Parameterizable
{
  /**
   * Returns the {@code id} identifying the object.
   *
   * @return The identifier or {@code null} if not available.
   */
  K getId();


  /**
   * Returns the identifier type.
   *
   * @return The identifier type.
   */
  @SuppressWarnings("unchecked")
  default Class<K> identifierType()
  {
    return (Class<K>) actualTypeArgument(getClass(), Identifiable.class, IdentifiableGenericParams.KEY.ordinal());
  }
}
