Grouping Hibernate Utilities

 A convenience project that declares a set of Hibernate Utilities. It currently contains:

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/uuid/UUIDGenerator.html} UUIDGenerator}} a static factory to retrieve a type 5 (name based) <<<UUID>>> based on the specified byte array.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/hibernate/id/uuid/CustomVersionFiveStrategy.html} CustomVersionFiveStrategy}} a type 5 (name based) <<<UUID>>> generation strategy.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/hibernate/id/uuid/CustomVersionThreeStrategy.html} CustomVersionThreeStrategy}} a type 3 (name based) <<<UUID>>> generation strategy.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/GenericDao.html} GenericDao}} all <<<CRUD>>> (create, read, update, delete) basic data access operations are isolated in this interface and shared across all <<<DAO>>> implementations.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/JpaDao.html} JpaDao}} a <<<JPA>>> implementation of <<<GenericDao>>>.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/Identifiable.html} Identifiable}} an interface to mark objects that are identifiable by an <<<ID>>> of any <<<Serializable>>> type.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/IdentifiableGenericParams.html} IdentifiableGenericParams}} order of <<<Identifiable>>> generic parameters.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/hibernate/repository/JpaDaoGenericParams.html} JpaDaoGenericParams}} order of <<<JpaDao>>> generic parameters.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/reflect/Parameterizable.html} Parameterizable}} find the parameterized type of a generic super class or a generic interface in the supplied class.

 * {{{./apidocs/nl/demon/shadowland/freedumbytes/reflect/ParameterMatchMix.html} ParameterMatchMix}} how to get a class instance of a generic parameter type with given index.

 []

 To include Hibernate Utilities add this project as a dependency:

+--
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>${project.artifactId}</artifactId>
      <version>${project.version}</version>
    </dependency>
+--
