package nl.demon.shadowland.freedumbytes.hibernate.repository.sample.identifier;


import lombok.Getter;

import nl.demon.shadowland.freedumbytes.hibernate.repository.Identifiable;


@Getter
public class User implements Identifiable<String>
{
  private String id;
}
