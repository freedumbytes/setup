package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import nl.demon.shadowland.freedumbytes.reflect.Parameterizable;


/**
 * Example that is mostly used.
 *
 * @param <FRST>
 * @param <SCND>
 */
public class GenericSample<FRST, SCND> implements Parameterizable
{
  @SuppressWarnings("unchecked")
  public Class<FRST> primaryGenericParameterTypeOfGenericSample()
  {
    return (Class<FRST>) genericParameterTypeOfGenericSample(0);
  }


  @SuppressWarnings("unchecked")
  public Class<SCND> secondaryGenericParameterTypeOfGenericSample()
  {
    return (Class<SCND>) genericParameterTypeOfGenericSample(1);
  }


  public Type genericParameterTypeOfGenericSample(int index)
  {
    ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();

    return genericSuperclass.getActualTypeArguments()[index];
  }
}
