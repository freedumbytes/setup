package nl.demon.shadowland.freedumbytes.hibernate.test;


import java.util.function.Consumer;

import javax.persistence.EntityManager;


/**
 * {@code JPA} transaction function without return value.
 */
@FunctionalInterface()
public interface JpaTransactionVoidFunction extends Consumer<EntityManager>
{
  /**
   * Before transaction completion function.
   */
  default void beforeTransactionCompletion()
  {
  }


  /**
   * After transaction completion function.
   */
  default void afterTransactionCompletion()
  {
  }
}
