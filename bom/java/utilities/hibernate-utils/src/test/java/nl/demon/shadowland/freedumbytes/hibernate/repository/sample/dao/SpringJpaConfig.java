package nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao;


import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.extern.slf4j.Slf4j;


@Configuration
@ComponentScan(basePackages = { "nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao" })
@PropertySource(value = "classpath:sample/h2-jdbc.properties")
@PropertySource(value = "classpath:sample/h2-hibernate.properties", ignoreResourceNotFound = false)
@Slf4j
public class SpringJpaConfig
{
  @Value("${jdbc.driverClassName}")
  private String jdbcDriverClassName;

  @Value("${jdbc.url}")
  private String jdbcUrl;

  @Value("${jdbc.username}")
  private String jdbcUsername;

  @Value("${jdbc.password}")
  private String jdbcPassword;

  @Value("${hibernate.dialect}")
  private String hibernateDialect;

  @Value("${hibernate.hbm2ddl.auto:}")
  private String hibernateHbm2ddlAuto;

  @Value("${hibernate.show_sql:false}")
  private String hibernateShowSql;

  @Value("${hibernate.format_sql:false}")
  private String hibernateFormatSql;

  @Value("${hibernate.bytecode.provider:javassist}")
  private String hibernateBytecodeProvider;

  @Value("${hibernate.cache.use_query_cache:false}")
  private String hibernateCacheUseQueryCache;

  @Value("${hibernate.cache.use_second_level_cache:false}")
  private String hibernateCacheUseSecondLevelCache;


  public SpringJpaConfig()
  {
    log.info("Run Spring JPA wiring for DAO tests.");
  }


  @Bean
  public PlatformTransactionManager transactionManager()
  {
    return new JpaTransactionManager(entityManagerFactory());
  }


  @Bean
  public EntityManagerFactory entityManagerFactory()
  {
    HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();

    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setPersistenceUnitName("emf-sample");
    factory.setJpaVendorAdapter(jpaVendorAdapter);

    DataSource dataSource = dataSource();
    factory.setDataSource(dataSource);

    factory.setPackagesToScan(new String[] { "nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao" });
    factory.setJpaProperties(jpaProperties());

    factory.afterPropertiesSet();

    return factory.getObject();
  }


  @Bean
  public DataSource dataSource()
  {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName(jdbcDriverClassName);
    dataSource.setUrl(jdbcUrl);
    dataSource.setUsername(jdbcUsername);
    dataSource.setPassword(jdbcPassword);

    return dataSource;
  }


  protected Properties jpaProperties()
  {
    Properties jpaProperties = new Properties();

    jpaProperties.put(Environment.DIALECT, hibernateDialect);
    jpaProperties.put(Environment.HBM2DDL_AUTO, hibernateHbm2ddlAuto);
    jpaProperties.put(Environment.SHOW_SQL, hibernateShowSql);
    jpaProperties.put(Environment.FORMAT_SQL, hibernateFormatSql);
    jpaProperties.put(Environment.BYTECODE_PROVIDER, hibernateBytecodeProvider);
    jpaProperties.put(Environment.USE_QUERY_CACHE, hibernateCacheUseQueryCache);
    jpaProperties.put(Environment.USE_SECOND_LEVEL_CACHE, hibernateCacheUseSecondLevelCache);

    return jpaProperties;
  }
}
