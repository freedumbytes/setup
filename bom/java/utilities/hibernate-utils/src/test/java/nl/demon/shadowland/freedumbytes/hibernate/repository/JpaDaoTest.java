package nl.demon.shadowland.freedumbytes.hibernate.repository;


import static org.assertj.core.api.Assertions.assertThat;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.PlatformTransactionManager;

import nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao.SpringJpaConfig;
import nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao.User;
import nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao.UserDao;
import nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao.UserJpaDao;
import nl.demon.shadowland.freedumbytes.hibernate.test.JpaTransactional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SpringJpaConfig.class })
@Transactional(TxType.REQUIRED)
public class JpaDaoTest implements JpaTransactional
{
  private static final Long UNKNOWN_ID = -1L;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private PlatformTransactionManager transactionManager;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  @PersistenceContext
  private EntityManager entityManager;

  @Resource(name = "userDao")
  private UserDao userDao;


  @Test
  public void springWiring()
  {
    assertThat(applicationContext).isNotNull();
    assertThat(transactionManager).isNotNull();
    assertThat(entityManagerFactory).isNotNull();
    assertThat(entityManager).isNotNull();

    assertThat(userDao).isNotNull() //
        .isExactlyInstanceOf(UserJpaDao.class);
  }


  @Test
  public void createDefaultTestTransactionRollback()
  {
    assertThat(TestTransaction.isActive()).isTrue();
    assertThat(TestTransaction.isFlaggedForRollback()).isTrue();

    User user = new User("刀", true);
    user = userDao.create(user);
    Long id = user.getId();

    assertThat(id).isNotNull();

    doInJpa(entityManagerFactory, entityManager ->
    {
      User found = entityManager.find(User.class, id);

      assertThat(found).isNull();
    });

    assertThat(entityManager.contains(user)).isTrue();

    TestTransaction.end();

    assertThat(TestTransaction.isActive()).isFalse();
    assertThat(entityManager.contains(user)).isFalse();

    TestTransaction.start();

    assertThat(TestTransaction.isActive()).isTrue();
    assertThat(entityManager.contains(user)).isFalse();

    doInJpa(entityManagerFactory, entityManager ->
    {
      User found = entityManager.find(User.class, id);

      assertThat(found).isNull();
    });
  }


  @Test
  public void createTestTransactionFlagForCommit()
  {
    assertThat(TestTransaction.isActive()).isTrue();
    assertThat(TestTransaction.isFlaggedForRollback()).isTrue();

    User user = new User("刀", true);
    user = userDao.create(user);
    Long id = user.getId();

    assertThat(id).isNotNull();

    doInJpa(entityManagerFactory, entityManager ->
    {
      User found = entityManager.find(User.class, id);

      assertThat(found).isNull();
    });

    TestTransaction.flagForCommit();

    assertThat(TestTransaction.isFlaggedForRollback()).isFalse();
    assertThat(entityManager.contains(user)).isTrue();

    TestTransaction.end();

    assertThat(TestTransaction.isActive()).isFalse();
    assertThat(entityManager.contains(user)).isFalse();

    TestTransaction.start();

    assertThat(TestTransaction.isActive()).isTrue();
    assertThat(entityManager.contains(user)).isFalse();

    doInJpa(entityManagerFactory, entityManager ->
    {
      User found = entityManager.find(User.class, id);

      assertThat(found).isNotNull();
      assertThat(found.getName()).isEqualTo("刀");
      assertThat(found.getRegistered()).isTrue();
    });
  }


  @Test
  public void read()
  {
    Long id = doInJpa(entityManagerFactory, entityManager ->
    {
      User newUser = new User("dao", true);
      entityManager.persist(newUser);

      return newUser.getId();
    });

    User found = userDao.read(id);

    assertThat(found).isNotNull();
    assertThat(found.getName()).isEqualTo("dao");
    assertThat(found.getRegistered()).isTrue();

    found = userDao.read(UNKNOWN_ID);

    assertThat(found).isNull();
  }


  @Test
  @Commit
  public void update()
  {
    Long id = doInJpa(entityManagerFactory, entityManager ->
    {
      User newUser = new User("dao", true);
      entityManager.persist(newUser);

      return newUser.getId();
    });

    User user = userDao.read(id);

    assertThat(user).isNotNull();
    assertThat(user.getName()).isEqualTo("dao");
    assertThat(user.getRegistered()).isTrue();

    user.setName("刀");
    userDao.update(user);

    TestTransaction.end();

    doInJpa(entityManagerFactory, entityManager ->
    {
      User found = entityManager.find(User.class, id);

      assertThat(found).isNotNull();
      assertThat(found.getName()).isEqualTo("刀");
      assertThat(found.getRegistered()).isTrue();
    });
  }


  @Test
  @Commit
  public void deleteManaged()
  {
    Long id = doInJpa(entityManagerFactory, entityManager ->
    {
      User newUser = new User("dao", true);
      entityManager.persist(newUser);

      return newUser.getId();
    });

    User user = userDao.read(id);

    assertThat(user).isNotNull();
    assertThat(user.getName()).isEqualTo("dao");
    assertThat(user.getRegistered()).isTrue();
    assertThat(userDao.isManaged(user)).isTrue();

    userDao.delete(user);

    TestTransaction.end();

    doInJpa(entityManagerFactory, entityManager ->
    {
      User found = entityManager.find(User.class, id);

      assertThat(found).isNull();
    });
  }


  @Test
  @Commit
  public void deleteDetached()
  {
    User user = doInJpa(entityManagerFactory, entityManager ->
    {
      User newUser = new User("dao", true);
      entityManager.persist(newUser);

      return newUser;
    });

    assertThat(user).isNotNull();
    assertThat(user.getName()).isEqualTo("dao");
    assertThat(user.getRegistered()).isTrue();
    assertThat(userDao.isManaged(user)).isFalse();

    userDao.delete(user);

    TestTransaction.end();

    doInJpa(entityManagerFactory, entityManager ->
    {
      User found = entityManager.find(User.class, user.getId());

      assertThat(found).isNull();
    });
  }


  @Test
  public void exists()
  {
    User existingUser = doInJpa(entityManagerFactory, entityManager ->
    {
      User newUser = new User("dao", true);
      entityManager.persist(newUser);

      return newUser;
    });

    assertThat(entityManager.contains(existingUser)).isFalse();
    assertThat(userDao.exists(existingUser.getId())).isTrue();
    assertThat(userDao.exists(UNKNOWN_ID)).isFalse();
  }


  @Test
  public void isManaged()
  {
    User existingUser = doInJpa(entityManagerFactory, entityManager ->
    {
      User newUser = new User("dao", true);
      entityManager.persist(newUser);

      return newUser;
    });

    assertThat(userDao.isManaged(existingUser)).isFalse();

    existingUser = userDao.read(existingUser.getId());

    assertThat(userDao.isManaged(existingUser)).isTrue();
  }
}
