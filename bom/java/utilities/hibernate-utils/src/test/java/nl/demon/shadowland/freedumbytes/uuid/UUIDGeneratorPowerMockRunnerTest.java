package nl.demon.shadowland.freedumbytes.uuid;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ UUIDGenerator.class })
public class UUIDGeneratorPowerMockRunnerTest
{
  @Test(expected = InternalError.class)
  public void type5UUID_invalidHashAlgorithm_expected() throws Exception
  {
    mockStatic(MessageDigest.class);
    when(MessageDigest.getInstance("SHA-1")).thenThrow(NoSuchAlgorithmException.class);

    UUIDGenerator.type5NameUUIDFromBytes("namespace + name.".getBytes());
  }


  @Test()
  public void type5UUID_invalidHashAlgorithm_assertThrows() throws NoSuchAlgorithmException
  {
    mockStatic(MessageDigest.class);
    when(MessageDigest.getInstance("SHA-1")).thenThrow(NoSuchAlgorithmException.class);

    byte[] name = "namespace + name.".getBytes();
    InternalError error = assertThrows(InternalError.class, () -> UUIDGenerator.type5NameUUIDFromBytes(name));
    assertThat(error).hasMessage("SHA-1 not supported");
  }
}
