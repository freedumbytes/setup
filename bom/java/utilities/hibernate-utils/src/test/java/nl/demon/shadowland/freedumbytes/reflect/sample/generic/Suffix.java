package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.io.Serializable;

import nl.demon.shadowland.freedumbytes.reflect.Parameterizable;


public interface Suffix<SFFX extends Serializable> extends Parameterizable
{
  @SuppressWarnings("unchecked")
  default Class<SFFX> suffix()
  {
    return (Class<SFFX>) actualTypeArgument(getClass(), Suffix.class, 0);
  }
}
