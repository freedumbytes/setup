package nl.demon.shadowland.freedumbytes.hibernate.repository.sample.identifier;


import lombok.Getter;

import nl.demon.shadowland.freedumbytes.hibernate.repository.Identifiable;


@Getter
public class Invoice implements Identifiable<Long>
{
  private Long id;
}
