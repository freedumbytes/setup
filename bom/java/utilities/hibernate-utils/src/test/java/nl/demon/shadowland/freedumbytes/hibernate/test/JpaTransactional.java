package nl.demon.shadowland.freedumbytes.hibernate.test;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;


/**
 * Enroll a given set of operations in a database transaction.
 *
 * @see <a href="https://vladmihalcea.com/mysql-metadata-locking-and-database-transaction-ending/" target="_blank" rel="noopener noreferrer">MySQL metadata locking and database transaction ending posted by Vlad Mihalcea</a>
 * @see <a href="https://vladmihalcea.com/tutorials/hibernate/" target="_blank" rel="noopener noreferrer">High-Performance Hibernate Tutorial posted by Vlad Mihalcea</a>
 */
public interface JpaTransactional
{
  /**
   * Execute function in a {@code JPA} transaction without return value.
   *
   * @param entityManagerFactory
   *          The entityManagerFactory.
   * @param function
   *          The transaction function.
   */
  default void doInJpa(EntityManagerFactory entityManagerFactory, JpaTransactionVoidFunction function)
  {
    doInJpa(entityManagerFactory, wrapInJpaTransactionFunction(function));
  }


  /**
   * Wrap {@link JpaTransactionVoidFunction} in a {@link JpaTransactionFunction}{@code <Void>} for code reuse.
   *
   * @param function
   *          The transaction function.
   *
   * @return The wrapped transaction function.
   */
  default JpaTransactionFunction<Void> wrapInJpaTransactionFunction(JpaTransactionVoidFunction function)
  {
    return new JpaTransactionFunction<Void>()
    {
      @Override()
      public Void apply(EntityManager entityManager)
      {
        function.accept(entityManager);

        return null;
      }


      @Override()
      public void beforeTransactionCompletion()
      {
        function.beforeTransactionCompletion();
      }


      @Override()
      public void afterTransactionCompletion()
      {
        function.afterTransactionCompletion();
      }
    };
  }


  /**
   * Execute function in a {@code JPA} transaction.
   *
   * @param entityManagerFactory
   *          The entityManagerFactory.
   * @param function
   *          The transaction function.
   *
   * @return The function result.
   */
  default <T> T doInJpa(EntityManagerFactory entityManagerFactory, JpaTransactionFunction<T> function)
  {
    T result = null;
    EntityManager entityManager = null;
    EntityTransaction transaction = null;

    try
    {
      entityManager = entityManagerFactory.createEntityManager();

      function.beforeTransactionCompletion();

      transaction = entityManager.getTransaction();
      transaction.begin();

      result = function.apply(entityManager);

      transaction.commit();
    }
    catch (RuntimeException e)
    {
      if (transaction != null && transaction.isActive())
      {
        transaction.rollback();
      }

      throw e;
    }
    finally
    {
      function.afterTransactionCompletion();

      if (entityManager != null)
      {
        entityManager.close();
      }
    }

    return result;
  }
}
