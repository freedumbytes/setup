package nl.demon.shadowland.freedumbytes.hibernate.id.uuid;


import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;


public class CustomVersionThreeStrategyTest extends CustomVersionStrategyTest
{
  private static final int UUID_TYPE = 3;
  private static final Pattern UUID_PATTERN = Pattern.compile("(?i)^[0-9a-f]{8}-[0-9a-f]{4}-[3][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$");

  private CustomVersionThreeStrategy customStrategy;


  @Before()
  public void init()
  {
    customStrategy = new CustomVersionThreeStrategy();
  }


  @Test()
  public void generatedVersion()
  {
    assertEquals(UUID_TYPE, customStrategy.getGeneratedVersion());
  }


  @Test()
  public void generateUUID()
  {
    verifyUUID(customStrategy.generateUUID(null), UUID_TYPE, UUID_PATTERN);
  }
}
