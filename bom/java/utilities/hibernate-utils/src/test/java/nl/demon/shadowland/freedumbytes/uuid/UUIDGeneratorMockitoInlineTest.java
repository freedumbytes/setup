package nl.demon.shadowland.freedumbytes.uuid;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;


public class UUIDGeneratorMockitoInlineTest
{
  @Test(expected = InternalError.class)
  public void type5UUID_invalidHashAlgorithm_expected() throws Exception
  {
    try (MockedStatic<MessageDigest> mockedMessageDigest = Mockito.mockStatic(MessageDigest.class))
    {
      mockedMessageDigest.when(() -> MessageDigest.getInstance("SHA-1")).thenThrow(NoSuchAlgorithmException.class);

      UUIDGenerator.type5NameUUIDFromBytes("namespace + name.".getBytes());
    }
  }


  @Test()
  public void type5UUID_invalidHashAlgorithm_assertThrows() throws NoSuchAlgorithmException
  {
    try (MockedStatic<MessageDigest> mockedMessageDigest = Mockito.mockStatic(MessageDigest.class))
    {
      mockedMessageDigest.when(() -> MessageDigest.getInstance("SHA-1")).thenThrow(NoSuchAlgorithmException.class);

      byte[] name = "namespace + name.".getBytes();
      InternalError error = assertThrows(InternalError.class, () -> UUIDGenerator.type5NameUUIDFromBytes(name));
      assertThat(error).hasMessage("SHA-1 not supported");
    }
  }
}
