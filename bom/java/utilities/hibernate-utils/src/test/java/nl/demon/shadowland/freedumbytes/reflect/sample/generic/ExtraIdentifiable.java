package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.io.Serializable;

import nl.demon.shadowland.freedumbytes.reflect.Parameterizable;


public interface ExtraIdentifiable<XTR extends Serializable> extends Parameterizable
{
  XTR getExtraId();


  @SuppressWarnings("unchecked")
  default Class<XTR> extraIdentifierType()
  {
    return (Class<XTR>) actualTypeArgument(getClass(), ExtraIdentifiable.class, 0);
  }
}
