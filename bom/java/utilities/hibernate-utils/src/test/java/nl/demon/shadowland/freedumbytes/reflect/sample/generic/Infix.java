package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


public interface Infix extends Affix<String>
{
  default Class<?> infix()
  {
    return (Class<?>) actualTypeArgument(this.getClass(), Infix.class, 0);
  }
}
