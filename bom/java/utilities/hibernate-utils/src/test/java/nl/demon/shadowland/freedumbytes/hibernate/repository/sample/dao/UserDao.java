package nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao;


import nl.demon.shadowland.freedumbytes.hibernate.repository.GenericDao;


public interface UserDao extends GenericDao<User, Long>
{
}
