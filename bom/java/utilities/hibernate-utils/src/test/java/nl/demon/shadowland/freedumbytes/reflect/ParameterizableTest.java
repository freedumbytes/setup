package nl.demon.shadowland.freedumbytes.reflect;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

import org.junit.Test;

import nl.demon.shadowland.freedumbytes.reflect.sample.generic.Combination;
import nl.demon.shadowland.freedumbytes.reflect.sample.generic.GenericSample;
import nl.demon.shadowland.freedumbytes.reflect.sample.generic.Hidden;
import nl.demon.shadowland.freedumbytes.reflect.sample.generic.HideSample;
import nl.demon.shadowland.freedumbytes.reflect.sample.generic.Invoice;
import nl.demon.shadowland.freedumbytes.reflect.sample.generic.MaskSample;
import nl.demon.shadowland.freedumbytes.reflect.sample.generic.Swapped;
import nl.demon.shadowland.freedumbytes.reflect.sample.generic.SwappedChild;


public class ParameterizableTest
{
  @Test()
  public void invoiceSuper()
  {
    Invoice invoice = new Invoice();

    assertThat(invoice.superIdentifierType()).isEqualTo(Long.class);
  }


  @Test()
  public void invoiceSub()
  {
    Invoice invoice = new Invoice();

    assertThat(invoice.subIdentifierType()).isEqualTo(String.class);
  }


  @Test()
  public void invoiceExtra()
  {
    Invoice invoice = new Invoice();

    assertThat(invoice.extraIdentifierType()).isEqualTo(Number.class);
  }


  @Test()
  public void combinationSuffix()
  {
    Combination combination = new Combination();

    assertThat(combination.suffix()).isEqualTo(String.class);
  }


  @Test()
  public void combinationPostfix()
  {
    Combination combination = new Combination();

    assertThat(combination.postfix()).isEqualTo(String.class);
  }


  @Test()
  public void combinationAffix()
  {
    Combination combination = new Combination();

    assertThat(combination.affix()).isEqualTo(String.class);
  }


  @Test()
  public void combinationInfix()
  {
    Combination combination = new Combination();

    assertThat(combination.infix()).isNull();
  }


  @Test()
  public void combinationPrefix()
  {
    Combination combination = new Combination();

    assertThat(combination.prefix()).isEqualTo(Long.class);
  }


  @Test()
  public void hiddenFailure()
  {
    Hidden secondHidden = new Hidden();

    assertThat(secondHidden.primaryGenericParameterTypeOfGenericSample()).isEqualTo(Number.class);

    ArrayIndexOutOfBoundsException exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> secondHidden.secondaryGenericParameterTypeOfGenericSample());
    assertThat(exception).hasMessage("1");
  }


  @Test()
  public void hiddenSuccess()
  {
    Hidden secondHidden = new Hidden();

    assertThat(secondHidden.actualTypeArgument(Hidden.class, HideSample.class, 0)).isEqualTo(Number.class);

    assertThat(secondHidden.actualTypeArgument(Hidden.class, GenericSample.class, 0)).isEqualTo(Number.class);
    assertThat(secondHidden.actualTypeArgument(Hidden.class, GenericSample.class, 1)).isEqualTo(String.class);
  }


  @Test()
  public void maskedFailure()
  {
    Swapped swapped = new Swapped();

    assertThat(swapped.primaryGenericParameterTypeOfGenericSample()).isNotEqualTo(Long.class);
    assertThat(swapped.secondaryGenericParameterTypeOfGenericSample()).isNotEqualTo(Number.class);
  }


  @Test()
  public void maskedSuccess()
  {
    Swapped swapped = new Swapped();

    assertThat(swapped.actualTypeArgument(Swapped.class, MaskSample.class, 0)).isEqualTo(Number.class);
    assertThat(swapped.actualTypeArgument(Swapped.class, MaskSample.class, 1)).isEqualTo(Long.class);

    assertThat(swapped.actualTypeArgument(Swapped.class, GenericSample.class, 0)).isEqualTo(Long.class);
    assertThat(swapped.actualTypeArgument(Swapped.class, GenericSample.class, 1)).isEqualTo(Number.class);
  }


  @Test()
  public void maskedInvalidLowerBound()
  {
    Swapped swapped = new Swapped();

    swapped.actualTypeArgument(Swapped.class, GenericSample.class, 0);

    ArrayIndexOutOfBoundsException exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> swapped.actualTypeArgument(Swapped.class, GenericSample.class, -1));
    assertThat(exception).hasMessage("-1");
  }


  @Test()
  public void maskedInvalidUpperBound()
  {
    Swapped swapped = new Swapped();

    swapped.actualTypeArgument(Swapped.class, GenericSample.class, 0);
    swapped.actualTypeArgument(Swapped.class, GenericSample.class, 1);

    ArrayIndexOutOfBoundsException exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> swapped.actualTypeArgument(Swapped.class, GenericSample.class, 2));
    assertThat(exception).hasMessage("2");
  }


  @Test()
  public void subclassSuccess()
  {
    SwappedChild child = new SwappedChild();

    assertThat(child.actualTypeArgument(child.getClass(), MaskSample.class, 0)).isEqualTo(Number.class);
    assertThat(child.actualTypeArgument(child.getClass(), MaskSample.class, 1)).isEqualTo(Long.class);

    assertThat(child.actualTypeArgument(child.getClass(), GenericSample.class, 0)).isEqualTo(Long.class);
    assertThat(child.actualTypeArgument(child.getClass(), GenericSample.class, 1)).isEqualTo(Number.class);

    assertThat(child.actualTypeArgument(SwappedChild.class, MaskSample.class, 0)).isEqualTo(Number.class);
    assertThat(child.actualTypeArgument(SwappedChild.class, MaskSample.class, 1)).isEqualTo(Long.class);

    assertThat(child.actualTypeArgument(SwappedChild.class, GenericSample.class, 0)).isEqualTo(Long.class);
    assertThat(child.actualTypeArgument(SwappedChild.class, GenericSample.class, 1)).isEqualTo(Number.class);

    assertThat(child.actualTypeArgument(Swapped.class, MaskSample.class, 0)).isEqualTo(Number.class);
    assertThat(child.actualTypeArgument(Swapped.class, MaskSample.class, 1)).isEqualTo(Long.class);

    assertThat(child.actualTypeArgument(Swapped.class, GenericSample.class, 0)).isEqualTo(Long.class);
    assertThat(child.actualTypeArgument(Swapped.class, GenericSample.class, 1)).isEqualTo(Number.class);
  }
}
