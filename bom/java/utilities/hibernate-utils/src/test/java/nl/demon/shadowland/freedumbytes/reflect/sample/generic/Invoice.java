package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import lombok.Getter;


@Getter
public class Invoice implements SubIdentifiable<String, Long>, ExtraIdentifiable<Number>
{
  private Long id;
  private String subId;
  private Number extraId;
}
