package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.io.Serializable;


public interface Prefix<PRFX extends Serializable> extends Infix
{
  @SuppressWarnings("unchecked")
  default Class<PRFX> prefix()
  {
    return (Class<PRFX>) actualTypeArgument(getClass(), Prefix.class, 0);
  }
}
