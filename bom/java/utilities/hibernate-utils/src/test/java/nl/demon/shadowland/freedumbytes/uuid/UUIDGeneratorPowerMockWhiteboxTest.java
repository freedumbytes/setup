package nl.demon.shadowland.freedumbytes.uuid;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ UUIDGeneratorPowerMockSample.class })
public class UUIDGeneratorPowerMockWhiteboxTest
{
  @Test
  public void powerMockWhitebox_finalClass() throws Exception
  {
    UUIDGeneratorPowerMockSample uuidGeneratorPowerMockSample = Whitebox.invokeConstructor(UUIDGeneratorPowerMockSample.class, new Object[0]);
    UUIDGeneratorPowerMockSample uuidGeneratorSpy = PowerMockito.spy(uuidGeneratorPowerMockSample);

    PowerMockito.when(uuidGeneratorSpy, "type5HashingAlgorithm").thenReturn("SHA~1");

    byte[] name = "namespace + name.".getBytes();
    UUID uuid = Whitebox.invokeMethod(uuidGeneratorPowerMockSample, "generateType5", name);

    verifyUUID(uuid, 5);

    Whitebox.setInternalState(UUIDGeneratorPowerMockSample.class, UUIDGeneratorPowerMockSample.class, uuidGeneratorSpy);

    InternalError error = assertThrows(InternalError.class, () -> UUIDGeneratorPowerMockSample.type5NameUUIDFromBytes(name));
    assertThat(error).hasMessage("SHA~1 not supported");
  }


  private void verifyUUID(UUID uuid, int version)
  {
    assertThat(uuid.variant()).isEqualTo(2);
    assertThat(uuid.version()).isEqualTo(version);
  }
}
