package nl.demon.shadowland.freedumbytes.hibernate.repository;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import nl.demon.shadowland.freedumbytes.hibernate.repository.sample.identifier.Invoice;
import nl.demon.shadowland.freedumbytes.hibernate.repository.sample.identifier.User;


public class IdentifiableTest
{
  @Test()
  public void invoiceLongId()
  {
    Invoice invoice = new Invoice();

    assertThat(invoice.identifierType()).isEqualTo(Long.class);
  }


  @Test()
  public void userStringId()
  {
    User user = new User();

    assertThat(user.identifierType()).isEqualTo(String.class);
  }
}
