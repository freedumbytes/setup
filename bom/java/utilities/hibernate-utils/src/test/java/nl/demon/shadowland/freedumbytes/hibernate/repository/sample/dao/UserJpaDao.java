package nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao;


import org.springframework.stereotype.Repository;

import nl.demon.shadowland.freedumbytes.hibernate.repository.JpaDao;


@Repository("userDao")
public class UserJpaDao extends JpaDao<User, Long> implements UserDao
{
}
