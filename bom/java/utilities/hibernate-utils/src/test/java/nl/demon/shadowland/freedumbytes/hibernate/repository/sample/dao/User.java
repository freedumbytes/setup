package nl.demon.shadowland.freedumbytes.hibernate.repository.sample.dao;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import nl.demon.shadowland.freedumbytes.hibernate.repository.Identifiable;


@Entity
@Table(name = "USER")
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class User implements Identifiable<Long>, Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @Column(name = "ID")
  @Setter(AccessLevel.NONE)
  private Long id;

  @Column(name = "NAME", nullable = false)
  @NonNull
  private String name;

  @Column(name = "REGISTERED", nullable = false)
  @NonNull
  private Boolean registered;
}
