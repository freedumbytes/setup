package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.io.Serializable;

import nl.demon.shadowland.freedumbytes.reflect.Parameterizable;


public interface SuperIdentifiable<SUP extends Serializable> extends Parameterizable
{
  SUP getId();


  @SuppressWarnings("unchecked")
  default Class<SUP> superIdentifierType()
  {
    return (Class<SUP>) actualTypeArgument(getClass(), SuperIdentifiable.class, 0);
  }
}
