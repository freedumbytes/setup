package nl.demon.shadowland.freedumbytes.hibernate.id.uuid;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;

import java.util.UUID;
import java.util.regex.Pattern;


public abstract class CustomVersionStrategyTest
{
  private static final int UUID_VARIANT = 2;


  protected void verifyUUID(UUID uuid, int version, Pattern pattern)
  {
    assertThat(uuid.variant()).isEqualTo(UUID_VARIANT);
    assertThat(uuid.version()).isEqualTo(version);

    String uuidValue = uuid.toString();
    assertThat(uuidValue, matchesPattern(pattern));
    assertThat(uuidValue.toLowerCase(), matchesPattern(pattern));
    assertThat(uuidValue.toUpperCase(), matchesPattern(pattern));
  }
}
