package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.io.Serializable;


public interface Affix<AFFX extends Serializable> extends Postfix<AFFX>
{
  @SuppressWarnings("unchecked")
  default Class<AFFX> affix()
  {
    return (Class<AFFX>) actualTypeArgument(getClass(), Affix.class, 0);
  }
}
