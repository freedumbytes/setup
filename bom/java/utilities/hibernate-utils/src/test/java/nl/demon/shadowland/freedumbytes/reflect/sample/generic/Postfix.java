package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.io.Serializable;


public interface Postfix<PSTFX extends Serializable> extends Suffix<PSTFX>
{
  @SuppressWarnings("unchecked")
  default Class<PSTFX> postfix()
  {
    return (Class<PSTFX>) actualTypeArgument(getClass(), Postfix.class, 0);
  }
}
