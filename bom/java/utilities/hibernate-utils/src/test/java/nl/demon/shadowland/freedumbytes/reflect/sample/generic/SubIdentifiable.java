package nl.demon.shadowland.freedumbytes.reflect.sample.generic;


import java.io.Serializable;


public interface SubIdentifiable<SUBONE extends Serializable, SUBTWO extends Serializable> extends SuperIdentifiable<SUBTWO>
{
  SUBONE getSubId();


  @SuppressWarnings("unchecked")
  default Class<SUBONE> subIdentifierType()
  {
    return (Class<SUBONE>) actualTypeArgument(this.getClass(), SubIdentifiable.class, 0);
  }
}
