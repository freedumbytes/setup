package nl.demon.shadowland.freedumbytes.hibernate.test;


import java.util.function.Function;

import javax.persistence.EntityManager;


/**
 * {@code JPA} transaction function.
 *
 * @param <T>
 *          The function result.
 */
@FunctionalInterface()
public interface JpaTransactionFunction<T> extends Function<EntityManager, T>
{
  /**
   * Before transaction completion function.
   */
  default void beforeTransactionCompletion()
  {
  }


  /**
   * After transaction completion function.
   */
  default void afterTransactionCompletion()
  {
  }
}
