package nl.demon.shadowland.freedumbytes.uuid;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;

import java.util.UUID;
import java.util.regex.Pattern;

import org.junit.Test;


public class UUIDGeneratorTest
{
  private static final Pattern UUID_PATTERN = Pattern.compile("(?i)^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$");


  @Test()
  public void type4UUID()
  {
    UUID uuid = UUID.randomUUID();

    verifyUUID(uuid, 4);
  }


  @Test()
  public void type3UUID()
  {
    UUID uuid = UUID.nameUUIDFromBytes("namespace + name.".getBytes());

    verifyUUID(uuid, 3);
  }


  @Test()
  public void type5UUID()
  {
    UUID uuid = UUIDGenerator.type5NameUUIDFromBytes("namespace + name.".getBytes());

    verifyUUID(uuid, 5);
  }


  private void verifyUUID(UUID uuid, int version)
  {
    assertThat(uuid.variant()).isEqualTo(2);
    assertThat(uuid.version()).isEqualTo(version);

    String uuidValue = uuid.toString();
    assertThat(uuidValue, matchesPattern(UUID_PATTERN));
    assertThat(uuidValue.toLowerCase(), matchesPattern(UUID_PATTERN));
    assertThat(uuidValue.toUpperCase(), matchesPattern(UUID_PATTERN));
  }
}
